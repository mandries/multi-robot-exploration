package algorithms.BrickMortar_NewImplementation;

import java.util.Vector;

import algorithms.Agent;


import tools.Constants;
import tools.Coordinate2D;
import tools.Map2D;
import tools.Map2D.ViewingRangeMap;
import tools.Viewer;


/** 
 * Improvements over BM_Classic:
 * 		Loop closing till the second intersection, skipping the first one 
 * 		(every loop closing closes at least one cell)
 * 
 * 		Keep in memory the time of last nonLoopCellClosing,
 * 			so as to identify false loops.
 * 
 * 		Use LRTA instead of NodeCounting for the DispersionMap
 * 
 *  	Loop closing during loop cleaning
 *  
 *  	Next: add the ATTRACTOR CELL on a separate feature map
 * */
public class BMILRV
extends Agent
{
	// The position of the agent on the grid
	private int cellX;
	private int cellY;
	
	private int agentID;
	
	// The local clock used for time-stamps
	private int localTime = 0;
	
	// Total number of steps made by the agent
	private int totalSteps = 0;
	
	// A vector containing the route taken by the agent
	private Vector <Coordinate2D> lastTrip = new Vector <Coordinate2D>();
	
	// (A pointer to) the map on which the agent travels
	//	and which is shared with other agents
	// Maps for synchrony
	private Map2D map; // The map
	
	private int agentState;
	
	/** BMI SPECIFIC */
	private int timeOfLastNonLoopCellClosing = -1; 
	//private boolean justEnteredLoopControl = false;
	// Used for "second intersection" optimization
	private boolean alreadyClosedCells = false;
	private int intersectionsPassed = 0; // WARNING: USED FOR OPTIMISATION
	/** BMI SPECIFIC (END) */
	
	/* 
	 * Bresenham view (robot local view, not top-down view on the map)
	 * */
	int orientationAngle = 0; //int orientationAngle, 
	float visionFieldArc = 360; //int visionFieldArc,
	int bresenhamDegStep = 1; //int degStep, // angle degree step
	float bresenhamSamplingStep = (float) 0.5; //float step
	int BMI_VIEWING_RANGE = 10; // declared above
	
	public BMILRV(Map2D inputMap, int x, int y, int agentID_in)
	{
		map = inputMap;
		cellX = x;
		cellY = y;
		agentID = agentID_in;
		lastTrip.add(new Coordinate2D(x, y));
		//lastTrip.add(new Coordinate2D(cellX, cellY));
		// Set the initial agent state to loop detection
		agentState = Constants.BM_AGENT_LOOP_DETECTION;
		super.setViewingRange(BMI_VIEWING_RANGE);
	}
		
	
	@Override
	public Coordinate2D getGlobalCoordinates(){return new Coordinate2D(cellX, cellY);}
	//public Coordinate2D getGlobalCoordinates(){return new Coordinate2D(cellX, cellY);}
	
	public int getAgentSteps() {return totalSteps;}
	@Override
	public int getX(){return cellX;}
	@Override
	public int getY(){return cellY;}
	public Vector <Coordinate2D> getLastTrip(){return lastTrip;}
		
	public float getOrientationAngle() {return orientationAngle;} 
	public float getVisionFieldArc() {return visionFieldArc;} //int visionFieldArc,
	public int getBresenhamDegStep() {return bresenhamDegStep;} //int degStep, // angle degree step
	public float getBresenhamSamplingStep() {return bresenhamSamplingStep;} //float step
	public float getViewingRange() {return super.getViewingRange();}
	
	/* Forces 1..N calls to the algorithm to make a single step */
	// FIXED a problem may arise here when exploration ends
	//	OR when agent in standby
	//	OR when agent is OFF
	// the "step" function will loop forever
	@Override
	public void step()
	{	
		// Call the algorithm while a step is not made
		localTime++;
		boolean stepMade; // = false;
		do 
		{
			stepMade=call();
			lastTrip.add(new Coordinate2D(getX(), getY()));
			//lastTrip.add(new Coordinate2D(cellX, cellY));
		}while ((!stepMade) && 
				(agentState != Constants.BM_AGENT_STANDBY) &&
				(agentState != Constants.BM_AGENT_OFF));
		totalSteps++;
	}
	
	/* Make a call to the BrickMortar algorithm*/
	public boolean call()	
	{
		boolean stepMade = false;
		if (Constants.DEBUG)
			System.out.println("Agent" + agentID + " at [" + getX() + ", " + getY() + "], " +
					"which is " + map.getPhysicalMapValue(getX(), getY()));
		/* The call first checks the state of the agent
		 * 	and then performs an action
		 * */
		
		this.setViewingRange(Viewer.getDistanceToClosestAgentInsideMyViewingRange(this.agentID));
		// System.out.println("Distance to closest agent: " + this.getViewingRange());
		
		// Get the map with the cells in the Bresenham viewing range (with cells of type ViewingRangeMap.NOT_VIEWABLE, BM_WALL, BM_EXPL, ...)
		ViewingRangeMap viewingRangeMapRelativeCoords = this.map.BMI_LongVision_Bresenham_getViewingRangeMap(
				getX(), getY(), (int) this.getViewingRange(), (int) this.getOrientationAngle(), 
				(int) this.getVisionFieldArc(), this.getBresenhamDegStep(), this.getBresenhamSamplingStep());
		
		int offsetX = getX() - viewingRangeMapRelativeCoords.getRobotLocation().getX();
		int offsetY = getY() - viewingRangeMapRelativeCoords.getRobotLocation().getY();
		Coordinate2D currentRelativeLocation = viewingRangeMapRelativeCoords.getRobotLocation();//new Coordinate2D(cellX, cellY);

		/**************************\
		|***** Loop DETECTION *****|
		\**************************/
		if (agentState == Constants.BM_AGENT_LOOP_DETECTION)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP DETECTION");
		
			Coordinate2D tmpCell = map.BMgetPreviousCell(getX(), getY(), agentID);
			if (Constants.DEBUG)
				System.out.println("Previous cell is: [" + tmpCell.getX() + ", " + tmpCell.getY() + "]");
			
			/*************/
			/** Marking **/
			/*************/

			// For all the visible cells in this map, treat only those for which:
			//	- you either see all its neightbors
			//	- or which are on border of your vision, and which are blocking for their surroundings
			//  - or consider that everything beyond your border is unlinked free space? -> Difficult to implement
			// Warning: wrong thinking path: 
			// Thought: When checking the isBlocking condition, it would suffice to check only the immediate surroundings,
			//		because all other links which may be blocked by this cell depend on this cell's immediate vicinity.
			// Answer : Not really, because bigger surroundings can show you the presence of far-away links 
			//			(links which exist, but which are not visible in the immediate vicinity).
			
			// 2015.02.04
			// explorable means: not wall, not closed.
			Vector <Coordinate2D> cellsInVisionFieldRelativeCoords = this.map.BMgetReacheableCellsInMap(viewingRangeMapRelativeCoords); 
			//System.out.println("Reacheable cells number: " + cellsInVisionFieldRelativeCoords.size());
			
			
			// 2015.02.19 BMILV optimization: mark again all the cells inside your field of view
			// Mark again only the cells that are not near shadows or near the edge of your field of view.
			// Try to close each cell in the visible range
			// First, reopen them all as explored cells.
			/*
			for (int cellID=0; cellID < cellsInVisionFieldRelativeCoords.size(); cellID+=1)
			{
				Coordinate2D cellRelativeCoord = cellsInVisionFieldRelativeCoords.get(cellID);

				if (!map.BMI_LongVision_isShadowOrEdge(cellRelativeCoord.getX(),cellRelativeCoord.getY(),viewingRangeMapRelativeCoords))
				{
					if (map.getPhysicalMapValue(cellRelativeCoord.getX() + offsetX, cellRelativeCoord.getY() + offsetY) != Constants.BMI_ATTRACTOR)
					{
						map.setPhysicalMapValue(cellRelativeCoord.getX() + offsetX, cellRelativeCoord.getY() + offsetY, Constants.BM_EXPLORED);
						viewingRangeMapRelativeCoords.setMapValue(cellRelativeCoord.getX(),cellRelativeCoord.getY(), (int) Constants.BM_EXPLORED);
					}
				}
			}
			*/
			// End of re-mark optimization
			
			
			// Repeat this step while there are still open cells in the visible range
			// and that after a full pass over these cells, at least one has been closed.
			// TODO: (optimization) start repeating with the cells that were neighbors to the closed ones.
			
			boolean atLeastOneCellWasClosedInFieldOfView = false;
			do
			{
				atLeastOneCellWasClosedInFieldOfView = false; // reset the value of the boolean condition
				
				// Try to close each cell in the visible range
				for (int cellID=0; cellID < cellsInVisionFieldRelativeCoords.size(); cellID+=1)
				//for (int cellID = cellsInVisionFieldRelativeCoords.size()-1; cellID>=0; cellID-=1) // Optimization
				{
					//System.out.println("Reacheable cells number: " + cellsInVisionFieldRelativeCoords.size());
					Coordinate2D cellRelativeCoord = cellsInVisionFieldRelativeCoords.get(cellID);
					// Avoid marking the place where you stand before marking surrounding places. This may block you.
					if (!cellRelativeCoord.equals(currentRelativeLocation))
					{
						if (map.getPhysicalMapValue(cellRelativeCoord.getX() + offsetX, cellRelativeCoord.getY() + offsetY) != Constants.BM_CLOSED)
						{
							markingInLoopDetection(
									cellRelativeCoord.getX() + offsetX, 
									cellRelativeCoord.getY() + offsetY, 
									viewingRangeMapRelativeCoords);
							
							if (map.getPhysicalMapValue(cellRelativeCoord.getX() + offsetX, cellRelativeCoord.getY() + offsetY) == Constants.BM_CLOSED)
							{
								atLeastOneCellWasClosedInFieldOfView = true;
							}	
						}						
					}
				}
				
			}while (atLeastOneCellWasClosedInFieldOfView);
			
			
			// Close the cell where you are standing in the last order (otherwise you risk of blocking an exite route from where you're standing)
			// Attempt to mark the cell the agent is standing on, and then move on to the navigation phase
			// This may be useless, as this cell can be closed in the next step. But why wait for the next step?
			//markingInLoopDetection(currentRelativeLocation.getX() + offsetX, currentRelativeLocation.getY() + offsetY, viewingRangeMapRelativeCoords);
			if (!markingInLoopDetection(getX(), getY(), viewingRangeMapRelativeCoords))
				return false;
			//navigationInLoopDetection(cellX, cellY);
			//"Coord: "
			
			/****************/
			/** Navigation **/
			/****************/
			Vector<Coordinate2D> nextCells = new Vector<Coordinate2D>();
			nextCells = map.BMgetNeighbourCells(getX(), getY());
			nextCells = map.BMreturnPriorityCells(nextCells);

			// If it is the only attainable cell, step on it.
			if (nextCells.size() == 1)
			{
				Coordinate2D nextCell =	nextCells.elementAt(0);

				// 2. NextCellMap Leave direction trace
				map.BMsetNextCell(getX(), getY(), agentID, nextCell);
				
				cellX = nextCell.getX();
				cellY = nextCell.getY();				
				stepMade = true; // Report that 1 step has been made
				return stepMade;
			}
			// Else break ties among equal cells
			else if (nextCells.size() != 1)
			{
				// Remove the previous cell from the next possible cells
				Coordinate2D previousCell = map.BMgetPreviousCell(getX(), getY(), agentID);
				nextCells = map.BMexcludeCellFromList(previousCell.getX(), previousCell.getY(), nextCells);
				
				// BMILV no return to RDV (start)
				if (nextCells.size() == 1)
				{
					Coordinate2D remainingNextCell = nextCells.get(0);
					if (map.BMgetCommonDispersionMap(remainingNextCell.getX(), remainingNextCell.getY()) == Integer.MAX_VALUE)
					{
						// Come back to the previous cell only if it's also not a path to the RDV point. 
						if (map.BMgetCommonDispersionMap(previousCell.getX(), previousCell.getY()) != Integer.MAX_VALUE)
						{
							nextCells.clear();
							nextCells.add(previousCell);
						}
					}
				}
				// BMILV no return to RDV (end)
				
				
				// If there are unexplored cells around you, prefer these
				// Else go to simply explored.
				if (nextCells.size() > 0)
				{
					if (nextCells.elementAt(0).getValue() == Constants.BM_EXPLORED)
					{
						nextCells = map.BMreturnCellsWithMinLRTA(nextCells);										
					}
					else if (nextCells.elementAt(0).getValue() == Constants.BM_UNEXPLORED)
					{
						nextCells = map.BMreturnCellsWithMostClosedWallsAround(nextCells);
					}						
				}

				// Nowhere left to go (surrounded by closed cells and walls) after excluding the previous cell
				if (nextCells.size() == 0)
				{
					// BMILV check
					if (Viewer.BMILV_noAgentOnClosedCellAround(getX(), getY())) // Check if you don't close other agents
					{
						map.setPhysicalMapValue(getX(), getY(), Constants.BM_CLOSED);
						map.BMsetNextCell(getX(), getY(), agentID, new Coordinate2D(-1, -1));
						agentState = Constants.BM_AGENT_OFF;
						stepMade = false; // Report that no step has been made	
					}					
					else
					{
						// Otherwise allow others to move, while doing nothing.
						stepMade = true;
					}
				}
				else //if (nextCells.size() >= 1)
				{
					// Choose a random cell
					Coordinate2D nextCell = new Coordinate2D();
					nextCell = nextCells.elementAt((int) (Viewer.randomGenerator.nextDouble() * nextCells.size()));
					
					// 2. NextCellMap Leave direction trace
					map.BMsetNextCell(getX(), getY(), agentID, nextCell);
					
					cellY = nextCell.getY();
					cellX = nextCell.getX();
					stepMade = true; // Report that 1 step has been made
				}
			}
			
		} // end of LOOP_DETECTION
		
		/*******************\
		|***** CONTROL *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_LOOP_CONTROL)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CONTROL");

			// 3. Individual trace map
			map.BMsetIndividualTraceMap(cellX, cellY, agentID, localTime);
			
			// If cell already closed by someone else, start loop cleaning
			if (map.getPhysicalMapValue(cellX, cellY) == Constants.BM_CLOSED)
			{
				if (Constants.DEBUG)
					System.out.println("This cell is closed.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			else if	(	(map.getPhysicalMapValue(cellX, cellY) == Constants.BM_EXPLORED) ||
						(map.getPhysicalMapValue(cellX, cellY) == Constants.BMI_ATTRACTOR)) /** BMI addition */
			{
				// Agent has found its own ID trace
				if (map.BMgetIDsMap(cellX, cellY) == agentID)
				{
					if (Constants.DEBUG)
						System.out.println("Found my own ID trace.");
					agentState = Constants.BM_AGENT_LOOP_CLOSING;
					stepMade = false;
					return stepMade;
					
					// BMILV
					// map.BMsetIDsMap(cellX, cellY, agentID); // Control cell -> Useless, ID sign already on it
					// Navigation
					/*
					Coordinate2D nextCell = map.BMgetNextCell(cellX, cellY, agentID);
					System.out.println("Leaving my ID mark on the starting (and also ending cell of the loop).");
					cellX = nextCell.getX();
					cellY = nextCell.getY();
					stepMade = true;
					return stepMade;
					*/
				}
				// Agent did not find any trace (cell explored without control)
				else if (map.BMgetIDsMap(cellX, cellY) == Constants.BM_NO_AGENT_TRACE)
				{
					// Marking
					map.BMsetIDsMap(cellX, cellY, agentID); // Control cell

					// Navigation
					Coordinate2D nextCell = map.BMgetNextCell(cellX, cellY, agentID);
					
					// FIX BM
					if (nextCell.getX() == -1)
					{
						if (Constants.DEBUG)
							System.out.println("No next cell found. Cleaning...");
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}
					else if (map.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) == Constants.BM_CLOSED)
					{
						if (Constants.DEBUG)
							System.out.println("The next cell is closed. Cleaning...");
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}
					// ELSE IF CELL NOT CLOSED (THEREFORE EXPLORED)
					else if (map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) > agentID) // and != Constants.BM_NO_AGENT_TRACE))
					{
						if (Constants.DEBUG)
							System.out.println("Agent with a higher priority controls next cell.");
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}				
					// Check for agents in STANDBY
					else if(map.BMgetStandbyMap(nextCell.getX(), nextCell.getY()) > agentID)
					{
						if (Constants.DEBUG)
							System.out.println("Another agent with a higher priority " +
									"is on standby on the next cell.");
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}
					else //if (map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == Constants.BM_NO_AGENT_TRACE) 
						//(map.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
					{
						if (Constants.DEBUG)
							System.out.println("The next cell is open and free to control.");
						cellX = nextCell.getX();
						cellY = nextCell.getY();
						stepMade = true;
						return stepMade;	
					}
				}
				
				// If B controls and B > A
				else if (map.BMgetIDsMap(cellX, cellY) > agentID)
				{
					if (Constants.DEBUG)
						System.out.println("Another agent with a higher priority is controling this cell.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				// If C standby and C > A
				else if (map.BMgetStandbyMap(cellX, cellY) > agentID)
				{
					if (Constants.DEBUG)
						System.out.println("Another agent with a higher priority is on standby here.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				// Otherwise become standby
				else
				{
					// Wait for your cell to be liberated
					if (Constants.DEBUG)
					{
						System.out.println("Waiting for the cell to be liberated");
						System.out.println("Local current CONTROL ID:" + map.BMgetIDsMap(cellX, cellY));
						System.out.println("Local current STANDBY ID:" + map.BMgetStandbyMap(cellX, cellY));
					}
					
					map.BMsetStandbyMap(cellX, cellY, agentID);
					agentState = Constants.BM_AGENT_STANDBY;
					stepMade = false;
					return stepMade;				
				}			
				
			}
		}
		
		/*******************\
		|***** CLOSING *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_LOOP_CLOSING )
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CLOSING");
			
			// 3. Individual trace map (useless here)
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			// Marking
			//Switch to loop cleaning if intersection reached
			if (map.BMisIntersection(cellX, cellY, agentID))
			{
				if (Constants.DEBUG)
					System.out.println("Intersection detected !");
				
				// BMI OPTIMIZATION (SKIP 1st intersection if no closed cells)
				intersectionsPassed++;
				if ((intersectionsPassed == 2) || (alreadyClosedCells))
				{
					if (Constants.DEBUG)
						System.out.println("Second intersection found ! Stopping Loop closing !");
					intersectionsPassed = 0;
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					alreadyClosedCells = false;
					
					stepMade = false;
					return stepMade;
				}
				else if ((intersectionsPassed == 1) && (!alreadyClosedCells))
				{
					if (Constants.DEBUG)
					System.out.println("First intersection found ! " +
							"Skipping and continuing Loop closing !");
				
					// Navigation
					Coordinate2D nextCell = map.BMgetNextCell(cellX, cellY, agentID);

					if (map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == agentID)
					{
						if (map.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
						{
							cellX = nextCell.getX();
							cellY = nextCell.getY();
							stepMade = true;
							return stepMade;
						}
						else // cell is closed
						{
							agentState = Constants.BM_AGENT_LOOP_CLEANING;
							stepMade = false;
							return stepMade;
						}
					}
					//else // should never occur. Closing should go until intersection or until ended. (is it true? 2015.02.09)
					//{
					//	agentState = Constants.BM_AGENT_LOOP_CLEANING;
					//	stepMade = false;
					//	return stepMade;
					//}
				}
				/** BMI OPTIMIZATION END */
				
			}
			else // Not an intesection (loop closing) //if(!map.BMisIntersection(currentX, currentY, agentID))
			{
				// Marking
				map.setPhysicalMapValue(cellX, cellY, Constants.BM_CLOSED);
				alreadyClosedCells = true;
				
				// Navigation
				Coordinate2D nextCell = map.BMgetNextCell(cellX, cellY, agentID);

				if (map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == agentID)
				{
					if (map.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
					{
						cellX = nextCell.getX();
						cellY = nextCell.getY();
						stepMade = true;
						return stepMade;
					}
					else // cell is closed
					{
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}
				}
				//else // Should never occur
				//{
				//	agentState = Constants.BM_AGENT_LOOP_CLEANING;
				//	stepMade = false;
				//	return stepMade;
				//}
			}
		}
		
		/********************\
		|***** CLEANING *****|
		\********************/
		else if (agentState == Constants.BM_AGENT_LOOP_CLEANING)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CLEANING");
			
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
				
			// Get the previous cell
			Coordinate2D previousCell = map.BMgetPreviousCellLoopCleaning(cellX, cellY, agentID);
			
			if (map.BMgetIDsMap(cellX, cellY) == agentID)
			{
				// Marking
				map.BMsetIDsMap(cellX, cellY, Constants.BM_NO_AGENT_TRACE);
				map.BMsetNextCell(cellX, cellY, agentID, new Coordinate2D(-1, -1));
								
				/** BMI OPTIMIZATION */				
				if ((!map.BMI_LongVision_isBlocking(currentRelativeLocation.getX(), currentRelativeLocation.getY(), viewingRangeMapRelativeCoords)) &&
					(map.BMnoClosedCellsWithIDAround(cellX, cellY)) &&
					(map.BMgetIDsMap(cellX, cellY) == Constants.BM_NO_AGENT_TRACE) &&
					Viewer.BMILV_noAgentOnClosedCellAround(cellX, cellY) && // CHEAT, should be removed (is it a cheat? doesn't work otherwise)
					(map.getPhysicalMapValue(cellX, cellY) != Constants.BMI_ATTRACTOR))
				{
					map.setPhysicalMapValue(cellX, cellY, Constants.BM_CLOSED);
					timeOfLastNonLoopCellClosing = localTime;
				}// end of !isBlocking		
				/** BMI OPTIMIZATION END */
				
				// If we can continue following previous cells, go on.
				if (previousCell.getX() != -1)
				{
					cellX = previousCell.getX();
					cellY = previousCell.getY();
					stepMade = true;
					return stepMade;
				}
				// If no previous cell was found in order to continue cleaning, then move on to Loop detection
				else
				{
					if (Constants.DEBUG)
						System.out.println("Previous cell not found for cleaning.");
					agentState = Constants.BM_AGENT_LOOP_DETECTION;
					stepMade = false;
					return stepMade;
				}
			}
			// This cell's ID is not the same as this agent's ID, and there is a previous cell to which we can go to
			else if (previousCell.getX() != -1)
			{
				/** BMI OPTIMIZATION */
				if ((!map.BMI_LongVision_isBlocking(	currentRelativeLocation.getX(), currentRelativeLocation.getY(), viewingRangeMapRelativeCoords)) &&	
					(map.BMnoClosedCellsWithIDAround(cellX, cellY)) && // cheat ?
					(map.BMgetIDsMap(cellX, cellY) == Constants.BM_NO_AGENT_TRACE) &&
					Viewer.BMILV_noAgentOnClosedCellAround(cellX, cellY) // CHEAT, should be removed (is it a cheat? doesn't work otherwise)
					)
				{
					map.setPhysicalMapValue(cellX, cellY, Constants.BM_CLOSED);
					timeOfLastNonLoopCellClosing = localTime;
				}		
				/** END OF BMI OPTIMIZATION */
				
				if (Constants.DEBUG)
					System.out.println("This cell is clean from my ID, " +
						"going to the neighbouring one to clean it.");
				cellX = previousCell.getX();
				cellY = previousCell.getY();
				stepMade = true;
				return stepMade;
			}
			else
			{
				if (Constants.DEBUG)
					System.out.println("This cell is already clean from my ID.");
				agentState = Constants.BM_AGENT_LOOP_DETECTION;
				stepMade = false;
				return stepMade;
			}
		}
		
		/*******************\
		|***** STANDBY *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_STANDBY)
		{
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			if (Constants.DEBUG)
			{
				System.out.println("Agent " + agentID + " is in STANDBY");
				System.out.println("Local current CONTROL ID:" + map.BMgetIDsMap(cellX, cellY));
				System.out.println("Local current STANDBY ID:" + map.BMgetStandbyMap(cellX, cellY));
			}

			// If cell becomes visited
			if (map.getPhysicalMapValue(cellX, cellY) == Constants.BM_CLOSED)
			{
				if (Constants.DEBUG)
					System.out.println("Cell became visited. Switching to cleaning.");
				map.BMsetStandbyMap(cellX, cellY, Constants.BM_NO_AGENT_TRACE);
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If replaced by another agent in STANDBY
			else if (map.BMgetStandbyMap(cellX, cellY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Agent with a higher priority took over the standby slot. Switching to cleaning.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If another agent TOOK CONTROL over
			else if (map.BMgetIDsMap(cellX, cellY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Agent with a higher priority took over the cell. Switching to cleaning.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If cell is cleaned
			else if (map.BMgetIDsMap(cellX, cellY) == Constants.BM_NO_AGENT_TRACE)
			{
				if (Constants.DEBUG)
					System.out.println("Cell cleaned. Switching back to CONTROL.");
				//System.out.println("Standby mode: Cell cleaned. Switching back to CONTROL.");				
				map.BMsetStandbyMap(cellX, cellY, Constants.BM_NO_AGENT_TRACE);
				agentState = Constants.BM_AGENT_LOOP_CONTROL;
				stepMade = false;
				return stepMade;
			}
			// otherwise remain standby
			else
			{
				// Remain standby
				if (Constants.DEBUG)
				{
					System.out.println("Remaining in standby");
					System.out.println("Local current CONTROL ID:" + map.BMgetIDsMap(cellX, cellY));
					System.out.println("Local current STANDBY ID:" + map.BMgetStandbyMap(cellX, cellY));
				}
				stepMade = false;
				return stepMade;
			}
		}
		
		/***************\
		|***** OFF *****|
		\***************/
		else if (agentState == Constants.BM_AGENT_OFF)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is OFF");
			stepMade = false;
			return stepMade;
		} 
		
		/* Say if a step has been made */
		return stepMade;
	}
	// End of "CALL" function
	
	
	
	/* WARNING: receives as input global coordinates for the cell to be marked.*/
	/** 
	 * Returns a boolean, telling whether a marking step has been made or not.
	 * Make sure that you return the boolean stepMade in every branch of the function
	 * */
	private boolean markingInLoopDetection(int cellToMarkX_GlobalCoords, int cellToMarkY_GlobalCoords, ViewingRangeMap viewingRangeMap_in)
	{
		//boolean stepMade = false;
		int offsetX = cellX - viewingRangeMap_in.getRobotLocation().getX();
		int offsetY = cellY - viewingRangeMap_in.getRobotLocation().getY();
		//System.out.println("Making a decision to mark global coords : [" + (cellToMarkX_GlobalCoords) + ", " + (cellToMarkY_GlobalCoords)+ "]");
		//System.out.println("Making a decision to mark relative coords : [" + (cellToMarkX_GlobalCoords - offsetX) + ", " + (cellToMarkY_GlobalCoords - offsetY)+ "]");
		
		/* 0. Attractor cell */
		if (map.getPhysicalMapValue(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords) == Constants.BMI_ATTRACTOR)
		{
			//System.err.println("Attractor cell: No decision.");
			// Do not mark anything	
			
			// If no way to go, then close
			// Else leave the cell without making any traces
		}
		// Cell is explored
		else if (map.getPhysicalMapValue(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords) == Constants.BM_EXPLORED)
		{
			// If is blocking
			if (map.BMI_LongVision_isBlocking(cellToMarkX_GlobalCoords - offsetX, cellToMarkY_GlobalCoords - offsetY, viewingRangeMap_in))
			{
				// Do nothing (in terms of marking)
				// However, check if you should start loop control
				
				/* 1. Already been in this cell */
				if (	
						// BMI OPTIMIZATION : avoid closing loops that may have been cut since your last visit
						(map.BMgetIndividualTraceMap(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID) > timeOfLastNonLoopCellClosing) &&
						
						// Not from opposite direction
						(!	map.BMgetNextCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).equals
							(map.BMgetPreviousCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID))
						)	&&
						
						// THIS Agent has once passed through this cell
						(!map.BMgetNextCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).equals(new Coordinate2D(-1, -1))) && // there is a trace
						
						(!map.BMgetPreviousCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).equals(new Coordinate2D(-1, -1))) && // there is a trace
						
						// The loop is not being closed by anyone else
						(map.BMgetIDsMap(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords) == Constants.BM_NO_AGENT_TRACE)
					)
				{
					// Check if you are here
					if ((cellToMarkX_GlobalCoords == cellX) && (cellToMarkY_GlobalCoords == cellY))
					{
						/*
						System.out.println("IDs map for : [" + cellToMarkX_GlobalCoords + ", " + cellToMarkY_GlobalCoords + "] = " + map.BMgetIDsMap(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords));
						System.out.println("Individual trace map for : [" + cellToMarkX_GlobalCoords + ", " + cellToMarkY_GlobalCoords + "] = " + map.BMgetIndividualTraceMap(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID));
						System.out.print("Previous cell for: [" + cellToMarkX_GlobalCoords + ", " + cellToMarkY_GlobalCoords + "] = ");
							map.BMgetPreviousCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).print();
						System.out.println("Individual trace at prev cell: " + 
							map.BMgetIndividualTraceMap(
									map.BMgetPreviousCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).getX(), 
									map.BMgetPreviousCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).getY(), 
									agentID));
						System.out.print("Next cell for: [" + cellToMarkX_GlobalCoords + ", " + cellToMarkY_GlobalCoords + "] = ");
							map.BMgetNextCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).print();
						System.out.println("Individual trace at next cell: " + 
							map.BMgetIndividualTraceMap(
									map.BMgetNextCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).getX(), 
									map.BMgetNextCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID).getY(), 
									agentID));							
						*/	
						agentState = Constants.BM_AGENT_LOOP_CONTROL;
						//System.err.println("Switching to loop control!");
						return false; // no stepMade;
					}
				}
			}
			// If not blocking
			else
			{
				/* 2. Agent hasn't yet visited this cell 
				 * and cell is not blocking anything */
				// and cell not controlled by someone else
				if (
						(map.BMnoClosedCellsWithIDAround(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords)) && // CHEAT, should be removed (is it a cheat? doesn't work otherwise)
						Viewer.BMILV_noAgentOnClosedCellAround(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords) // CHEAT, should be removed (is it a cheat? doesn't work otherwise)
					) 
				{
					// 1. BM map 
					map.setPhysicalMapValue(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, Constants.BM_CLOSED);
					map.BMsetNextCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID, new Coordinate2D(-1, -1));
						
					// update the viewingRangeMap, otherwise the subsequent calculations for blocking will be inconsistent
					viewingRangeMap_in.setMapValue(cellToMarkX_GlobalCoords - offsetX, cellToMarkY_GlobalCoords - offsetY, (int) Constants.BM_CLOSED);
					
					//System.out.println("Agent " + agentID + " closed: [" + cellToMarkX_GlobalCoords +  ", " + cellToMarkY_GlobalCoords +"]");
						
					// Mettre a jour le temps de la derniere fermeture de cellule 
					// que quand tu ferme celle en-dessous de toi (puisque tu veux garder le chemin)
					if ((cellToMarkX_GlobalCoords == cellX) && (cellToMarkY_GlobalCoords == cellY))
					{
						timeOfLastNonLoopCellClosing = localTime;
					}
				}
				// This cell blocks the exit of another agent into the open portion of the graph.
				else // if (map.BMgetIDsMap(currentX, currentY) != Constants.BM_NO_AGENT_TRACE)
				{
					 // Check if no cells with this ID are around
					//System.err.println("ELSE case : should not occur");
				}
			}
			// end of is not Blocking
		}			
		else if (map.getPhysicalMapValue(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords) == Constants.BM_UNEXPLORED)
		{
			// If is blocking
			if (map.BMI_LongVision_isBlocking(cellToMarkX_GlobalCoords - offsetX, cellToMarkY_GlobalCoords - offsetY, viewingRangeMap_in))
			{
				//Gaseste aici de ce nu lucreaza algoritmul de inchidere a celulelor in impasuri.
				map.setPhysicalMapValue(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, Constants.BM_EXPLORED);
			}// end of isBlocking
			
			// If not blocking
			else
			{
				if (	(map.BMnoClosedCellsWithIDAround(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords)) && // TODO : try to remove this. CHEAT, should be removed (is it a cheat? doesn't work otherwise)
						Viewer.BMILV_noAgentOnClosedCellAround(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords) // TODO : try to remove this. CHEAT, should be removed (is it a cheat? doesn't work otherwise)
					)
				{
					// 1. BM map 
					map.setPhysicalMapValue(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, Constants.BM_CLOSED);
					map.BMsetNextCell(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID, new Coordinate2D(-1, -1));
						
					// update the viewingRangeMap, otherwise the subsequent calculations for blocking will be inconsistent
					viewingRangeMap_in.setMapValue(cellToMarkX_GlobalCoords - offsetX, cellToMarkY_GlobalCoords - offsetY, (int) Constants.BM_CLOSED);
						
					//System.out.println("Closed: [" + cellToMarkX_GlobalCoords +  ", " + cellToMarkY_GlobalCoords +"]");
						
					// Mettre a jour le temps de la derniere fermeture de cellule 
					// que quand tu ferme celle en-dessous de toi (puisque tu veux garder le chemin)
					if ((cellToMarkX_GlobalCoords == cellX) && (cellToMarkY_GlobalCoords == cellY))
					{
						timeOfLastNonLoopCellClosing = localTime;
					}
				}
			}
		}		
		// Else : don't do anything to walls or closed cells.

		// 2. Common dispersion map
		// WARNING: the LRTA marking is done on the level above, only for the cell where the robot is standing.
		if ((cellToMarkX_GlobalCoords == cellX) && (cellToMarkY_GlobalCoords == cellY))
		{
			/** BMI */
			// Should call this only on the cell your are standing on.
			map.BMILVupdateLRTAMap(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords);
			
			// Alternatives way of directing the agents
				//map.BMupdateThrunMap(currentX, currentY); 
				//map.BMupdateTimestampMap(currentX, currentY, localTime);
				//currentMap.BMupdateNodeCountingMap(currentX, currentY);
			/** BMI END */
					
			// 3. Individual trace map
			// Should call this only on the cell your are standing on.
			map.BMsetIndividualTraceMap(cellToMarkX_GlobalCoords, cellToMarkY_GlobalCoords, agentID, localTime);	
		}
		
		// BMILV
		return true; // a marking step was made.
	}

	
}
