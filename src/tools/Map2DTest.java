package tools;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tools.Map2D.ViewingRangeMap;

public class Map2DTest 
{

	public static Map2D map; 
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception 
	{
		map = new Map2D(10, 10); // height, width
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void viewingRangeTest() 
	{
		//fail("Not yet implemented");
	}
	
	@Test 
	public void testMapSize()
	{
		map = new Map2D(10, 10); // height, width
		
		assertTrue(map.getDataMap().length == 10);
		assertTrue(map.getDataMap()[0].length == 10);
	}
	
	@Test
	public void testMapGeneration()
	{
		for (int x=0; x<map.getDataMap().length; x++)
			for (int y=0; y<map.getDataMap()[0].length; y++)
				assertTrue(map.getPhysicalMapValue(x, y) == Constants.BM_UNEXPLORED);
	}
	
	@Test
	public void testMapWriting()
	{
		map = new Map2D(10, 10); // height, width
		
		map.setPhysicalMapValue(0, 0, Constants.BM_WALL);
		assertTrue(map.getPhysicalMapValue(0, 0) == Constants.BM_WALL);
		
		map.setPhysicalMapValue(9, 9, Constants.BM_EXPLORED);
		assertTrue(map.getPhysicalMapValue(9, 9) == Constants.BM_EXPLORED);
	}
	
	@Test
	public void testGetCellsInSquareViewingRange_1()
	{	
		map = new Map2D(10, 10); // height, width
		
		ViewingRangeMap viewingRangeMap = map.getCellsInSquareViewingRange(
				0, // x
				0, //y  
				5);//int viewingRange	
		
		// System.out.println("map width: " + viewingRangeMap.getMap().length);
		// System.out.println("map height: " + viewingRangeMap.getMap()[0].length);
		
		assertTrue(viewingRangeMap.getMap().length == 6);
		assertTrue(viewingRangeMap.getMap()[0].length == 6);
	}
	
	@Test
	public void testGetCellsInSquareViewingRange_2()
	{		
		ViewingRangeMap viewingRangeMap = map.getCellsInSquareViewingRange(
				5, // x
				4, //y  
				5);//int viewingRange	
		
		//System.out.println("map width: " + viewingRangeMap.getMap().length);
		//System.out.println("map height: " + viewingRangeMap.getMap()[0].length);
		
		assertTrue(viewingRangeMap.getMap().length == 10);
		assertTrue(viewingRangeMap.getMap()[0].length == 10);
	}
	
	@Test
	public void testGetCellsInSquareViewingRange_3()
	{		
		ViewingRangeMap viewingRangeMap = map.getCellsInSquareViewingRange(
				8, // x
				8, //y  
				5);//int viewingRange	
		
		// System.out.println("map width: " + viewingRangeMap.getMap().length);
		// System.out.println("map height: " + viewingRangeMap.getMap()[0].length);
		
		assertTrue(viewingRangeMap.getMap().length == 7);
		assertTrue(viewingRangeMap.getMap()[0].length == 7);
	}
		
	
	@Test
	public void testGetCellsInSquareViewingRange_4()
	{		
		ViewingRangeMap viewingRangeMap = map.getCellsInSquareViewingRange(
				5, // x
				4, //y  
				2);//int viewingRange	
		
		//System.out.println("map width: " + viewingRangeMap.getMap().length);
		//System.out.println("map height: " + viewingRangeMap.getMap()[0].length);
		
		assertTrue(viewingRangeMap.getMap().length == 5);
		assertTrue(viewingRangeMap.getMap()[0].length == 5);
	}

	@Test
	public void testBMI_LongVision_Bresenham_getViewingRangeMap() 
	{
		ViewingRangeMap viewingRangeMap = map.BMI_LongVision_Bresenham_getViewingRangeMap(
						5, //int x_in, 
						5, //int y_in, 
						2, //int viewingRange, 
						0, //int orientationAngle, 
						360, //float visionFieldArc,
						5, //int degStep, // angle degree step
						(float) 0.5 //float step
						);

		assertTrue(viewingRangeMap.getMap().length == 5); // width
		assertTrue(viewingRangeMap.getMap()[0].length == 5); // height
		
		assertTrue(viewingRangeMap.getRobotLocation().getX() == 2); // height
		assertTrue(viewingRangeMap.getRobotLocation().getY() == 2); // height

		viewingRangeMap.printMap();
		
		Vector<Coordinate2D> reachableCells = map.BMgetReacheableCellsInMap(viewingRangeMap);
		System.out.println("There are : " + reachableCells.size() + " reachable cells.");
		
		// It's 21 because the Bresenham algorithm scans a circle around the robot. 
		// Not all the cells in the surrounding square fit into the viewing circle. 
		// Otherwise, it would see 25 cells, and not 21. 
		assertTrue(reachableCells.size() == 21);   
		//public boolean BMI_LongVision_isBlocking(int x_in_relativeCoords, int y_in_relativeCoords, ViewingRangeMap viewingRangeMap)
	}
	
	//@Test
	public void testBMI_LongVision_isBlocking_1() 
	{
		ViewingRangeMap viewingRangeMap = map.BMI_LongVision_Bresenham_getViewingRangeMap(
						5, //int x_in, 
						5, //int y_in, 
						2, //int viewingRange, 
						0, //int orientationAngle, 
						360, //float visionFieldArc,
						5, //int degStep, // angle degree step
						(float) 0.5 //float step
						);

		viewingRangeMap.setMapValue(2, 0, (int) Constants.BM_WALL);
		viewingRangeMap.setMapValue(2, 1, (int) Constants.BM_WALL);
		viewingRangeMap.setMapValue(2, 3, (int) Constants.BM_WALL);
		viewingRangeMap.setMapValue(2, 4, (int) Constants.BM_WALL);
		
		boolean robotLocationIsBlocking = 
				map.BMI_LongVision_isBlocking(
				viewingRangeMap.getRobotLocation().getX(), //int x_in_relativeCoords, 
				viewingRangeMap.getRobotLocation().getY(), //int y_in_relativeCoords, 
				viewingRangeMap);
		
		viewingRangeMap.printMap();
		
		assertTrue(robotLocationIsBlocking == true);
	}
	
	@Test
	public void testBMI_LongVision_isBlocking_2() 
	{
		map.setPhysicalMapValue(0, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(1, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(2, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(3, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(4, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(6, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(7, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(8, 5, (int) Constants.BM_WALL);
		map.setPhysicalMapValue(9, 5, (int) Constants.BM_WALL);	
		
		ViewingRangeMap viewingRangeMap = map.BMI_LongVision_Bresenham_getViewingRangeMap(
						5, //int x_in, 
						5, //int y_in, 
						3, //int viewingRange, 
						0, //int orientationAngle, 
						360, //float visionFieldArc,
						5, //int degStep, // angle degree step
						(float) 0.5 //float step
						);
		/*
		boolean robotLocationIsBlocking = 
				map.BMI_LongVision_isBlocking(
				viewingRangeMap.getRobotLocation().getX(), //int x_in_relativeCoords, 
				viewingRangeMap.getRobotLocation().getY(), //int y_in_relativeCoords, 
				viewingRangeMap);
		
		viewingRangeMap.printMap();
		*/
		//assertTrue(robotLocationIsBlocking == true);
		
		//viewingRangeMap.printMap();
		Vector<Coordinate2D> reachableCells = map.BMgetReacheableCellsInMap(viewingRangeMap);
		for (int cellID = 0; cellID < reachableCells.size(); cellID+=1)
		{
			//System.out.print(cellID + ": robotLocation is: ");
			//viewingRangeMap.getRobotLocation().print();
			//viewingRangeMap.printMap();

			Coordinate2D cellCoord = reachableCells.get(cellID);
			System.out.print("Trying to close: "); 
			cellCoord.print();
			
			if (!cellCoord.equals(viewingRangeMap.getRobotLocation())) // close last the stell you are standing on.
			{
				if (!map.BMI_LongVision_isBlocking(
						cellCoord.getX(), //int x_in_relativeCoords, 
						cellCoord.getY(), //int y_in_relativeCoords, 
						viewingRangeMap)
					)
				{
					System.out.println("SUCCESS!");
					viewingRangeMap.setMapValue(cellCoord.getX(), cellCoord.getY(), (int) Constants.BM_CLOSED);
				}
				else
				{
					System.out.println("FAILED");
				}
				viewingRangeMap.printMap();				
			}
		}
	}
}
