package tools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import javax.swing.JApplet;

import tools.Map2D.ViewingRangeMap;

import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;

import algorithms.Agent;
import algorithms.BrickMortar_NewImplementation.BrickMortarClassic;
import algorithms.BrickMortar_NewImplementation.BrickMortarImproved;

public class Viewer
extends JApplet
implements KeyListener
{
	private static final long serialVersionUID = 1L;
	public static final boolean DEBUG = true;

	public static final long seed = (long) 200718403109832L; // OK
	// public static final long seed = (long) 0L; // OK
	//public static final long seed = (long) Math.random(); // OK
	public static Random randomGenerator = new Random(seed);
	
	public static final int BMfeatureNoFeature = -1;
	public static final int BMfeatureLoopEntrance = 0;
	// 1 to avoid IDs equal to 0 (which is used for NO_AGENT_TRACE)
	//public static final int BM_ID_AGENTS_OFFSET = 1; 

	Vector<String> mapsPaths = new Vector<String>();
	
	/** SIMULATION VARIABLES */
	private static final int GLOBAL_AGENTS = 3;
	private static final int MIN_AGENTS = GLOBAL_AGENTS;
	public static final int MAX_AGENTS = GLOBAL_AGENTS;
	//public static final int MANUAL_MAX_AGENTS = GLOBAL_AGENTS;
	
	private static final int TOTAL_RUNS = 20;
	private static final int MAX_STEPS = 100000; // max steps per run
	
	private static final int startX = 1;//25; //19;
	private static final int startY = 1;//25; //20;
	public static final int rdvX = 1; //-1;
	public static final int rdvY = 1; //-1;

	private static final int BM = 10;  // Brick & Mortar
	private static final int BMI = 12;	 // Brick & Mortar Improved
	private static final int BMILRV = 13; // Brick & Mortar Improved, Long Range Vision
	private static final int BMILRVSM = 14;  // Brick & Mortar Improved, Long Range Vision, Shared Map
	private static final int FEG = 20; // Frontier Explorator Greedy
	private static final int FED = 21; // Frontier Explorator Distributed
	
	private static int ALGO = BMILRV; //BMILRV; //BMILRVSM; // FED; // FED; // FEG; // BMI; // BM;
	
	private static final int AUTORUN = 0;
	private static final int MANUAL = 1;
	private final int RUNMODE = MANUAL; //AUTORUN; // MANUAL;
	
	private static boolean RECORD_GIF = false;
	//private static boolean RECORD_GIF = true;
	
	//private final int END_STATE = Map2D.MAP_EXPLORED;
	private final int END_STATE = Map2D.MAP_CLOSED;
	
	private static String rootPath = ""; // is given value in init()
	private static final String mapExtension = ".png";
	private static String mapsCategory = "";
	/*************************/

	private int fullExplore = 0;
	private int fullVisit = 0;
	
	private Vector<Integer> explorationTimes = new Vector<Integer>();
	private Vector<Integer> closureTimes = new Vector<Integer>();
	private Vector<Integer> totalAgentSteps = new Vector<Integer>();
	
	private static Map2D m;
	private Map2D BMoldMap;
	private Map2D BMnewMap;
		
	private static Vector <Agent> agents = new Vector <Agent>();
	
	private int agentSize = Constants.AGENT_SIZE; 
	private int pathSize = 6; // probably the size of the drawn circles retracing the paths
	
	private int step=0;
	
	// Frontier Explorator Distributed, frontier assignments
	private static Vector<Vector<Coordinate2D>> FEDopenFrontiers = new Vector<Vector<Coordinate2D>>();
	private static HashMap<Integer, Integer> FEDfrontierAssignments = new HashMap<Integer,Integer>();
	
	private AnimatedGifEncoder gif = new AnimatedGifEncoder();
	
	public void init()
	{
		// Setting the benchmark maps
		// Adding paths to the 2015.02 maps
				
		//* 
		// 1. Garden
		mapsPaths.add("garden_50x50");
		mapsPaths.add("garden_75x75");
		mapsPaths.add("garden_100x100");
		mapsPaths.add("garden_150x150");
		mapsCategory = "Garden";
		rootPath = "../src/images/MapSamples/2015.02_samples/";		
		//*/
		
		/*
		// 2. Maze
		mapsPaths.add("maze_50x50");
//		mapsPaths.add("maze_75x75");
//		mapsPaths.add("maze_100x100");
//		mapsPaths.add("maze_150x150");
		mapsCategory = "Maze";
		rootPath = "../src/images/MapSamples/2015.02_samples/";
		*/
		
			/*
		// 3. Office map
		//mapsPaths.add("office_50x50");
		//mapsPaths.add("high_res_obstacle_shadow_50x50");
		//mapsPaths.add("high_res_obstacle_shadow_100x100");
		mapsPaths.add("sample5_20x20_closed");
		mapsCategory = "Office";
		rootPath = "../src/images/MapSamples/2015.02_samples/";
		*/
		
		// checkMapParameters();
				
		if (RUNMODE == MANUAL)
		{
			m = new Map2D();
			
			if 	((ALGO == BM) || (ALGO == BMI))
			{
				// Create the maps
				BMoldMap = new Map2D();
				BMnewMap = new Map2D();
				
				// Create the last step map
				BMoldMap.BMImageToMap2D(rootPath + 
						mapsPaths.get(0) + 
						mapExtension);
				BMoldMap.setCellHeight(24);
				BMoldMap.setCellWidth(24);
				
				// Create the current step map (for synchronized version)
				BMnewMap.BMImageToMap2D(rootPath + 
						mapsPaths.get(0) + 
						mapExtension);
				BMnewMap.setCellHeight(24);
				BMnewMap.setCellWidth(24);
				
				/** BMI */
				if (ALGO == BMI)
				{	
					if ((rdvX != -1) && (rdvY != -1))
					{
						BMoldMap.setPhysicalMapValue(rdvX, rdvY, Constants.BMI_ATTRACTOR);
						BMnewMap.setPhysicalMapValue(rdvX, rdvY, Constants.BMI_ATTRACTOR);	
					}					
				}
				
				Constants.AGENT_SIZE = 16;
				//agentSize = Constants.AGENT_SIZE;  
			}
			else if ((ALGO == BMILRV) || (ALGO == BMILRVSM))
			{
				// Create the maps
				m = new Map2D();
				
				// Create the map
				m.BMImageToMap2D(rootPath + 
						mapsPaths.get(0) + 
						mapExtension);
				m.setCellHeight(Constants.CELL_HEIGHT);
				m.setCellWidth(Constants.CELL_WIDTH);
								
				if ((rdvX != -1) && (rdvY != -1))
				{
					m.setPhysicalMapValue(rdvX, rdvY, Constants.BMI_ATTRACTOR);
				}
				
//				Constants.AGENT_SIZE = 16;
			}
			else if ((ALGO == FEG) || (ALGO == FED))
			{
				// Create the maps
				m = new Map2D();
				
				// Create the map
				m.BMImageToMap2D(rootPath + 
						mapsPaths.get(0) + 
						mapExtension);
				m.setCellHeight(Constants.CELL_HEIGHT);
				m.setCellWidth(Constants.CELL_WIDTH);
			}
			
			// Adding agents
			agents.clear();
			for (int id=0; id<GLOBAL_AGENTS; id++)
			{
				Agent agent = new Agent();
				if (ALGO == BM)
				{
					agent = new BrickMortarClassic(BMoldMap, BMnewMap, startX, startY, id);					
				}
				else if (ALGO == BMI)
				{
					agent = new BrickMortarImproved(BMoldMap, BMnewMap, startX, startY, id);
				}
				else if (ALGO == BMILRV)
				{
					agent = new algorithms.BrickMortar_NewImplementation.BMILRV(m, startX, startY, id);
				}
				else if (ALGO == BMILRVSM)
				{
					agent = new algorithms.BrickMortar_NewImplementation.BMILRVSM(m, startX, startY, id);
				}
				else if (ALGO == FEG)
				{
					agent = new algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy(m, startX, startY, id);
				}
				else if (ALGO == FED)
				{
					agent = new algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed(m, startX, startY, id);
				}
				agents.add(agent);
			}		
			
			addKeyListener(this);
			setFocusable(true);	
		}
		
		else if (RUNMODE == AUTORUN)
		{
			try 
			{
				if (ALGO == BM)
					writeAlgorithmData(BM, END_STATE, mapsCategory);
				else if (ALGO == BMI)
					writeAlgorithmData(BMI, END_STATE, mapsCategory);
				else if (ALGO == BMILRV)
					writeAlgorithmData(BMILRV, END_STATE, mapsCategory);
				else if (ALGO == BMILRVSM)
					writeAlgorithmData(BMILRVSM, END_STATE, mapsCategory);
				else if (ALGO == FEG)
					writeAlgorithmData(FEG, END_STATE, mapsCategory);
				else if (ALGO == FED)
					writeAlgorithmData(FED, END_STATE, mapsCategory);
			}
			catch (Exception e) 
			{
				e.printStackTrace();	
			}
		}
	}
	
	public void paint(Graphics g)
	{
		bufferedPaint(g);
	}
	
	/** The buffered paint method (avoids flickering of the screen) */
	public void bufferedPaint(Graphics g)
	{
		//g.drawImage(m.getMapImage(), 0, 0, this);
		BufferedImage tmpDrawAgents = drawAgents(); 
		g.drawImage(tmpDrawAgents, 0, 0, this);
		
		// Record animation if RECORD_GIF options is true
		if (RECORD_GIF)
		{
			gif.addFrame(tmpDrawAgents);
		}
		//gif.addFrame(image2);
		
		if 	((ALGO == BM) || 
			 (ALGO == BMI))
		{
			// Draw step number
			g.fillRect(	//BMnewMap.getMapWidth()*BMnewMap.getCellWidth(),
						BMnewMap.getMapWidth()*Constants.CELL_WIDTH,
						0, //m.getMapHeight()*m.getCellHeight(),
						100,
						100);
			g.setColor(Color.GREEN);
				g.drawString("Step: "+ step, 
				//BMnewMap.getMapWidth()*BMnewMap.getCellWidth() + 20, 
				BMnewMap.getMapWidth()*Constants.CELL_WIDTH + 20,
				50);
		}
		else if ((ALGO == BMILRV) || (ALGO == BMILRVSM) || 
				 (ALGO == FEG) || (ALGO == FED))
		{
			// Draw step number
			g.fillRect(	//BMnewMap.getMapWidth()*BMnewMap.getCellWidth(),
						m.getMapWidth()*Constants.CELL_WIDTH,
						0, //m.getMapHeight()*m.getCellHeight(),
						100,
						100);
			g.setColor(Color.GREEN);
				g.drawString("Step: "+ step,  
				m.getMapWidth()*Constants.CELL_WIDTH + 20,
				50);
		}
		else
		{
			// Draw step number
			g.fillRect(	m.getMapWidth()*m.getCellWidth(),
						0, //m.getMapHeight()*m.getCellHeight(),
						100,
						100);
			g.setColor(Color.GREEN);
				g.drawString("Step: "+ step, 
				m.getMapWidth()*m.getCellWidth() + 20, 
				50);
		}				
	}

	
	/** Draws the map according to the type of algorithm used */
	public BufferedImage drawAgents()
	{
		if ((ALGO == BM) || (ALGO == BMI))
		{
			return drawAgentsBM();
		}
		else if (ALGO == BMILRV)
		{
			return drawAgentsBMILRV();
		}
		else if (ALGO == BMILRVSM)
		{
			return drawAgentsBMILRV();
		}
		else if (ALGO == FEG)
		{
			return drawAgentsFEG();
		}
		else if (ALGO == FED)
		{
			return drawAgentsFED(); // TODO use a new function here, to color-differentiate the frontiers in FED
		}
		else 
		{
			return drawAgentsBMILRV();
		}
		
		// TODO for agent type change		
	}
	
	
	/** Write a given data into a given file */
	private void writeData(String additionalFolderPath,
			String fileName, String output) 
	throws IOException
	{
		// Create a results folder using the current date
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");// HH.mm.ss");
		Calendar calendar = Calendar.getInstance();
		//System.out.println(dateFormat.format(calendar.getTime()));
		String resultsFolderName = "" + dateFormat.format(calendar.getTime());
			
		// Create the destination folder
		String filePath = "../src/" + resultsFolderName + additionalFolderPath ;
		File file = new File(filePath);
		file.mkdirs();
		// Create the destination file
		filePath = filePath + "/" + fileName;
		//file = new File(filePath);
		//file.createNewFile();
		
	    Writer out = 
	    		new OutputStreamWriter(new FileOutputStream(filePath));
	    try 
	    {
	    	out.write(output);
	    }
	    finally 
	    {
	    	out.close();
	    }		
	}	
	
	
	/** Generic algorithm testing 
	 * INPUT: algorithmID and endState for the simulation
	 * */
	private void writeAlgorithmData	(int algorithmID, int endState, 
			String mapCategoryName) 
	throws IOException  
	{
		/** 
		 * Function Plan:
		 * 1. Given a number of agents
		 * 2. 	For all the maps of a given type
		 * 3. 		For 1 to TOTAL_RUNS runs
		 * 4. 			Record the min, avg and max exploration time and cover time
		 *  
		 * 5. Save the results on a plot, having:
		 * 		X-axis = Map size
		 * 		Y-axis = Number of steps
		 * */
		
		String agentClassName = ""; // Is filled lower in this function
		//String fileName = "agents" + agentsNumber + "_map" + mapID + "_explore";
		String fileNameClosure = "";
		String fileNameExplore = "";
		String fileNameTotalSteps = "";
		int loopedRuns = 0;
		String loopedRunsLocation = "";
		
		// Declare these here, only if you are interested in varying the size of the environment
		String outputExplore = "";
		String outputClosure = "";
		String outputSteps = "";
	
		// For all given agent numbers
		for (int agentsNumber=MIN_AGENTS; agentsNumber <= MAX_AGENTS; agentsNumber++)
		{
			// for all the given maps
			for (int mapID = 0; mapID<mapsPaths.size(); mapID++)
			{
				int mapSurface = -1; // The map surface will be written in the log file
				
				explorationTimes.clear();
				closureTimes.clear();
				totalAgentSteps.clear();
				
				/** Executed at every run */
				for (int i=0; i<TOTAL_RUNS; i++)
				{
					// Launch execution
					int runResult = 1;
					// Try to execute while run result isn't normal
					while (0 != runResult) 		 		
					{
						/** Create the map used by the algorithm */						
						
						if (ALGO == BM)
						{
							// Create the 2 maps used by Brick&Mortar
							BMoldMap = new Map2D();
							BMnewMap = new Map2D();
							
							// Create the last step map
							BMoldMap.BMImageToMap2D(rootPath + 
									mapsPaths.get(mapID) + 
									mapExtension);
							
							// Create the current step map (for synchronized version)
							BMnewMap.BMImageToMap2D(rootPath + 
									mapsPaths.get(mapID) + 
									mapExtension);
							BMnewMap.setCellHeight(24);
							BMnewMap.setCellWidth(24);
										
							// Calculate map surface if not already done
							if (mapSurface == -1)
								mapSurface = BMoldMap.getTotalBlankCells();								
						}
						else if (ALGO == BMI)
						{
							// Create the 2 maps used by Brick&Mortar
							BMoldMap = new Map2D();
							BMnewMap = new Map2D();
							
							// Create the last step map
							BMoldMap.BMImageToMap2D(rootPath + 
									mapsPaths.get(mapID) + 
									mapExtension);
							
							// Create the current step map (for synchronized version)
							BMnewMap.BMImageToMap2D(rootPath + 
									mapsPaths.get(mapID) + 
									mapExtension);
							BMnewMap.setCellHeight(24);
							BMnewMap.setCellWidth(24);
							
							/** BMI */
							BMoldMap.setPhysicalMapValue(0, 0, Constants.BMI_ATTRACTOR);
							BMnewMap.setPhysicalMapValue(0, 0, Constants.BMI_ATTRACTOR);
							
							// Calculate map surface if not already done
							if (mapSurface == -1)
								mapSurface = BMoldMap.getTotalBlankCells();
						}
						else if ((ALGO == BMILRV) ||  (ALGO == BMILRVSM))
						{
							// Create the map used by BMILV
							m = new Map2D();
							
							// Create the last step map
							m.BMImageToMap2D(rootPath + 
									mapsPaths.get(mapID) + 
									mapExtension);
							
							if ((rdvX != -1) && (rdvY != -1))
								m.setPhysicalMapValue(rdvX, rdvY, Constants.BMI_ATTRACTOR);							
							
							// Calculate map surface if not already done
							if (mapSurface == -1)
								mapSurface = m.getTotalBlankCells();
						}
						else if ((ALGO == FEG) || (ALGO == FED))
						{
							// Create the map used by BMILV
							m = new Map2D();
							
							// Create the last step map
							m.BMImageToMap2D(rootPath + 
									mapsPaths.get(mapID) + 
									mapExtension);
							
							// WARNING: the RDV point is embedded in the code of FrontierExplorationGreedy (FEG)
							//if ((rdvX != -1) && (rdvY != -1))
							//	m.setPhysicalMapValue(rdvX, rdvY, Constants.BMI_ATTRACTOR);							
							
							// Calculate map surface if not already done
							if (mapSurface == -1)
								mapSurface = m.getTotalBlankCells();
						}
						
						// Adding agents
						agents.clear();
						for (int id=0; id<agentsNumber; id++)
						{
							// Change agent type here (for the simulation)
							Agent agent = new Agent();
							
							if (ALGO == BM)
							{
								agent = new BrickMortarClassic(BMoldMap, BMnewMap, startX, startY, id);
							}
							else if (ALGO == BMI)
							{
								agent = new BrickMortarImproved(BMoldMap, BMnewMap, startX, startY, id);
							}
							else if (ALGO == BMILRV)
							{
								agent = new algorithms.BrickMortar_NewImplementation.BMILRV(m, startX, startY, id);
							}
							else if (ALGO == BMILRVSM)
							{
								agent = new algorithms.BrickMortar_NewImplementation.BMILRVSM(m, startX, startY, id);
							}
							else if (ALGO == FEG)
							{
								agent = new algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy(m, startX, startY, id);
							}
							else if (ALGO == FED)
							{
								agent = new algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed(m, startX, startY, id);
							}
							agents.add(agent);
						}		
						
						// Extract the precise algorithm name
						agentClassName = agents.get(0).getClass().getName();
						// Leave only the class name, without its full path
						agentClassName = agentClassName.substring(agentClassName.lastIndexOf(".")+1);
						
						fileNameExplore = agentClassName + "_" + mapCategoryName + "_explore";
						fileNameClosure = agentClassName + "_" + mapCategoryName + "_visit";
						fileNameTotalSteps = agentClassName + "_" + mapCategoryName + "_steps";
						
						if ((ALGO == BM) || (ALGO == BMI) || (ALGO == BMILRV) || (ALGO == BMILRVSM) || (ALGO == FEG) || (ALGO == FED))
						{
							// Report an execution result
							runResult = 
								BMexecuteTill(endState,
								"\n\n" +
								"Map: " + (mapID+1) + "/" + mapsPaths.size() + "\n" +
								"agents: " + agentsNumber + "/" + MAX_AGENTS + "\n" +
								"run " + (i+1) + "/" + TOTAL_RUNS + "\n");
						}
					
						// Report a run that takes too long to execute
						// (that has reached the imposed upper barrier of time
						if (runResult == 1)
						{ 
							loopedRuns++;
							loopedRunsLocation += 
									"Map: " + (mapID+1) + "/" + mapsPaths.size() + "\n" +
									"agents: " + agentsNumber + "/" + MAX_AGENTS + "\n" +
									"run " + (i+1) + "/" + TOTAL_RUNS + "\n";
						}
						
					}	// end of while EndState not reached loop
				}	// end of TOTAL_RUNS loop
				
				// Calculating the min, avg, max times for
				//	exploration and termination
				int minVisit = 0;
				int maxVisit = 0;
				float avgVisit = 0;
				float totalVisit = 0;
				if ((ALGO == BM) || (ALGO == BMI) || (ALGO == BMILRV) || (ALGO == BMILRVSM) || (ALGO == FEG) || (ALGO == FED))
				{
					minVisit = closureTimes.elementAt(0);
					maxVisit = closureTimes.elementAt(0);					
				}
				
				int minExplore = explorationTimes.elementAt(0);
				int maxExplore = explorationTimes.elementAt(0);
				float avgExplore = (float) 0.0;
				float totalExplore = (float) 0.0;
				
				int minTotalSteps = totalAgentSteps.elementAt(0);
				int maxTotalSteps = totalAgentSteps.elementAt(0);
				float avgTotalAgentsSteps = (float) 0.0;
				float sumTotalSteps = (float) 0.0;
				
				// Write exploration and visit time data 
				for (int runs=0; runs<TOTAL_RUNS; runs++)
				{
					System.out.println("Run " + (runs+1));
					System.out.println("  Exploration " + explorationTimes.elementAt(runs));
					
					// Calculate exploration times
					if (explorationTimes.elementAt(runs) < minExplore)
						minExplore = explorationTimes.elementAt(runs);					
					if (explorationTimes.elementAt(runs) > maxExplore)
						maxExplore = explorationTimes.elementAt(runs);
					totalExplore += explorationTimes.elementAt(runs);

					// Calculate visit times
					if ((ALGO == BM) ||  
						(ALGO == BMI) ||
						(ALGO == BMILRV) || 
						(ALGO == BMILRVSM) || 
						(ALGO == FEG) || 
						(ALGO == FED))
					{
						System.out.println("  Closure " + closureTimes.elementAt(runs));
						
						if (closureTimes.elementAt(runs) < minVisit)
							minVisit = closureTimes.elementAt(runs);						
						if (closureTimes.elementAt(runs) > maxVisit)
							maxVisit = closureTimes.elementAt(runs);	
						totalVisit += closureTimes.elementAt(runs);
					}
					
					// Calculate total steps
					if (totalAgentSteps.elementAt(runs) < minTotalSteps)
						minTotalSteps = totalAgentSteps.elementAt(runs);	
					if (totalAgentSteps.elementAt(runs) > maxTotalSteps)
						maxTotalSteps = totalAgentSteps.elementAt(runs);
					sumTotalSteps += totalAgentSteps.elementAt(runs);
				} // end of TOTAL_RUNS loop
				avgExplore = totalExplore/((float) TOTAL_RUNS);
				avgVisit = totalVisit/((float) TOTAL_RUNS);
				avgTotalAgentsSteps = sumTotalSteps/((float) TOTAL_RUNS);

				// Once the average is calculated
				// 	calculate the standard deviation
				double visitDeviation = 0;
				double explorationDeviation = 0;
				double totalAgentsStepsDeviation = 0;
				for(int runs=0; runs<TOTAL_RUNS; runs++)
				{
					explorationDeviation += 
							Math.pow((explorationTimes.elementAt(runs) - avgExplore), 2);
					totalAgentsStepsDeviation +=
							Math.pow((totalAgentSteps.elementAt(runs) - avgTotalAgentsSteps), 2);
					
					if ((ALGO == BM) || (ALGO == BMI) || (ALGO == BMILRV) || (ALGO == BMILRVSM) || (ALGO == FEG) || (ALGO == FED))
					{
						visitDeviation +=
							Math.pow((closureTimes.elementAt(runs) - avgVisit), 2);
					}
				}
				explorationDeviation /= TOTAL_RUNS;
				explorationDeviation = Math.sqrt(explorationDeviation);
				visitDeviation /= TOTAL_RUNS;
				visitDeviation = Math.sqrt(explorationDeviation);
				totalAgentsStepsDeviation /= TOTAL_RUNS;
				totalAgentsStepsDeviation = Math.sqrt(totalAgentsStepsDeviation);				
				
				outputExplore += mapSurface + " "
						+ agentsNumber + " " 
						+ avgExplore + " "
						+ minExplore + " "
						+ maxExplore + " "
						+ (avgExplore-explorationDeviation) + " "
						+ (avgExplore+explorationDeviation) + " "
						+ "\n";			
				outputClosure += mapSurface + " "
						+ agentsNumber + " " 
						+ avgVisit + " "
						+ minVisit + " "
						+ maxVisit + " "
						+ (avgVisit-visitDeviation) + " "
						+ (avgVisit+visitDeviation) + " "
						+ "\n";
				outputSteps += mapSurface + " "
						+ agentsNumber + " " 
						+ avgTotalAgentsSteps + " "
						+ minTotalSteps + " "
						+ maxTotalSteps + " "
						+ (avgTotalAgentsSteps-totalAgentsStepsDeviation) + " "
						+ (avgTotalAgentsSteps+totalAgentsStepsDeviation) + " "
						+ "\n";
			} // end of FOR ALL MAPS loop
			// Write the data for every map category
			String folderPath = "/" + MIN_AGENTS + "/" + mapCategoryName + "/" + agentClassName + "/";
			writeData(folderPath, fileNameExplore, outputExplore);
			writeData(folderPath, fileNameClosure, outputClosure);
			writeData(folderPath, fileNameTotalSteps, outputSteps);
		} // end of FOR AGENTS_MIN .. AGENTS_MAX loop
		
		/** Gnuplot generation */
		generateGnuplot(agentClassName, mapCategoryName, fileNameExplore);
		
		// Report runs that took too long to compute 
		if (loopedRuns>0)
		{
			System.err.println("WARNING: total looped runs = " + loopedRuns);
			System.err.println(loopedRunsLocation);	
		}		
	}
	
	
	private void generateGnuplot(
			String agentClassName, 
			String mapCategoryName, 
			String fileNameExplore)
	{
		/** Gnuplot generation */
		String output ="";
		String gnuplotHeader = "";
		gnuplotHeader += "set term postscript eps enhanced color\n";
		gnuplotHeader += "set output \"" + agentClassName 
				+ MIN_AGENTS + "_" + MAX_AGENTS
				+ ".eps\"\n";
		//gnuplotHeader += "set key left top\n";
		gnuplotHeader += "set size 0.75,0.75\n";
		//#set term png size 1000, 750 #crop
		//gnuplotHeader += "#set terminal png	" +
			//	"# gnuplot recommends setting terminal before output";
		//gnuplotHeader += "#set output \"output.png\"	" +
		//	"# The output filename; to be set after setting terminal";
		gnuplotHeader += "set title \"" + mapCategoryName + " maps, "//agentClassName
				+ MIN_AGENTS + " agents"
				+ " (avg on " + TOTAL_RUNS + " runs)\"\n";
		gnuplotHeader += "set xlabel \"Environment size\"" + "\n";
		gnuplotHeader += "set ylabel \"Time-steps till full exploration\"" + "\n";
		//gnuplotHeader += "set xrange [0:" + (MAX_AGENTS+1) + "]" + "\n";
		gnuplotHeader += "set xtics nomirror " + "\n";
		//gnuplotHeader += "set yrange [0:800]" + "\n";
		
		// Adding linestyles for color differentiation
		for (int i=1; i<=10; i++)
			gnuplotHeader += "set style line " + i + " linetype " + i + " linewidth 1\n";
				
		//output += "plot  ";
		for (int mapID = 0; mapID<mapsPaths.size(); mapID++)
		{
			output = gnuplotHeader;
			output += "plot  ";			
			//output += "[0:" + (MAX_AGENTS+1) + "] 400/x title 'Absolute minimum coverage time'" +
			//		"with lines ls 9,\\\n";
			
			/* Structure:
			 * outputExplore += mapSurface + " " 
						+ agentsNumber + " " 
						+ avgExplore + " "
						+ minExplore + " "
						+ maxExplore + " "
						+ (avg-std_deviation)
						+ (avg+std_deviation)
						+ "\n";		
			 * */
			
			output += 
					"\"" + fileNameExplore+/*"_"+mapsPaths.get(mapID)+*/ "\"" +
							" using 1:3 " +
							"notitle with lines ls 1,\\\n" +
					"\"" + fileNameExplore+/*"_"+mapsPaths.get(mapID)+*/ "\"" +
							" using 1:3:4:5 " +
							//"title 'B&M Explore steps for map " + mapsPaths.get(mapID) + "' " +
							"title '" + agentClassName + "' " +
							"with yerrorbars ls 1\n";
			output += "pause -1 \"Hit any key to continue\"";
			try 
			{
				String folderPath = "/" + MIN_AGENTS + "/" + mapCategoryName + "/" + agentClassName + "/";
				writeData(folderPath,
						agentClassName+"_plot_" + mapCategoryName + ".gp", 
						output);
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}		
		
	}
		
	
	public BufferedImage drawAgentsBM()
	{
		BufferedImage result = BMnewMap.BMgetMapImage();
		Graphics g = result.getGraphics();
		
		int maxTraceSize = 0;
		for (int k=0; k<agents.size(); k++)
		{
			maxTraceSize = Math.max(
					maxTraceSize,
					agents.get(k).getLastTrip().size());
		}
		
		// This is max trace size		
		/*
		for (int i=0; i<maxTraceSize-1; i++)
		{	
			for (int k=0; k<agents.size(); k++)
			{
				if (agents.get(k).getLastTrip().size()-2 >=i)
				{
					// Draw the agent
					if (k%agents.size() == 0)					
						g.setColor(Color.BLUE);
					else if (k%agents.size() == 1)
						g.setColor(Color.ORANGE);
					else if (k%agents.size() == 2)
						g.setColor(Color.RED);
					else if (k%agents.size() == 3)
						g.setColor(Color.MAGENTA);
					
					// Draw their last trips
					Vector <Coordinate2D> v;
					v = agents.get(k).getLastTrip();
					
					Coordinate2D start = v.elementAt(i);
					Coordinate2D end = v.elementAt(i+1);
					
					g.drawLine(	start.getX()*m.getCellWidth() + m.getCellWidth()/2, 
								start.getY()*m.getCellHeight() + m.getCellHeight()/2, 
								end.getX()*m.getCellWidth() + m.getCellWidth()/2, 
								end.getY()*m.getCellHeight() + m.getCellHeight()/2);
					
					g.fillOval(
							v.elementAt(i).getX()*m.getCellWidth() + m.getCellWidth()/2 - pathSize/2,
							v.elementAt(i).getY()*m.getCellHeight() + m.getCellHeight()/2 - pathSize/2,
							pathSize, pathSize);
				}
			}
		}		
		*/
		
		
		// TODO update
		// Draw view radius if single agents
		//if (agents.size() == 0)
		for (int k=0; k<agents.size(); k++)
		{
			if (ALGO == BM)
			{
				// Draw the square top-view field of vision.
				Color color = new Color(200,10,10,50);
				//g.setColor(color);
				g.setColor(Color.red);
				
				// Get the agent for easier references.
				BrickMortarClassic BMIagent = (BrickMortarClassic) agents.get(k);
				g.drawRect(
						(int) ((BMIagent.getX() - BMIagent.getViewingRange())*Constants.CELL_WIDTH),
						(int) ((BMIagent.getY() - BMIagent.getViewingRange())*Constants.CELL_HEIGHT),
						(int) ((1+2*BMIagent.getViewingRange())*Constants.CELL_WIDTH), 
						(int) ((1+2*BMIagent.getViewingRange())*Constants.CELL_HEIGHT));
				//g.fillRect(				 
				//g.drawRect(
				//		(agents.get(k).getX() - ((BrickMortarImproved) agents.get(k)).getViewingRange())*Constants.CELL_WIDTH,
				//		(agents.get(k).getY() - ((BrickMortarImproved) agents.get(k)).getViewingRange())*Constants.CELL_HEIGHT,
				//		(1+2*agents.get(k).getViewingRange())*Constants.CELL_WIDTH, (1+2*agents.get(k).getViewingRange())*Constants.CELL_HEIGHT);
			}
			
			
			// 2015.01.28 Bresenham viewing
			if (ALGO != BM)
			{
				Color viewFieldColor = new Color (0, 0, 255, 32);
				g.setColor(viewFieldColor);
				System.out.println("Agent " + k + " position: [" + agents.get(k).getX() + ", " + agents.get(k).getY() + "]");
				g.fillArc(
						(int) ( 
								(	agents.get(k).getX() - 
									((BrickMortarImproved) agents.get(k)).getViewingRange())
								)
								* Constants.CELL_WIDTH, // x (origin)
						(int) ( 
								(	agents.get(k).getY() - 
									((BrickMortarImproved) agents.get(k)).getViewingRange())
								)
								* Constants.CELL_HEIGHT, // y (origin)							
						//(int) (((float) agents.get(k).getX() - ((BrickMortarImproved) agents.get(k)).getViewingRange() / 2.0) *Constants.CELL_WIDTH),  
						//(int) (((float) agents.get(k).getY() - ((BrickMortarImproved) agents.get(k)).getViewingRange() / 2.0) *Constants.CELL_WIDTH),
						//(int) agents.get(k).getY()*Constants.CELL_HEIGHT, // y (origin)
						(int) ( (((BrickMortarImproved) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_WIDTH), // rectangle width 
						(int) ( (((BrickMortarImproved) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_HEIGHT), // rectangle width
						(int) (
								((BrickMortarImproved) agents.get(k)).getOrientationAngle() - ((BrickMortarImproved) agents.get(k)).getVisionFieldArc()/2.0
							  ), //startAngle, 
						(int) ((BrickMortarImproved) agents.get(k)).getVisionFieldArc()
						);
			}
		}
		
		
		// Draw agents
		for (int k=0; k<agents.size(); k++)
		{
			if (k%agents.size() == 0)					
				g.setColor(Color.BLUE);
			else if (k%agents.size() == 1)
				g.setColor(Color.MAGENTA);
			else if (k%agents.size() == 2)
				g.setColor(Color.RED);
			else if (k%agents.size() == 3)
				g.setColor(Color.ORANGE);
			else if (k%agents.size() == 4)
				g.setColor(Color.CYAN);
			else if (k%agents.size() == 5)
				g.setColor(Color.DARK_GRAY);
			else if (k%agents.size() == 6)
				g.setColor(Color.PINK);
			else if (k%agents.size() == 7)
				g.setColor(Color.black);
			else if (k%agents.size() == 8)
				g.setColor(Color.yellow);
			else if (k%agents.size() == 9)
				g.setColor(Color.LIGHT_GRAY);
			else if (k%agents.size() == 10)
				g.setColor(Color.GREEN);
			else if (k%agents.size() == 11)
				g.setColor(Color.gray);
			//			else if (k%agents.size() == 12)
			//				g.setColor(Color.MAGENTA);
			//			else if (k%agents.size() == 13)
			//				g.setColor(Color.);
			//			else if (k%agents.size() == 14)
			//				g.setColor(Color.);
			
			Agent agent = agents.get(k);

			// Draw the agents with a little offset, so as to be able to see them
			//	when they are together on a single cell.
			int agentDrawingDispersion = (Constants.CELL_WIDTH - Constants.AGENT_SIZE) / 2;
			
			if (agents.size() > 1)
			{
				g.fillOval(	
							(int)
							(
							//agent.getX()*BMnewMap.getCellWidth() + BMnewMap.getCellWidth()/2 
							agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
							- agentSize/2 
							+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
							),
							(int)
							(
							//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
							agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
							- agentSize/2
							+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
							),
							agentSize, agentSize);
			}
			// If there is only one agent, draw it in the center of the tile.
			else if (agents.size() == 1)
			{
				g.fillOval(	
						(int)
						(
						agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
						- agentSize/2 
						//+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
						),
						(int)
						(
						//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
						agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
						- agentSize/2
						//+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
						),
						agentSize, agentSize);				
			}
		}
		
		// Draw agent exit routes
		for (int x=0; x<BMnewMap.getMapWidth(); x++)
		{
			for (int y=0; y<BMnewMap.getMapHeight(); y++)
			{
				for (int k=0; k<agents.size(); k++)
				{
					if (k%agents.size() == 0)					
						g.setColor(Color.BLUE);
					else if (k%agents.size() == 1)
						g.setColor(Color.ORANGE);
					else if (k%agents.size() == 2)
						g.setColor(Color.RED);
					else if (k%agents.size() == 3)
						g.setColor(Color.MAGENTA);
					
					String direction = "";
					
					if ((BMnewMap.BMgetNextCell(x, y, k).getY() == -1) ||
						(BMnewMap.BMgetNextCell(x, y, k).getX() == -1))
					{
						direction = "";
					}
					else if (BMnewMap.BMgetNextCell(x, y, k).getY() == y-1)
					{
						direction = "���";
					}
					else if (BMnewMap.BMgetNextCell(x, y, k).getX() == x+1)
					{
						direction = "���";
					}
					else if (BMnewMap.BMgetNextCell(x, y, k).getY() == y+1)
					{
						direction = "���";
					}
					else if (BMnewMap.BMgetNextCell(x, y, k).getX() == x-1)
					{
						direction = "���";
					}
					
					// Set color to black to get nice images for the article
					//g.setColor(Color.BLACK);
					/* Draw agents' movement direction */
					//g.drawString(direction, 
						//	x*m.getCellWidth() +(k+1)*m.getCellWidth()/(agents.size()+1), 
							//y*m.getCellHeight() + (k+1)*m.getCellHeight()/(agents.size()+1));							
				}
				
				// Draw the agent ID trace				
				if (BMnewMap.BMgetIDsMap(x,y) != Constants.BM_NO_AGENT_TRACE) // if there is an ID trace on this cell
				{							  // then draw it
					g.setColor(Color.BLACK);
					//g.setColor(Color.red);
					g.drawString(BMnewMap.BMgetIDsMap(x,y)+"", 
							//x*BMnewMap.getCellWidth(),
							//(y+1)*BMnewMap.getCellHeight() - 2);
							x*Constants.CELL_WIDTH,
							(y+1)*Constants.CELL_HEIGHT - 2);
				}			
			}
		}

		
		return result;
	}
	
	
	
	

	public BufferedImage drawAgentsBMILRV()
	{
		BufferedImage result = m.BMgetMapImage();
		Graphics g = result.getGraphics();
		
		/*
		int maxTraceSize = 0;
		for (int k=0; k<agents.size(); k++)
		{
			maxTraceSize = Math.max(
					maxTraceSize,
					agents.get(k).getLastTrip().size());
		}
		*/
		
		for (int k=0; k<agents.size(); k++)
		{
			/*
			// Draw the square top-view field of vision.
			Color color = new Color(200,10,10,50);
			//g.setColor(color);
			g.setColor(Color.red);
			
			// Get the agent for easier references.
			algorithms.BrickMortar_NewImplementation.BMI_LongVision BMIagent = (algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k);
			g.drawRect(
					(int) ((BMIagent.getX() - BMIagent.getViewingRange())*Constants.CELL_WIDTH),
					(int) ((BMIagent.getY() - BMIagent.getViewingRange())*Constants.CELL_HEIGHT),
					(int) ((1+2*BMIagent.getViewingRange())*Constants.CELL_WIDTH), 
					(int) ((1+2*BMIagent.getViewingRange())*Constants.CELL_HEIGHT));
			*/
			
			int agentViewingRange = 0;
			float orientationAngle = 0;
			float visionFieldArc = 0;
			
			if (ALGO == BMILRV)
			{
				agentViewingRange = (int) ((algorithms.BrickMortar_NewImplementation.BMILRV) agents.get(k)).getViewingRange();
				orientationAngle = ((algorithms.BrickMortar_NewImplementation.BMILRV) agents.get(k)).getOrientationAngle();
				visionFieldArc = ((algorithms.BrickMortar_NewImplementation.BMILRV) agents.get(k)).getVisionFieldArc();
			}
			else if (ALGO == BMILRVSM)
			{
				agentViewingRange = (int) ((algorithms.BrickMortar_NewImplementation.BMILRVSM) agents.get(k)).getViewingRange();
				orientationAngle = ((algorithms.BrickMortar_NewImplementation.BMILRVSM) agents.get(k)).getOrientationAngle();
				visionFieldArc = ((algorithms.BrickMortar_NewImplementation.BMILRVSM) agents.get(k)).getVisionFieldArc(); 
			}
							
			// 2015.01.28 Bresenham viewing
			Color viewFieldColor = new Color (0, 0, 255, 32);
			g.setColor(viewFieldColor);
			System.out.println("Agent " + k + " position: [" + agents.get(k).getX() + ", " + agents.get(k).getY() + "]");
			g.fillArc(
					(int) ( agents.get(k).getX() - agentViewingRange) * Constants.CELL_WIDTH, // x (origin)
					(int) ( agents.get(k).getY() -  agentViewingRange) * Constants.CELL_HEIGHT, // y (origin)							
					(int) ( (agentViewingRange * 2 + 1) * Constants.CELL_WIDTH), // rectangle width 
					(int) ( (agentViewingRange * 2 + 1) * Constants.CELL_HEIGHT), // rectangle width
					(int) ( orientationAngle - visionFieldArc/2.0), //startAngle, 
					(int) visionFieldArc
					);	
		}
		
		
		// Draw agents
		for (int k=0; k<agents.size(); k++)
		{
			if (k%agents.size() == 0)					
				g.setColor(Color.BLUE);
			else if (k%agents.size() == 1)
				g.setColor(Color.MAGENTA);
			else if (k%agents.size() == 2)
				g.setColor(Color.RED);
			else if (k%agents.size() == 3)
				g.setColor(Color.ORANGE);
			else if (k%agents.size() == 4)
				g.setColor(Color.CYAN);
			else if (k%agents.size() == 5)
				g.setColor(Color.DARK_GRAY);
			else if (k%agents.size() == 6)
				g.setColor(Color.PINK);
			else if (k%agents.size() == 7)
				g.setColor(Color.black);
			else if (k%agents.size() == 8)
				g.setColor(Color.yellow);
			else if (k%agents.size() == 9)
				g.setColor(Color.LIGHT_GRAY);
			else if (k%agents.size() == 10)
				g.setColor(Color.GREEN);
			else if (k%agents.size() == 11)
				g.setColor(Color.gray);
			//			else if (k%agents.size() == 12)
			//				g.setColor(Color.MAGENTA);
			//			else if (k%agents.size() == 13)
			//				g.setColor(Color.);
			//			else if (k%agents.size() == 14)
			//				g.setColor(Color.);
			
			Agent agent = agents.get(k);

			// Draw the agents with a little offset, so as to be able to see them
			//	when they are together on a single cell.
			int agentDrawingDispersion = (Constants.CELL_WIDTH - Constants.AGENT_SIZE) / 2;
			
			if (agents.size() > 1)
			{
				g.fillOval(	
							(int)
							(
							//agent.getX()*BMnewMap.getCellWidth() + BMnewMap.getCellWidth()/2 
							agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
							- agentSize/2 
							+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
							),
							(int)
							(
							//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
							agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
							- agentSize/2
							+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
							),
							agentSize, agentSize);
			}
			// If there is only one agent, draw it in the center of the tile.
			else if (agents.size() == 1)
			{
				g.fillOval(	
						(int)
						(
						agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
						- agentSize/2 
						//+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
						),
						(int)
						(
						//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
						agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
						- agentSize/2
						//+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
						),
						agentSize, agentSize);				
			}
		}
		
		// Draw agent exit routes
		for (int x=0; x<m.getMapWidth(); x++)
		{
			for (int y=0; y<m.getMapHeight(); y++)
			{
				for (int k=0; k<agents.size(); k++)
				{
					if (k%agents.size() == 0)					
						g.setColor(Color.BLUE);
					else if (k%agents.size() == 1)
						g.setColor(Color.ORANGE);
					else if (k%agents.size() == 2)
						g.setColor(Color.RED);
					else if (k%agents.size() == 3)
						g.setColor(Color.MAGENTA);
					
					String direction = "";
					
					Coordinate2D nextCell = m.BMgetNextCell(x, y, k); 
					
					if ((nextCell.getY() == -1) ||
						(nextCell.getX() == -1))
					{
						direction = "";
					}
					else if (nextCell.getY() == y-1)
					{
						direction = "^";
					}
					else if (nextCell.getX() == x+1)
					{
						direction = ">";
					}
					else if (nextCell.getY() == y+1)
					{
						direction = "v";
					}
					else if (nextCell.getX() == x-1)
					{
						direction = "<";
					}
					
					// Set color to black to get nice images for the article
					//g.setColor(Color.BLACK);
					/* Draw agents' movement direction */
					//g.drawString(direction, 
						//	x*m.getCellWidth() +(k+1)*m.getCellWidth()/(agents.size()+1), 
							//y*m.getCellHeight() + (k+1)*m.getCellHeight()/(agents.size()+1));							
				}
				
				// Draw the agent ID trace				
				if (m.BMgetIDsMap(x,y) != Constants.BM_NO_AGENT_TRACE) // if there is an ID trace on this cell
				{							  // then draw it
					/*
					int k = m.BMgetIDsMap(x,y);
					
					if (k%agents.size() == 0)					
						g.setColor(Color.BLUE);
					else if (k%agents.size() == 1)
						g.setColor(Color.MAGENTA);
					else if (k%agents.size() == 2)
						g.setColor(Color.RED);
					else if (k%agents.size() == 3)
						g.setColor(Color.ORANGE);
					else if (k%agents.size() == 4)
						g.setColor(Color.CYAN);
					else if (k%agents.size() == 5)
						g.setColor(Color.DARK_GRAY);
					else if (k%agents.size() == 6)
						g.setColor(Color.PINK);
					else if (k%agents.size() == 7)
						g.setColor(Color.black);
					else if (k%agents.size() == 8)
						g.setColor(Color.yellow);
					else if (k%agents.size() == 9)
						g.setColor(Color.LIGHT_GRAY);
					else if (k%agents.size() == 10)
						g.setColor(Color.GREEN);
					else if (k%agents.size() == 11)
						g.setColor(Color.gray);
					
					int agentDrawingDispersion = (Constants.CELL_WIDTH - Constants.AGENT_SIZE) / 2;
					
					g.fillOval(	
							(int)
							(
							x*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
							- agentSize/2 
							+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
							),
							(int)
							(
							y*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
							- agentSize/2
							+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
							),
							agentSize/2, agentSize/2);
					
					*/
					// Draw the IDs in text mode
					g.setColor(Color.BLACK);
					//g.setColor(Color.red);
					g.drawString(m.BMgetIDsMap(x,y)+"", 
							//x*BMnewMap.getCellWidth(),
							//(y+1)*BMnewMap.getCellHeight() - 2);
							x*Constants.CELL_WIDTH,
							(y+1)*Constants.CELL_HEIGHT - 2);
				}			
			}
		}

		
		return result;
	}
	
	
	public BufferedImage drawAgentsFEG()
	{
		BufferedImage result = m.BMgetMapImage();
		Graphics g = result.getGraphics();
		
		for (int k=0; k<agents.size(); k++)
		{
			// 2015.01.28 Bresenham viewing
			Color viewFieldColor = new Color (0, 0, 255, 32);
			g.setColor(viewFieldColor);
			System.out.println("Agent " + k + " position: [" + agents.get(k).getX() + ", " + agents.get(k).getY() + "]");
			g.fillArc(
					(int) ( 
							(	agents.get(k).getX() - 
								((algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy) agents.get(k)).getViewingRange())
							)
							* Constants.CELL_WIDTH, // x (origin)
					(int) ( 
							(	agents.get(k).getY() - 
								((algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy) agents.get(k)).getViewingRange())
							)
							* Constants.CELL_HEIGHT, // y (origin)							
					(int) ( (((algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_WIDTH), // rectangle width 
					(int) ( (((algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_HEIGHT), // rectangle width
					(int) (
							((algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy) agents.get(k)).getOrientationAngle() - 
							((algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy) agents.get(k)).getVisionFieldArc()/2.0
						  ), //startAngle, 
					(int) ((algorithms.BrickMortar_NewImplementation.FrontierExploratorGreedy) agents.get(k)).getVisionFieldArc()
					);	
		}
		
		
		// Draw agents
		for (int k=0; k<agents.size(); k++)
		{
			if (k%agents.size() == 0)					
				g.setColor(Color.BLUE);
			else if (k%agents.size() == 1)
				g.setColor(Color.MAGENTA);
			else if (k%agents.size() == 2)
				g.setColor(Color.RED);
			else if (k%agents.size() == 3)
				g.setColor(Color.ORANGE);
			else if (k%agents.size() == 4)
				g.setColor(Color.CYAN);
			else if (k%agents.size() == 5)
				g.setColor(Color.DARK_GRAY);
			else if (k%agents.size() == 6)
				g.setColor(Color.PINK);
			else if (k%agents.size() == 7)
				g.setColor(Color.black);
			else if (k%agents.size() == 8)
				g.setColor(Color.yellow);
			else if (k%agents.size() == 9)
				g.setColor(Color.LIGHT_GRAY);
			else if (k%agents.size() == 10)
				g.setColor(Color.GREEN);
			else if (k%agents.size() == 11)
				g.setColor(Color.gray);
			//			else if (k%agents.size() == 12)
			//				g.setColor(Color.MAGENTA);
			//			else if (k%agents.size() == 13)
			//				g.setColor(Color.);
			//			else if (k%agents.size() == 14)
			//				g.setColor(Color.);
			
			Agent agent = agents.get(k);

			// Draw the agents with a little offset, so as to be able to see them
			//	when they are together on a single cell.
			int agentDrawingDispersion = (Constants.CELL_WIDTH - Constants.AGENT_SIZE) / 2;
			
			if (agents.size() > 1)
			{
				g.fillOval(	
							(int)
							(
							//agent.getX()*BMnewMap.getCellWidth() + BMnewMap.getCellWidth()/2 
							agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
							- agentSize/2 
							+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
							),
							(int)
							(
							//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
							agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
							- agentSize/2
							+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
							),
							agentSize, agentSize);
			}
			// If there is only one agent, draw it in the center of the tile.
			else if (agents.size() == 1)
			{
				g.fillOval(	
						(int)
						(
						agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
						- agentSize/2 
						//+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
						),
						(int)
						(
						//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
						agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
						- agentSize/2
						//+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
						),
						agentSize, agentSize);				
			}
		}
				
		return result;
	}
	
	
	public BufferedImage drawAgentsFED()
	{
		BufferedImage result = m.BMgetMapImage();
		Graphics g = result.getGraphics();
		
		for (int k=0; k<agents.size(); k++)
		{
			// 2015.01.28 Bresenham viewing
			Color viewFieldColor = new Color (0, 0, 255, 32);
			g.setColor(viewFieldColor);
			System.out.println("Agent " + k + " position: [" + agents.get(k).getX() + ", " + agents.get(k).getY() + "]");
			g.fillArc(
					(int) ( 
							(	agents.get(k).getX() - 
								((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(k)).getViewingRange())
							)
							* Constants.CELL_WIDTH, // x (origin)
					(int) ( 
							(	agents.get(k).getY() - 
								((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(k)).getViewingRange())
							)
							* Constants.CELL_HEIGHT, // y (origin)							
					(int) ( (((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_WIDTH), // rectangle width 
					(int) ( (((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_HEIGHT), // rectangle width
					(int) (
							((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(k)).getOrientationAngle() - 
							((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(k)).getVisionFieldArc()/2.0
						  ), //startAngle, 
					(int) ((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(k)).getVisionFieldArc()
					);	
		}
		
		
		// Draw agents
		for (int k=0; k<agents.size(); k++)
		{
			if (k%agents.size() == 0)					
				g.setColor(Color.BLUE);
			else if (k%agents.size() == 1)
				g.setColor(Color.MAGENTA);
			else if (k%agents.size() == 2)
				g.setColor(Color.RED);
			else if (k%agents.size() == 3)
				g.setColor(Color.ORANGE);
			else if (k%agents.size() == 4)
				g.setColor(Color.CYAN);
			else if (k%agents.size() == 5)
				g.setColor(Color.DARK_GRAY);
			else if (k%agents.size() == 6)
				g.setColor(Color.PINK);
			else if (k%agents.size() == 7)
				g.setColor(Color.black);
			else if (k%agents.size() == 8)
				g.setColor(Color.yellow);
			else if (k%agents.size() == 9)
				g.setColor(Color.LIGHT_GRAY);
			else if (k%agents.size() == 10)
				g.setColor(Color.GREEN);
			else if (k%agents.size() == 11)
				g.setColor(Color.gray);
			//			else if (k%agents.size() == 12)
			//				g.setColor(Color.MAGENTA);
			//			else if (k%agents.size() == 13)
			//				g.setColor(Color.);
			//			else if (k%agents.size() == 14)
			//				g.setColor(Color.);
			
			Agent agent = agents.get(k);

			// Draw the agents with a little offset, so as to be able to see them
			//	when they are together on a single cell.
			int agentDrawingDispersion = (Constants.CELL_WIDTH - Constants.AGENT_SIZE) / 2;
			
			if (agents.size() > 1)
			{
				g.fillOval(	
							(int)
							(
							//agent.getX()*BMnewMap.getCellWidth() + BMnewMap.getCellWidth()/2 
							agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
							- agentSize/2 
							+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
							),
							(int)
							(
							//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
							agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
							- agentSize/2
							+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
							),
							agentSize, agentSize);
			}
			// If there is only one agent, draw it in the center of the tile.
			else if (agents.size() == 1)
			{
				g.fillOval(	
						(int)
						(
						agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
						- agentSize/2 
						//+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
						),
						(int)
						(
						//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
						agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
						- agentSize/2
						//+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
						),
						agentSize, agentSize);				
			}
		}
				
		return result;
	}
	
	/** 
	 * Genereates a vector image of the agent and the floor
	 * */
	public void drawAgentsBMI_LongVision_VectorGraphics() throws IOException
	{
		String vectorImageFileName = "agentsImage.eps";
		FileOutputStream fosVectorImage = new FileOutputStream(vectorImageFileName);
		EpsGraphics floorVectorImageGraphics = null;
		
		if (ALGO == BM)
		{
			floorVectorImageGraphics = new EpsGraphics(
					"Agents Image", 
					fosVectorImage, 
					0, 
					0, 
					Constants.CELL_WIDTH * BMnewMap.getMapWidth(), 
					Constants.CELL_HEIGHT * BMnewMap.getMapHeight(), 
					ColorMode.COLOR_RGB
					//ColorMode.COLOR_CMYK
					);
			System.out.println("Creating an image of size: " + BMnewMap.getMapWidth() + " : " + BMnewMap.getMapHeight());
			
			BMnewMap.BMgetMapImageOnGraphics(floorVectorImageGraphics);
		}
		else if (ALGO != BM)
		{
			/* EpsGraphics */ floorVectorImageGraphics = new EpsGraphics(
					"Agents Image", 
					fosVectorImage, 
					0, 
					0, 
					Constants.CELL_WIDTH * m.getMapWidth(), 
					Constants.CELL_HEIGHT * m.getMapHeight(), 
					ColorMode.COLOR_RGB
					//ColorMode.COLOR_CMYK
					);
			System.out.println("Creating an image of size: " + m.getMapWidth() + " : " + m.getMapHeight());
			
			m.BMgetMapImageOnGraphics(floorVectorImageGraphics);
		}		
		
		/*
		// Draw step number
		floorVectorImageGraphics.fillRect(	//BMnewMap.getMapWidth()*BMnewMap.getCellWidth(),
					m.getMapWidth()*Constants.CELL_WIDTH,
					0, //m.getMapHeight()*m.getCellHeight(),
					100,
					100);
		floorVectorImageGraphics.setColor(Color.GREEN);
		floorVectorImageGraphics.drawString("Step: "+ step,  
			m.getMapWidth()*Constants.CELL_WIDTH + 20,
			50);
		*/
		
		// Draw agents' field of view		
		for (int k=0; k<agents.size(); k++)
		{
			/*
			// Draw the square top-view field of vision.
			Color color = new Color(200,10,10,50);
			//g.setColor(color);
			g.setColor(Color.red);
			
			// Get the agent for easier references.
			algorithms.BrickMortar_NewImplementation.BMI_LongVision BMIagent = (algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k);
			g.drawRect(
					(int) ((BMIagent.getX() - BMIagent.getViewingRange())*Constants.CELL_WIDTH),
					(int) ((BMIagent.getY() - BMIagent.getViewingRange())*Constants.CELL_HEIGHT),
					(int) ((1+2*BMIagent.getViewingRange())*Constants.CELL_WIDTH), 
					(int) ((1+2*BMIagent.getViewingRange())*Constants.CELL_HEIGHT));
			*/
			
			// 2015.01.28 Bresenham viewing
			/*
			Color viewFieldColor = new Color (0, 0, 255, 32);
			floorVectorImageGraphics.setColor(viewFieldColor);
			System.out.println("Agent " + k + " position: [" + agents.get(k).getX() + ", " + agents.get(k).getY() + "]");
			floorVectorImageGraphics.fillArc(
					(int) ( 
							(	agents.get(k).getX() - 
								((algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k)).getViewingRange())
							)
							* Constants.CELL_WIDTH, // x (origin)
					(int) ( 
							(	agents.get(k).getY() - 
								((algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k)).getViewingRange())
							)
							* Constants.CELL_HEIGHT, // y (origin)							
					(int) ( (((algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_WIDTH), // rectangle width 
					(int) ( (((algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k)).getViewingRange() * 2 + 1) * Constants.CELL_HEIGHT), // rectangle width
					(int) (
							((algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k)).getOrientationAngle() - 
							((algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k)).getVisionFieldArc()/2.0
						  ), //startAngle, 
					(int) ((algorithms.BrickMortar_NewImplementation.BMI_LongVision) agents.get(k)).getVisionFieldArc()
					);
			*/	
		}
		
		
		// Draw agents
		for (int k=0; k<agents.size(); k++)
		{
			if (k%agents.size() == 0)					
				floorVectorImageGraphics.setColor(Color.BLUE);
			else if (k%agents.size() == 1)
				floorVectorImageGraphics.setColor(Color.MAGENTA);
			else if (k%agents.size() == 2)
				floorVectorImageGraphics.setColor(Color.RED);
			else if (k%agents.size() == 3)
				floorVectorImageGraphics.setColor(Color.ORANGE);
			else if (k%agents.size() == 4)
				floorVectorImageGraphics.setColor(Color.CYAN);
			else if (k%agents.size() == 5)
				floorVectorImageGraphics.setColor(Color.DARK_GRAY);
			else if (k%agents.size() == 6)
				floorVectorImageGraphics.setColor(Color.PINK);
			else if (k%agents.size() == 7)
				floorVectorImageGraphics.setColor(Color.black);
			else if (k%agents.size() == 8)
				floorVectorImageGraphics.setColor(Color.yellow);
			else if (k%agents.size() == 9)
				floorVectorImageGraphics.setColor(Color.LIGHT_GRAY);
			else if (k%agents.size() == 10)
				floorVectorImageGraphics.setColor(Color.GREEN);
			else if (k%agents.size() == 11)
				floorVectorImageGraphics.setColor(Color.gray);
			
			Agent agent = agents.get(k);

			// Draw the agents with a little offset, so as to be able to see them
			//	when they are together on a single cell.
			int agentDrawingDispersion = (Constants.CELL_WIDTH - Constants.AGENT_SIZE) / 2;
			
			if (agents.size() > 1)
			{
				floorVectorImageGraphics.fillOval(	
							(int)
							(
							//agent.getX()*BMnewMap.getCellWidth() + BMnewMap.getCellWidth()/2 
							agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
							- agentSize/2 
							+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
							),
							(int)
							(
							//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
							agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
							- agentSize/2
							+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
							),
							agentSize, agentSize);
			}
			// If there is only one agent, draw it in the center of the tile.
			else if (agents.size() == 1)
			{
				floorVectorImageGraphics.fillOval(	
						(int)
						(
						agent.getX()*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
						- agentSize/2 
						//+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
						),
						(int)
						(
						//agent.getY()*BMnewMap.getCellHeight() + BMnewMap.getCellHeight()/2
						agent.getY()*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
						- agentSize/2
						//+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
						),
						agentSize, agentSize);				
			}
		}
		
		
		// Draw agent exit routes
		for (int x=0; x<m.getMapWidth(); x++)
		{
			for (int y=0; y<m.getMapHeight(); y++)
			{
				// Draw the agent ID trace				
				if (m.BMgetIDsMap(x,y) != Constants.BM_NO_AGENT_TRACE) // if there is an ID trace on this cell
				{							  // then draw it
					int k = m.BMgetIDsMap(x,y);
					
					if (k%agents.size() == 0)					
						floorVectorImageGraphics.setColor(Color.BLUE);
					else if (k%agents.size() == 1)
						floorVectorImageGraphics.setColor(Color.MAGENTA);
					else if (k%agents.size() == 2)
						floorVectorImageGraphics.setColor(Color.RED);
					else if (k%agents.size() == 3)
						floorVectorImageGraphics.setColor(Color.ORANGE);
					else if (k%agents.size() == 4)
						floorVectorImageGraphics.setColor(Color.CYAN);
					else if (k%agents.size() == 5)
						floorVectorImageGraphics.setColor(Color.DARK_GRAY);
					else if (k%agents.size() == 6)
						floorVectorImageGraphics.setColor(Color.PINK);
					else if (k%agents.size() == 7)
						floorVectorImageGraphics.setColor(Color.black);
					else if (k%agents.size() == 8)
						floorVectorImageGraphics.setColor(Color.yellow);
					else if (k%agents.size() == 9)
						floorVectorImageGraphics.setColor(Color.LIGHT_GRAY);
					else if (k%agents.size() == 10)
						floorVectorImageGraphics.setColor(Color.GREEN);
					else if (k%agents.size() == 11)
						floorVectorImageGraphics.setColor(Color.gray);
					
					int agentDrawingDispersion = (Constants.CELL_WIDTH - Constants.AGENT_SIZE) / 2;
					
					floorVectorImageGraphics.fillOval(	
							(int)
							(
							x*Constants.CELL_WIDTH + Constants.CELL_WIDTH/2
							- agentSize/2 
							+ agentDrawingDispersion * Math.sin( 2*Math.PI * (((double) k)/agents.size()) )
							),
							(int)
							(
							y*Constants.CELL_HEIGHT + Constants.CELL_HEIGHT / 2 
							- agentSize/2
							+ agentDrawingDispersion * Math.cos( 2*Math.PI * (((double) k)/agents.size()) )
							),
							agentSize/2, agentSize/2);
				}			
			}
		}

		
		floorVectorImageGraphics.flush();
		floorVectorImageGraphics.close();
		fosVectorImage.close();
		System.out.println("Vector image saved!");
	}
	
	
	public int BMexecuteTill(int endState, String input_map_run)
	{
		int fullExplore = 0;
		int fullVisit = 0;
		step=0;		
		
		int BMexplorationState = -1;
		
		if ((ALGO == BM) || (ALGO == BMI)) 
		{
			BMexplorationState = BMnewMap.BMexplorationState();
		}
		else if (ALGO == BMILRV) 
		{
			BMexplorationState = m.BMexplorationState();
		}
		else if ((ALGO == FEG) || (ALGO == FED))
		{
			BMexplorationState = m.FEexplorationState();			
		}
		
		// Always calculate both exploration termination 
		//	and exploration termination identification
		// Bad idea for BMS, which is not able to compute 
		// 	exploration termination
		while 	(	(BMexplorationState != endState)
						&&
					(BMexplorationState != Map2D.MAP_CLOSED)
				&&
				(step < MAX_STEPS))
		{
				// Print news every 1500 steps
				//if (step%150000==0)
				if (step%15000==0)
				{
					if ((ALGO == BM) || (ALGO == BMI)) 
					{
						System.out.println(input_map_run + ": " 
								+ (BMnewMap.BMgetVisitPercentage() * 100) 
								+ "% cells visited\n"
								+ "step: " + step + "/" + MAX_STEPS);
					}
					else if ((ALGO == BMILRV) || (ALGO == BMILRVSM) || (ALGO == FEG) || (ALGO == FED))
					{
						System.out.println(input_map_run + ": " 
								+ (m.BMgetVisitPercentage() * 100) 
								+ "% cells visited\n"
								+ "step: " + step + "/" + MAX_STEPS);
					}
					else if ((ALGO == FEG) || (ALGO == FED))
					{
						System.out.println(input_map_run + ": " 
								+ (m.FEgetExplorationPercentage() * 100) 
								+ "% cells explored\n"
								+ "step: " + step + "/" + MAX_STEPS);
					}
				}
				step++;
				
				// Update the old map (by copying the current new one into the old one)
				if ((ALGO == BM) || (ALGO == BMI))
				{
					BMnewMap.BMcopyMap(BMoldMap);
				}
				// Update the new map
				// -- nothing to do 
				
				if (ALGO == FED)
				{
					Viewer.FEDopenFrontiers = Viewer.FEDcalculateOpenFrontiers();
					Viewer.FEDassignFrontiersToAgentsGroupGreedy();
				}
					
				//System.out.println("\nStep " + step);
				
				// Force all the agents to step
				for (int k=0; k<agents.size(); k++)
				{		
					if (ALGO == BM)
					{
						((BrickMortarClassic) agents.elementAt(k)).BMsetOldMap(BMoldMap);
						((BrickMortarClassic) agents.elementAt(k)).BMsetCurrentMap(BMnewMap);
					}
					else if (ALGO == BMI)
					{
						((BrickMortarImproved) agents.elementAt(k)).BMsetOldMap(BMoldMap);
						((BrickMortarImproved) agents.elementAt(k)).BMsetCurrentMap(BMnewMap);
					}
					agents.elementAt(k).step();
				}
				
				// Recheck the exploration state
				// (recheck if the exploration is complete)
				if ((ALGO == BM) || (ALGO == BMI)) 
					BMexplorationState = BMnewMap.BMexplorationState();
				else if ((ALGO == BMILRV) || (ALGO == BMILRVSM))
					BMexplorationState = m.BMexplorationState();
				else if ((ALGO == FEG) || (ALGO == FED))
					BMexplorationState = m.FEexplorationState();
				
				//System.out.println("Exploration state: " + BMexplorationState );
				
				// Save the time-step when full exploration was achieved
				// (if it was achieved)
				if ((fullExplore == 0)
					&&
					(BMexplorationState == Map2D.MAP_EXPLORED))
				{
					fullExplore = step;
				}				
				//repaint();		
		}		
		
		System.out.println("BMexplorationState = " + BMexplorationState);
		
		// Save the time-step when termination of exploration was identified
		// (it it was identified)
		if ((fullVisit == 0) &&
			(BMexplorationState == Map2D.MAP_CLOSED))
		{
			fullVisit = step;
		}
				
		// Save the time-step when full exploration was achieved
		// (if it was achieved) OR (if termination of exploration was identified)
		if ((fullExplore == 0)
				&&
			((BMexplorationState == Map2D.MAP_CLOSED)
				||
			(BMexplorationState == Map2D.MAP_EXPLORED)))
		{
			fullExplore = step;
		}				
				
		// if total time-steps is greater or equal to the MAX_STEPS
		// then report an error, by returning 1
		if (step == MAX_STEPS)
		{
			return 1;
		}
		else
		{
			System.out.println("Full explore at step: " + fullExplore);
			System.out.println("Full closure at step: " + fullVisit);
			explorationTimes.add(fullExplore);
			closureTimes.add(fullVisit);
			
			/* Write down the total number of steps made by agents*/
			int localTotalAgentSteps = 0;
			for (int k=0; k<agents.size(); k++)
			{		
				localTotalAgentSteps += agents.elementAt(k).getAgentSteps();
			}
			totalAgentSteps.add(localTotalAgentSteps);
			System.out.println("Total agent steps : " + localTotalAgentSteps);
			
			return 0;
		}
	}
	
	
	public void keyPressed(KeyEvent e)
	{
		// Synchronous mode		
		if ((e.getKeyCode() == java.awt.event.KeyEvent.VK_SPACE) ||
			(e.getKeyCode() == KeyEvent.VK_SPACE))
		{	
			// Brick&Mortar
			//if (fullVisit == 0)
			{
				step++;
				System.out.println("\nStep " + step);
				
				if (ALGO == FED)
				{
					Viewer.FEDopenFrontiers = Viewer.FEDcalculateOpenFrontiers();
					Viewer.FEDassignFrontiersToAgentsGroupGreedy();
				}
				
				// Update the old map (by copying the current new one into the old one)
				if ((ALGO == BM) ||	(ALGO == BMI))
					BMnewMap.BMcopyMap(BMoldMap);
				// Update the new map
				// -- nothing to do 
				
				// Force all the agents to step
				for (int k=agents.size()-1; k>=0;  k--)
				{		
					if (ALGO == BM)
					{
						((BrickMortarClassic) agents.elementAt(k)).BMsetOldMap(BMoldMap);
						((BrickMortarClassic) agents.elementAt(k)).BMsetCurrentMap(BMnewMap);
					}
					else if (ALGO == BMI)
					{
						((BrickMortarImproved) agents.elementAt(k)).BMsetOldMap(BMoldMap);
						((BrickMortarImproved) agents.elementAt(k)).BMsetCurrentMap(BMnewMap);
					}
					agents.elementAt(k).step();
					
				}
				
				if (fullExplore == 0)					
				{
					if ( (ALGO == BM) && (BMnewMap.BMexplorationState() == Map2D.MAP_EXPLORED))
					{
						System.out.println("Full exploration at step " + step + " !");
						fullExplore = step;
						
						int max = agents.elementAt(0).getAgentSteps();
						for (int k=0; k<agents.size(); k++)
						{	
							if (agents.elementAt(k).getAgentSteps() > max)
								max = agents.elementAt(k).getAgentSteps();
						}
						//fullExplore = step;
						fullExplore = max;
						System.out.println("(Using max agents step) Full exploration at step " + step + " !");
					}				
				}
				
				if (fullVisit == 0)		
				{
					if ((ALGO == BM) && (BMnewMap.BMexplorationState() == Map2D.MAP_CLOSED))
					{
						System.out.println("Full visit at step " + step + " !");
						fullVisit = step;
						
						if (fullExplore == 0)					
						{
							fullExplore = step;
						}
					}
				}
				
				repaint();
			}
			
			if (fullExplore != 0)
			{
				System.out.println("Full exploration at step: " + fullExplore);	
			}
			if (fullVisit != 0)
			{		
				System.out.println("Full visit at step: " + fullVisit);
			}
			
			//System.out.println("Upper bound = 2 * " + m.AW2getTotalLinksInGraph());
		}	
		else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_S)
		{
			if (RECORD_GIF)
			{
				String outputFileName = "../src/results/animation.gif"; 
				 gif.start(outputFileName);
				 gif.setRepeat(10);
				 //gif.setDelay(1000);   // 1 frame per sec
				 gif.setDelay(200);   // 5 frames per sec				
			}
		}
		else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_D)
		{
			if (RECORD_GIF)
			{
				gif.finish();
			}
		}
		else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_UP)
		{
			agents.elementAt(0).stepUp();			
			repaint();
		}
		else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_DOWN)
		{
			agents.elementAt(0).stepDown();
			repaint();
		}
		else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_LEFT)
		{
			agents.elementAt(0).stepLeft();
			repaint();
		}
		else if (e.getKeyCode() == java.awt.event.KeyEvent.VK_RIGHT)
		{
			agents.elementAt(0).stepRight();
			repaint();
		}
		//  Save a vector image of the current tileDataUnit.
		else if (e.getKeyCode() == KeyEvent.VK_P)
		{			
			try 
			{
				if (ALGO == BM)
				{
					drawAgentsBMI_LongVision_VectorGraphics();
				}
				
				if (ALGO != BM)
				{
					drawAgentsBMI_LongVision_VectorGraphics();
				}					
			} 
			catch (IOException e1) {e1.printStackTrace();}
		}
	}
	
	public void keyReleased(KeyEvent e) {}
	public void keyTyped(KeyEvent e) {}
	
	/** Returns the total number of agents at a given spot */
	public static int countAgentsAt(int in_x, int in_y)
	{
		int result = 0;
		
		for (int i=0; i<agents.size(); i++)
		{
			if (agents.elementAt(i).isAt(in_x, in_y))
			{
				result += 1;
			}
		}
		
		return result;
	}
	
	/** Check map parameters (number of blank cells) */
	private void checkMapParameters()
	{	
		for (int i=0; i<mapsPaths.size(); i++)
		{
			m = new Map2D();
			m.LRTAImageToMap2D(
					rootPath + 
					mapsPaths.get(i) + 
					mapExtension);
			System.out.println("Map " + mapsPaths.get(i) + " has " 
					+ m.getTotalBlankCells() + " blank cells and "
					+ m.getTotalWallCells() + " wall cells");
		}
	}

	
	
	
	
	public static Vector <Agent> getAgents() {return agents;}
	public static Map2D getMapBMILV() {return m;}
	
	
	
	
	
	/** 
	 * Returns TRUE if there is no agent on the given coordinates.
	 * */
	private static boolean BMILVnoAgentOn(int xGlobal_in, int yGlobal_in)
	{
		for (int agentID=0; agentID < agents.size(); agentID+=1)
		{
			if (ALGO == BMILRV)
			{
				if (((algorithms.BrickMortar_NewImplementation.BMILRV) agents.get(agentID)).getGlobalCoordinates().equals(new Coordinate2D(xGlobal_in, yGlobal_in)))
				{
					// Say that there is an agent on the given coordinates.
					return false;
				}				
			}
			else if (ALGO == BMILRVSM)
			{
				if (((algorithms.BrickMortar_NewImplementation.BMILRVSM) agents.get(agentID)).getGlobalCoordinates().equals(new Coordinate2D(xGlobal_in, yGlobal_in)))
				{
					// Say that there is an agent on the given coordinates.
					return false;
				}
			}
		}
		
		// Else 
		return true;
	}
	
	
	/** 
	 * Returns true if there is no agent on a closed cell around the given coordinate
	 * (2015.02.09) 
	 * */
	public static boolean BMILV_noAgentOnClosedCellAround(int xGlobal_in, int yGlobal_in)
	{
		for (int	x =  Math.max(0, xGlobal_in - 1); 
					x <= Math.min(xGlobal_in+1, m.getMapWidth()-1); 
					x += 1)
		{
			for (int 	y =  Math.max(0, yGlobal_in -1); 
						y <= Math.min(yGlobal_in+1, m.getMapHeight()-1);
						y += 1)
			{
				if (	((x == xGlobal_in) || (y == yGlobal_in)) && // 4 directions
						((x != xGlobal_in) || (y != yGlobal_in)) ) // not same cell
				{
					if (	(m.getPhysicalMapValue(x, y) == Constants.BM_CLOSED)	&& // the cell is closed
							(!BMILVnoAgentOn(x,y)) // There is an agent on X Y
						)
					{
						if (Constants.DEBUG)
							System.err.println("Cannot close: agent on [" + xGlobal_in + ", " + yGlobal_in + "]");
						return false; // Say that there is an agent on a closed cell around the given coordinates.
					}
				}				
			}
		}
	
		// Say that there are no agents on closed cells around ...
		return true;
	}

	/** Returns the list of frontier assignments for Frontier Explorator Distributed */
	public static HashMap<Integer, Integer> FEDgetFrontierAssignments()
	{
		return FEDfrontierAssignments;
	}


	/** Returns the list of open frontiers in the environment. FED (Frontier Explorator Distributed) */
	public static Vector<Vector<Coordinate2D>> FEDgetOpenFrontiers()
	{
		return FEDopenFrontiers;
	}
	
	/** Calculates and returns the list of open frontiers */
	public static Vector<Vector<Coordinate2D>> FEDcalculateOpenFrontiers()
	{
		Vector<Vector<Coordinate2D>> frontiers = new Vector<Vector<Coordinate2D>>();
		
		double[][] dataMap = m.getDataMap();
		int[][] frontierAssignmentMap = new int[dataMap.length][dataMap[0].length];
		Vector<Coordinate2D> frontierCellsToAnalyze = new Vector<Coordinate2D>();
		final int ANALYZED_CELL = -13; // Check if this integer is not occupied by other values (normally not) 
		
		// Copy the map and count the number of frontier cells.
		for (int x=0; x<dataMap.length; x++)
		{
			for (int y=0; y<dataMap[0].length; y++)
			{
				frontierAssignmentMap[x][y] = (int) dataMap[x][y];
				if  (m.FEDisFrontierCell(new Coordinate2D(x,y)))
				{
					frontierCellsToAnalyze.add(new Coordinate2D(x,y));	
				}
			}
		}
		
		//System.out.println("There are " + frontierCellsToAnalyze.size() + " cells to analyze.");
		
		while (frontierCellsToAnalyze.size() > 0)
		{
			// Take the next frontier cell from the list of frontier cells
			Coordinate2D frontierCell = frontierCellsToAnalyze.get(0);
			frontierCellsToAnalyze.remove(frontierCell);
			frontierAssignmentMap[frontierCell.getX()][frontierCell.getY()] = ANALYZED_CELL;
			
			// Use it to generate a new frontier
			Vector<Coordinate2D> frontier = new Vector<Coordinate2D>();
			frontiers.add(frontier);
			frontier.add(frontierCell);
			
			Vector<Coordinate2D> cellsWithNonanalyzedNeighbors = new Vector<Coordinate2D>();
			cellsWithNonanalyzedNeighbors.add(frontierCell);
			
			// Propagate the frontier through the neighbors of cells.
			while (cellsWithNonanalyzedNeighbors.size() > 0)
			{
				Coordinate2D cellWithNeighborsToAnalyze = cellsWithNonanalyzedNeighbors.get(0);
				cellsWithNonanalyzedNeighbors.remove(0);
				
				// Add the neighbors of this cell
				for (int x=Math.max(0, cellWithNeighborsToAnalyze.getX()-1);
						 x<=Math.min(cellWithNeighborsToAnalyze.getX()+1, dataMap.length-1);
						 x+=1)
				{
					for (int y=Math.max(0, cellWithNeighborsToAnalyze.getY()-1);
							 y<=Math.min(cellWithNeighborsToAnalyze.getY()+1, dataMap[0].length-1);
							 y+=1)
					{
						if (	((x!=cellWithNeighborsToAnalyze.getX()) || (y!=cellWithNeighborsToAnalyze.getY())) && // not same cell
								((x==cellWithNeighborsToAnalyze.getX()) || (y==cellWithNeighborsToAnalyze.getY())) // 4 directions
							)
						{
							Coordinate2D candidateCell = new Coordinate2D(x,y);
							
							if (m.FEDisFrontierCell(candidateCell))
							{
								//System.out.println("Candidate cell is a frontier cell.");
								//if (frontierCellsToAnalyze.contains(candidateCell)) // doesn't work
								// if (doesThisListOfCellsContainThatCell(frontierCellsToAnalyze, candidateCell)) not optimized
								if (frontierAssignmentMap[candidateCell.getX()][candidateCell.getY()] != ANALYZED_CELL)
								{
									//System.out.println("... and it is an unanalyzed frontier cell.");
									// Add this cell to the current frontier
									frontier.add(candidateCell);
									
									// Remember this cell as analyzed
									frontierAssignmentMap[candidateCell.getX()][candidateCell.getY()] = ANALYZED_CELL;
									
									// Propagate the frontier through the neighbors of this cell
									cellsWithNonanalyzedNeighbors.add(candidateCell);
									
									// Remove this cell from the list of non-analyzed frontier cells (the ones that had to be groupe into frontiers)
									int size = frontierCellsToAnalyze.size();
									//frontierCellsToAnalyze.remove(candidateCell); // doesn't work
									FEDremoveCellFromListOfCells(frontierCellsToAnalyze, candidateCell);
									
									// Functional verification test
									if (frontierCellsToAnalyze.size() < size)
									{
										//System.out.println("Successfull treatment.");
									}
									else
									{
										//System.err.println("Failed to remove.");
									}
								}
								else
								{
									//System.out.println("... but it was already analyzed.");
								}
								
							}									
						}							
					}
				}
			}
		
		}				
		
		/*
		System.out.println("There are " + frontiers.size() + " frontiers.");
		for (int frontierID=0; frontierID < frontiers.size(); frontierID++)
		{
			Vector<Coordinate2D> frontierCells = frontiers.get(frontierID);
			System.out.println(frontierID + " frontier: ");
			
			for (int cellID=0; cellID < frontierCells.size(); cellID++)
			{
				frontierCells.get(cellID).print();
			}
		}
		*/
		
		return frontiers;		
	}
	
	private static boolean FEDdoesThisListOfCellsContainThatCell(Vector<Coordinate2D> list_in, Coordinate2D cell_in)
	{
		for (int cellID=0; cellID< list_in.size(); cellID+=1)
		{
			if (list_in.get(cellID).equals(cell_in))
				return true;
		}
			
		return false;
	}
	
	private static void FEDremoveCellFromListOfCells(Vector<Coordinate2D> list_in, Coordinate2D cell_in)
	{
		for (int cellID=0; cellID< list_in.size(); cellID+=1)
		{
			if (list_in.get(cellID).equals(cell_in))
			{
				list_in.remove(cellID);
				return;
			}
		}
	}

	
	/** Assigns frontiers to agents in a greedy manner.*/
	public static void FEDassignFrontiersToAgentsGroupGreedy()
	{
		// Get the existing frontier assignments 
		HashMap<Integer, Integer> frontierAssignments = Viewer.FEDgetFrontierAssignments();
		// Clear the existing frontier assignments
		for (int agentID=0; agentID < agents.size(); agentID +=1)
		{
			frontierAssignments.put(agentID, Constants.FED_FRONTIER_NOT_SET);
		}
		
		// Get the list of existing frontiers
		Vector<Vector<Coordinate2D>> frontiers = Viewer.FEDgetOpenFrontiers();
		//System.out.println("There are " + frontiers.size() + " frontiers.");
		// In the absence of frontiers, there is nothing to do.
		if (frontiers.size() == 0)
			return;
		
		// Generate a structure for the distance maps of all the agents
		HashMap<Integer, int[][]> agentsDistanceMaps = new HashMap<Integer, int[][]>();
		for (int agentID=0; agentID < agents.size(); agentID +=1)
		{
			int[][] distanceMap = ((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(agentID)).getDistanceMapFromAgent();
			agentsDistanceMaps.put(agentID, distanceMap);
		}
		
		// For all the pairs of frontiers and agents, choose the minimal distance.
		int[][] costMatrix = new int[agents.size()][frontiers.size()];
		// Calculate the cost matrix for all the agents and all frontiers
		// Calculate this cost matrix only once, and then remove columns (for frontiers that were assigned agents) and rows (for agents that were assigned frontiers).		
		for (int agentID = 0; agentID < agents.size(); agentID+=1)
		{
			for (int frontierID = 0; frontierID < frontiers.size(); frontierID+=1)
			{
				// Get the closest cell from the selected frontier to the selected agent 
				Coordinate2D closestCellFromAssignedFrontier = 
						((algorithms.BrickMortar_NewImplementation.FrontierExploratorDistributed) agents.get(agentID))
						.getClosestCellFromGivenFrontier(frontiers.get(frontierID));
								
				// Save this distance in the cost matrix
				costMatrix[agentID][frontierID] = agentsDistanceMaps.get(agentID)[closestCellFromAssignedFrontier.getX()][closestCellFromAssignedFrontier.getY()];
			}
		} 
		
		// Use here a group-greedy approach.
		// Assign the frontiers in augmenting cost order.
		while (!allAgentsHaveBeenAssignedFrontiers())
		{
			// Store the popularity of frontiers in a hash map
			HashMap<Integer, Integer> frontiersPopularity = new HashMap<Integer, Integer>();
			
			// Initialize the popularity of all frontiers to 0.
			for (int frontierID=0; frontierID < frontiers.size(); frontierID++)
			{
				frontiersPopularity.put(frontierID, 0);
			}
			
			// Calculate the popularity of each frontier.
			for (Map.Entry<Integer, Integer> entry : frontierAssignments.entrySet()) 
			{
			    //Integer agentID = entry.getKey(); // agent ID
			    Integer frontierID = entry.getValue(); // frontier id
			    
			    if (frontierID != Constants.FED_FRONTIER_NOT_SET)
			    {
			    	// Increment the popularity of the given frontier
				    int selectedFrontierPopularity = frontiersPopularity.get(frontierID);
				    frontiersPopularity.put(frontierID, selectedFrontierPopularity+1);
			    }
			}
			
			// Calculate the frontier with minimal popularity
			int minimalPopularity = Integer.MAX_VALUE;
			for (Map.Entry<Integer, Integer> entry : frontiersPopularity.entrySet())
			{
				//Integer frontierID = entry.getKey();
				Integer frontierPopularity = entry.getValue();
				
				if (frontierPopularity <= minimalPopularity)
				{
					minimalPopularity = frontierPopularity;
				}
			}
			
			// Create a list of frontiers with minimal popularity
			Vector<Integer> frontiersIDsWithMinimalPopularity = new Vector<Integer>();
			for (Map.Entry<Integer, Integer> entry : frontiersPopularity.entrySet())
			{
				Integer frontierID = entry.getKey();
				Integer frontierPopularity = entry.getValue();
				
				if (frontierPopularity == minimalPopularity)
				{
					frontiersIDsWithMinimalPopularity.add(frontierID);
				}
			}
			
			// Calculate a list of agents that haven't yet been assigned any frontiers
			Vector<Integer> agentsIDsWithoutAssignedFrontiers = new Vector<Integer>();
			for (int agentID=0; agentID < agents.size(); agentID+=1)
			{
				if (frontierAssignments.get(agentID) == Constants.FED_FRONTIER_NOT_SET)
				{
					agentsIDsWithoutAssignedFrontiers.add(agentID);
				}
			}
			
			
			// Use the cost matrix here
			int minFrontierCost = Integer.MAX_VALUE;
			int selectedAgentID = -1;
			int selectedFrontierID = -1;
			for (int agentWithNoFrontierID=0; agentWithNoFrontierID < agentsIDsWithoutAssignedFrontiers.size(); agentWithNoFrontierID += 1)
			{
				for (int frontierIDWithNoAssignedAgent=0; frontierIDWithNoAssignedAgent < frontiersIDsWithMinimalPopularity.size(); frontierIDWithNoAssignedAgent +=1)
				{
					// Find the correspondencies to the matrix X Y values
					int matrixAgentID = agentsIDsWithoutAssignedFrontiers.get(agentWithNoFrontierID);
					int matrixFrontierID = frontiersIDsWithMinimalPopularity.get(frontierIDWithNoAssignedAgent);
					
					if (costMatrix[matrixAgentID][matrixFrontierID] < minFrontierCost)
					{
						minFrontierCost = costMatrix[matrixAgentID][matrixFrontierID];
						selectedAgentID = matrixAgentID;
						selectedFrontierID = matrixFrontierID;
					}
				}
			}
			
			// Once the minimal assignment cost has been detected, assign the selected frontier to the selected agent.
			// Assign the frontier to the agent, informing the others about our choice
			frontierAssignments.put(selectedAgentID, selectedFrontierID);
			// Increase the popularity of this frontier, because this agent has selected it --> no need, this is calculated
		}
		
	}
	
	private static boolean allAgentsHaveBeenAssignedFrontiers()
	{
		// Get the existing frontier assignments 
		HashMap<Integer, Integer> frontierAssignments = Viewer.FEDgetFrontierAssignments();
		
		for (Map.Entry<Integer, Integer> entry : frontierAssignments.entrySet())
		{
			//Integer agentID = entry.getKey();
			Integer frontierID = entry.getValue();
			
			if (frontierID == Constants.FED_FRONTIER_NOT_SET)
			{
				return false;
			}
		}
		
		return true;
	}

	/* Returns the distance from the requesting agent to the closest other agent, inside his viewing range.*/
	public static int getDistanceToClosestAgentInsideMyViewingRange(int myAgentID)
	{
		Coordinate2D inquirerLocation = new Coordinate2D(-1,-1);
		double distanceToClosestAgent = Integer.MAX_VALUE;
		int systemAgentID = -1; // The agentID in the list of agents
		
		// Get the location of the agent requesting this information
		for (int agentID=0; agentID < agents.size(); agentID+=1)
		{
			if (agentID == myAgentID)
			{
				inquirerLocation = agents.get(agentID).getGlobalCoordinates();
				systemAgentID = agentID;
			}
		}
		//System.out.print("Inquiring agent is on: "); inquirerLocation.print();
		
		// Initialize the variables
		Vector <Coordinate2D> cellsInVisionFieldRelativeCoords = new Vector<Coordinate2D>();
		int offsetX = 0;
		int offsetY = 0;
		if (ALGO == BMILRV)
		{
			algorithms.BrickMortar_NewImplementation.BMILRV 
			inquiringAgent = 
					(algorithms.BrickMortar_NewImplementation.BMILRV) agents.get(systemAgentID);

			ViewingRangeMap inquiringAgentViewingRangeMapRelativeCoords = 
					Viewer.m.BMI_LongVision_Bresenham_getViewingRangeMap(
							inquiringAgent.getX(), inquiringAgent.getY(), 
							Constants.AGENT_VIEWING_RANGE, // (int) this.getViewingRange(), 
							(int) inquiringAgent.getOrientationAngle(), 
							inquiringAgent.getVisionFieldArc(), 
							inquiringAgent.getBresenhamDegStep(), 
							inquiringAgent.getBresenhamSamplingStep());
			
			//Vector <Coordinate2D> 
			cellsInVisionFieldRelativeCoords = Viewer.m.BMgetReacheableCellsInMap(inquiringAgentViewingRangeMapRelativeCoords);

			offsetX = inquiringAgent.getX() - inquiringAgentViewingRangeMapRelativeCoords.getRobotLocation().getX();
			offsetY = inquiringAgent.getY() - inquiringAgentViewingRangeMapRelativeCoords.getRobotLocation().getY();
		}
		else if (ALGO == BMILRVSM)
		{
			algorithms.BrickMortar_NewImplementation.BMILRVSM 
			inquiringAgent = 
					(algorithms.BrickMortar_NewImplementation.BMILRVSM) agents.get(systemAgentID);
			
			ViewingRangeMap inquiringAgentViewingRangeMapRelativeCoords = 
					Viewer.m.BMI_LongVision_Bresenham_getViewingRangeMap(
							inquiringAgent.getX(), inquiringAgent.getY(), 
							Constants.AGENT_VIEWING_RANGE, // (int) this.getViewingRange(), 
							(int) inquiringAgent.getOrientationAngle(), 
							inquiringAgent.getVisionFieldArc(), 
							inquiringAgent.getBresenhamDegStep(), 
							inquiringAgent.getBresenhamSamplingStep());
			
			//Vector <Coordinate2D> 
			cellsInVisionFieldRelativeCoords = Viewer.m.BMgetReacheableCellsInMap(inquiringAgentViewingRangeMapRelativeCoords);

			offsetX = inquiringAgent.getX() - inquiringAgentViewingRangeMapRelativeCoords.getRobotLocation().getX();
			offsetY = inquiringAgent.getY() - inquiringAgentViewingRangeMapRelativeCoords.getRobotLocation().getY();
		}
				
		// Calculate euclidian distance here towards the visible agents (taking obstacles into account)
		for (int agentID=0; agentID < agents.size(); agentID+=1)
		{;
			if (agentID != myAgentID)
			{
				Coordinate2D agentRelativeCoordinate = new Coordinate2D(
						agents.get(agentID).getX() - offsetX, 
						agents.get(agentID).getY() - offsetY 
						);
				
				// Test if agents see each other (if the agent is in the field of view of the inquiring agent)
				if (coordinateListContainsCoordinate(cellsInVisionFieldRelativeCoords, agentRelativeCoordinate))
				{
					double interAgentDistance = inquirerLocation.getEuclideanDistanceTo(agents.get(agentID).getGlobalCoordinates());
					
					if (interAgentDistance < distanceToClosestAgent)
					{
						distanceToClosestAgent = interAgentDistance;
					}					
				}											
			}			
		}
		
		return Math.min(	(int) (distanceToClosestAgent),
							Constants.AGENT_VIEWING_RANGE
				);		
	}
	
	/** 2015.03.04
	 * Returns true if the given coordinate is contained by the given coordinate list */
	public static boolean coordinateListContainsCoordinate(Vector<Coordinate2D> coordinateList, Coordinate2D coordinate)
	{
		// Loop through all the coordinates of the list, to check for matches
		for (int coordinateID=0; coordinateID < coordinateList.size(); coordinateID+=1)
		{
			if (coordinateList.get(coordinateID).equals(coordinate))
			{
				return true;
			}
		}
		
		// If nothing matched the input coordinate until now, return false 
		return false;
	}
}
