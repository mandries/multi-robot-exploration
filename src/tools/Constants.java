package tools;

public class Constants 
{
	public static final int BM_AGENT_STANDBY = 0;
	public static final int BM_AGENT_LOOP_DETECTION = 1; // = AGENT_ON
	public static final int BM_AGENT_LOOP_CONTROL = 2;
	public static final int BM_AGENT_LOOP_CLOSING = 3;
	public static final int BM_AGENT_LOOP_CLEANING = 4;
	public static final int BM_AGENT_OFF = 5;
	
	public static final double BM_WALL = 0; // Brick&Mortar wall
	public static final double BM_CLOSED = 1; // Brick&Mortar visited cell
	public static final double BM_EXPLORED = 2; // Brick&Mortar explored cell
	public static final double BM_UNEXPLORED = 3; // Brick&Mortar unexplored cell
	public static final double BMI_ATTRACTOR = 4; // Brick&Mortar Improved attractor cell
	
	/** Brick&Mortar variables for directions */
	public static final int DIR_NONE = -1;
	public static final int DIR_UP = 0;
	public static final int DIR_RIGHT = 1;
	public static final int DIR_DOWN = 2;
	public static final int DIR_LEFT = 3;
	/******************************************/
	public static final int BM_NO_AGENT_TRACE = -1;
	
	public static final boolean DEBUG = false; 
	
	public static int CELL_WIDTH = 16; // in pixels
	public static int CELL_HEIGHT = CELL_WIDTH; // in pixels
	
	public static int AGENT_SIZE = CELL_WIDTH / 2; // in pixels
	
	/** Frontier Explorator Distributed */
	public static final int FED_FRONTIER_NOT_SET = -1;
	
	public static final int AGENT_VIEWING_RANGE = 10;

}
