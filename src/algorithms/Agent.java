package algorithms;

import java.util.Vector;

import tools.Coordinate2D;
import tools.Map2D;

public class Agent 
{
	private int currentX;
	private int currentY;
	private int localTime = 1;
	private int totalSteps = 0;
	private Vector <Coordinate2D> lastTrip = new Vector <Coordinate2D>();
	private Map2D map;
//	private Map2D mapLRTA = new Map2D();
//	private Map2D mapVAW = new Map2D();
	
	// 2015.02.02 Added for enabling long-vision BMI 
	private float viewingRange;
	
	public Agent(){}
	
	public Agent(Map2D inputMap, int x, int y)
	{
		map = inputMap;
		currentX = x;
		currentY = y;
		lastTrip.add(new Coordinate2D(currentX, currentY));
	}
	
	//public float getViewingRange(){return viewingRange;}
	public int getAgentSteps() {return totalSteps;}
	public Coordinate2D getGlobalCoordinates(){return new Coordinate2D(currentX, currentY);}
	public int getX(){return currentX;}
	public int getY(){return currentY;}
	public void setX(int x_in){currentX = x_in;}
	public void setY(int y_in){currentY = y_in;}
	public void setCoords(Coordinate2D coord_in)
	{
		currentX=coord_in.getX();
		currentY=coord_in.getY();
	}
	//public void setX(int x_in){currentX = x_in;}
	//public void setY(int y_in){currentY = y_in;}
//	public int AW2getSearchLevel(){return -1;}
//	public boolean AW2getAllVisited(){return false;}
	public Vector <Coordinate2D> getLastTrip(){return lastTrip;}
	
	public void step()	{totalSteps++;}

	// Forced movements (for manual simulator)
	public void stepDown()
	{
		currentY++;
		lastTrip.add(new Coordinate2D(currentX, currentY));
	}
	public void stepUp()
	{
		currentY--;
		lastTrip.add(new Coordinate2D(currentX, currentY));
	}
	public void stepLeft()
	{
		currentX--;
		lastTrip.add(new Coordinate2D(currentX, currentY));
	}
	public void stepRight()
	{
		currentX++;
		lastTrip.add(new Coordinate2D(currentX, currentY));
	}
	
	
	/** Returns true if agents is at the given spot */
	public boolean isAt(int in_x, int in_y)
	{
		if ((currentX == in_x) && (currentY == in_y))
		{
			return true;
		}
		return false;
	}
	
	
	public float getViewingRange(){return viewingRange;} 
	public void setViewingRange(float x){viewingRange = x;}
	//public int getBMIViewRange(){return viewingRange;} //Map2D.BM_VISION_RANGE;} // return viewRange;} // not yet used
	//public void setViewRange(int x){viewingRange = x;}//Map2D.BM_VISION_RANGE = x;} // viewRange = x; // not yet used
}
