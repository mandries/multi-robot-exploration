package algorithms.BrickMortar_NewImplementation;

import java.util.Vector;

import algorithms.Agent;

import tools.Constants;
import tools.Coordinate2D;
import tools.Map2D;


/** 
 * Improvements over BM_Classic:
 * 		Loop closing till the second intersection, skipping the first one 
 * 		(every loop closing closes at least one cell)
 * 
 * 		Keep in memory the time of last nonLoopCellClosing,
 * 			so as to identify false loops.
 * 
 * 		Use LRTA instead of NodeCounting for the DispersionMap
 * 
 *  	Loop closing during loop cleaning
 *  
 *  	Next: add the ATTRACTOR CELL on a separate feature map
 * */
public class BrickMortarImproved
extends Agent
{
	// The position of the agent on the grid
	private int currentX;
	private int currentY;
	
	private int agentID;
	
	// The local clock used for time-stamps
	private int localTime = 0;
	
	// Total number of steps made by the agent
	private int totalSteps = 0;
	
	// A vector containing the route taken by the agent
	private Vector <Coordinate2D> lastTrip = new Vector <Coordinate2D>();
	
	// (A pointer to) the map on which the agent travels
	//	and which is shared with other agents
	// Maps for synchrony
	private Map2D oldMap; // The map from last step (orientation only for the current step)
	private Map2D currentMap; // The map that is modified at the current step
	
	private int agentState;
	
	/** BMI SPECIFIC */
	private int timeOfLastNonLoopCellClosing = -1; 
	//private boolean justEnteredLoopControl = false;
	// Used for "second intersection" optimization
	private boolean alreadyClosedCells = false;
	private int intersectionsPassed = 0; // WARNING: USED FOR OPTIMISATION
	/** BMI SPECIFIC (END) */
	
	/* 
	 * Bresenham view (robot local view, not top-down view on the map)
	 * */
	int orientationAngle = 0; //int orientationAngle, 
	float visionFieldArc = 360; //int visionFieldArc,
	int bresenhamDegStep = 10; //int degStep, // angle degree step
	float bresenhamSamplingStep = (float) 0.5; //float step
	int BMI_VIEWING_RANGE = 1; // declared above
	
	public BrickMortarImproved(Map2D inputOldMap, Map2D inputNewMap, int x, int y, int agentID_in)
	{
		oldMap = inputOldMap;
		currentMap = inputNewMap;
		currentX = x;
		currentY = y;
		agentID = agentID_in;
		lastTrip.add(new Coordinate2D(currentX, currentY));
		// Set the initial agent state to loop detection
		agentState = Constants.BM_AGENT_LOOP_DETECTION;
		super.setViewingRange(BMI_VIEWING_RANGE);
	}
	
	/** Synchronization function (used to update the old map) */
	public void BMsetOldMap(Map2D inputMap)
	{
		oldMap = inputMap;
	}
	/** Synchronization function (used to update the current map) */
	public void BMsetCurrentMap(Map2D inputMap)
	{
		currentMap = inputMap;
	}
	
	
	public int getAgentSteps() {return totalSteps;}
	public int getX(){return currentX;}
	public int getY(){return currentY;}
	public int AW2getSearchLevel(){return -1;}
	public boolean AW2getAllVisited(){return false;}
	public Vector <Coordinate2D> getLastTrip(){return lastTrip;}
		
	public float getOrientationAngle() {return orientationAngle;} 
	public float getVisionFieldArc() {return visionFieldArc;} //int visionFieldArc,
	public int getBresenhamDegStep() {return bresenhamDegStep;} //int degStep, // angle degree step
	public float getBresenhamSamplingStep() {return bresenhamSamplingStep;} //float step
	public float getViewingRange() {return super.getViewingRange();}
	
	/* Forces 1..N calls to the algorithm to make a single step */
	// FIXED a problem may arise here when exploration ends
	//	OR when agent in standby
	//	OR when agent is OFF
	// the "step" function will loop forever
	public void step()
	{	
		// Call the algorithm while a step is not made
		localTime++;
		boolean stepMade; // = false;
		do 
		{
			stepMade=call();
			lastTrip.add(new Coordinate2D(currentX, currentY));
		}while ((!stepMade) && 
				(agentState != Constants.BM_AGENT_STANDBY) &&
				(agentState != Constants.BM_AGENT_OFF));
		totalSteps++;
	}
	
	/* Make a call to the BrickMortar algorithm*/
	public boolean call()	
	{
		boolean stepMade = false;
		if (Constants.DEBUG)
			System.out.println("Agent" + agentID + " at [" + currentX + ", " + currentY + "], " +
					"which is " + oldMap.getPhysicalMapValue(currentX, currentY));
		/* The call first checks the state of the agent
		 * 	and then performs an action
		 * */
		
		/*********************\
		|***** DETECTION *****|
		\*********************/
		if (agentState == Constants.BM_AGENT_LOOP_DETECTION)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP DETECTION");
		
			Coordinate2D tmpCell = oldMap.BMgetPreviousCell(currentX, currentY, agentID);
			if (Constants.DEBUG)
				System.out.println("Previous cell is: [" + tmpCell.getX() + ", " + tmpCell.getY() + "]");
			
			/*************/
			/** Marking **/
			/*************/

			/* 0. Attractor cell */
			if (oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BMI_ATTRACTOR)
			{
				// Do not mark anything	
			}
			/* 1. Already been in this cell */
			else if ((oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_EXPLORED) &&
				// Not from opposite direction
				(!oldMap.BMgetNextCell(currentX, currentY, agentID).equals
				 (oldMap.BMgetPreviousCell(currentX, currentY, agentID)))	&&
				// THIS Agent has once passed through this cell
				 (!oldMap.BMgetNextCell(currentX, currentY, agentID).equals(new Coordinate2D(-1, -1))) &&
				 (oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE) &&
				 // BMI OPTIMIZATION
				 (oldMap.BMgetIndividualTraceMap(currentX, currentY, agentID) > timeOfLastNonLoopCellClosing))
			{
				agentState = Constants.BM_AGENT_LOOP_CONTROL;
				stepMade = false;
				return stepMade;
			}			
			/* 2. Agent hasn't yet visited this cell 
			 * and cell is not blocking anything */
			// and cell not controlled by someone else
			else if ((!oldMap.BMisBlocking(currentX, currentY, (int) this.getViewingRange())) &&
					(!currentMap.BMisBlocking(currentX, currentY, (int) this.getViewingRange())) &&
					(currentMap.BMnoClosedCellsWithIDAround(currentX, currentY)))
			{
				// Check if the cell is an attractor
				if (oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BMI_ATTRACTOR)
				{
					// If no way to go, then close
					// Else leave the cell without making any traces
				}
				else if (oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE)
				{
					// 1. BM map 
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
					currentMap.BMsetNextCell(currentX, currentY, agentID, new Coordinate2D(-1, -1));
					timeOfLastNonLoopCellClosing = localTime;
				}
				else // if (map.BMgetIDsMap(currentX, currentY) != Constants.BM_NO_AGENT_TRACE)
				{
					 // Check if no cells with this ID are around
				}
			}// end of !isBlocking			
			else if (currentMap.BMisBlocking(currentX, currentY, (int) this.getViewingRange()))
			{
				// 1. BM map 
				if (currentMap.getPhysicalMapValue(currentX, currentY) != Constants.BM_CLOSED)
				{
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_EXPLORED);
				}
			}// end of isBlocking
			// 2. Common dispersion map
			//map.BMupdateTimestampMap(currentX, currentY, localTime);
			//currentMap.BMupdateNodeCountingMap(currentX, currentY);
			
			/** BMI */
			currentMap.BMupdateLRTAMap(currentX, currentY);
			/** BMI END */
			//map.BMupdateThrunMap(currentX, currentY);
						
			// 3. Individual trace map
			currentMap.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);	
			
			// Bresenham viewing (local robot perspective)
			this.currentMap.BMI_LongVision_BresenhamVisionColoring___(
					currentX, 
					currentY, 
					(int) super.getViewingRange(), // the viewing range is indicated in the Agent class 
					orientationAngle, //90, //int orientationAngle, 
					visionFieldArc, //int visionFieldArc,
					bresenhamDegStep, //int degStep, // angle degree step
					bresenhamSamplingStep //float step
					);
			
			/****************/
			/** Navigation **/
			/****************/
			Vector<Coordinate2D> nextCells = new Vector<Coordinate2D>();
			nextCells = oldMap.BMgetNeighbourCells(currentX, currentY);
			nextCells = oldMap.BMreturnPriorityCells(nextCells);

			// If it is the only attainable cell
			if (nextCells.size() == 1)
			{
				Coordinate2D nextCell =	nextCells.elementAt(0);

				// 2. NextCellMap Leave direction trace
				currentMap.BMsetNextCell(currentX, currentY, agentID, nextCell);
				
				currentX = nextCell.getX();
				currentY = nextCell.getY();				
				stepMade = true; // Report that 1 step has been made
				return stepMade;
			}
			// Else break ties among equal cells
			else if (nextCells.size() != 1)
			{
				// Remove the previous cell from the next possible cells
				Coordinate2D previousCell = 
						oldMap.BMgetPreviousCell(currentX, currentY, agentID);
				nextCells = oldMap.BMexcludeCellFromList(
						previousCell.getX(), previousCell.getY(), nextCells);
				
				if (nextCells.size() > 0)
				{
					if (nextCells.elementAt(0).getValue() == Constants.BM_EXPLORED)
					{
						nextCells = oldMap.BMreturnCellsWithMinLRTA(nextCells);										
					}
					else if (nextCells.elementAt(0).getValue() == Constants.BM_UNEXPLORED)
					{
						nextCells = oldMap.BMreturnCellsWithMostClosedWallsAround(nextCells);
					}						
				}

				// Nowhere left to go (surrounded by closed cells and walls)
				if (nextCells.size() == 0)
				{
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
					currentMap.BMsetNextCell(currentX, currentY, agentID, new Coordinate2D(-1, -1));
					agentState = Constants.BM_AGENT_OFF;
					stepMade = false; // Report that no step has been made
				}
				else if (nextCells.size() == 1)
				{
					// Move to the next cell
					Coordinate2D nextCell = nextCells.elementAt(0);

					// 2. NextCellMap Leave direction trace
					currentMap.BMsetNextCell(currentX, currentY, agentID, nextCell);
					
					currentX = nextCell.getX();
					currentY = nextCell.getY();
					stepMade = true; // Report that 1 step has been made
				}
				else if (nextCells.size() > 1)
				{
					// Choose a random cell
					Coordinate2D nextCell = new Coordinate2D();
					nextCell = nextCells.elementAt((int) (Math.random() * nextCells.size()));
					
					// 2. NextCellMap Leave direction trace
					currentMap.BMsetNextCell(currentX, currentY, agentID, nextCell);
					
					currentX = nextCell.getX();
					currentY = nextCell.getY();
					stepMade = true; // Report that 1 step has been made
				}
			}
			
		} // end of LOOP_DETECTION
		
		/*******************\
		|***** CONTROL *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_LOOP_CONTROL)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CONTROL");

			// 3. Individual trace map
			currentMap.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			// If cell already closed by someone else, start loop cleaning
			if (oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_CLOSED)
			{
				if (Constants.DEBUG)
					System.out.println("This cell is closed.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// Agent has found its own ID trace
			else if (oldMap.BMgetIDsMap(currentX, currentY) == agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Found my own ID trace.");
				agentState = Constants.BM_AGENT_LOOP_CLOSING;
				stepMade = false;
				return stepMade;
			}
			// Agent did not find any trace (cell explored without control)
			else if ((oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE) &&
				((oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_EXPLORED) ||
				/** BMI addition */
				 (oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BMI_ATTRACTOR)))
				/** BMI end */
			{
				// Marking
				currentMap.BMsetIDsMap(currentX, currentY, agentID); // Control cell

				// Navigation
				Coordinate2D nextCell = 
						oldMap.BMgetNextCell(currentX, currentY, agentID);
				
				// FIX BM
				if (nextCell.getX() == -1)
				{
					if (Constants.DEBUG)
						System.out.println("No next cell found.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				else if (oldMap.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) == Constants.BM_CLOSED)
				{
					if (Constants.DEBUG)
						System.out.println("The next cell is closed.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				// ELSE IF CELL NOT CLOSED (THEREFORE EXPLORED)
				else if (oldMap.BMgetIDsMap(nextCell.getX(), nextCell.getY()) > agentID) //&&
					//(map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) != Constants.BM_NO_AGENT_TRACE))
				{
					if (Constants.DEBUG)
						System.out.println("Agent with a higher priority controls next cell.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}				
				// Check for agents in STANDBY
				else if(oldMap.BMgetStandbyMap(nextCell.getX(), nextCell.getY()) > agentID)
				{
					if (Constants.DEBUG)
						System.out.println("Another agent with a higher priority " +
								"is on standby on the next cell.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				else //if (map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == Constants.BM_NO_AGENT_TRACE) 
					//(map.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
				{
					if (Constants.DEBUG)
						System.out.println("The next cell is open and free to control.");
					currentX = nextCell.getX();
					currentY = nextCell.getY();
					stepMade = true;
					return stepMade;	
				}
			}
			
			// If B controls and B > A
			else if (oldMap.BMgetIDsMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Another agent with a higher priority is controling this cell.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If C standby and C > A
			else if (oldMap.BMgetStandbyMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Another agent with a higher priority is on standby here.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// Otherwise become standby
			else
			{
				// Wait for your cell to be liberated
				if (Constants.DEBUG)
				{
					System.out.println("Waiting for the cell to be liberated");
					System.out.println("Local current CONTROL ID:" + oldMap.BMgetIDsMap(currentX, currentY));
					System.out.println("Local current STANDBY ID:" + oldMap.BMgetStandbyMap(currentX, currentY));
				}
				
				currentMap.BMsetStandbyMap(currentX, currentY, agentID);
				agentState = Constants.BM_AGENT_STANDBY;
				stepMade = false;
				return stepMade;				
			}			
		}
		
		/*******************\
		|***** CLOSING *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_LOOP_CLOSING )
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CLOSING");
			
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			// Marking
			//Switch to loop cleaning if intersection reached
			if (oldMap.BMisIntersection(currentX, currentY, agentID))
			{
				if (Constants.DEBUG)
					System.out.println("Intersection detected !");
				
				// BMI OPTIMIZATION (SKIP 1st intersection if no closed cells)
				intersectionsPassed++;
				if ((intersectionsPassed == 2) || (alreadyClosedCells))
				{
					if (Constants.DEBUG)
						System.out.println("Second intersection found ! Stopping Loop closing !");
					intersectionsPassed = 0;
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					alreadyClosedCells = false;
					
					stepMade = false;
					return stepMade;
				}
				else if ((intersectionsPassed == 1) && (!alreadyClosedCells))
				{
					if (Constants.DEBUG)
					System.out.println("First intersection found ! " +
							"Skipping and continuing Loop closing !");
				
					// Navigation
					Coordinate2D nextCell = oldMap.BMgetNextCell(currentX, currentY, agentID);

					if (currentMap.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == agentID)
					{
						if (currentMap.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
						{
							currentX = nextCell.getX();
							currentY = nextCell.getY();
							stepMade = true;
							return stepMade;
						}
						else // cell is closed
						{
							agentState = Constants.BM_AGENT_LOOP_CLEANING;
							stepMade = false;
							return stepMade;
						}
					}
					else
					{
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}
				}
				/** BMI OPTIMIZATION END */
				
			}
			else //if(!map.BMisIntersection(currentX, currentY, agentID))
			{
				// Marking
				currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
				alreadyClosedCells = true;
				
				// Navigation
				Coordinate2D nextCell = oldMap.BMgetNextCell(currentX, currentY, agentID);

				if (oldMap.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == agentID)
				{
					if (oldMap.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
					{
						currentX = nextCell.getX();
						currentY = nextCell.getY();
						stepMade = true;
						return stepMade;
					}
					else // cell is closed
					{
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}
				}
				else
				{
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
			}
		}
		
		/********************\
		|***** CLEANING *****|
		\********************/
		else if (agentState == Constants.BM_AGENT_LOOP_CLEANING)
		{
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CLEANING");
			Coordinate2D previousCell =
					oldMap.BMgetPreviousCellLoopCleaning(currentX, currentY, agentID);
			
			if (oldMap.BMgetIDsMap(currentX, currentY) == agentID)
			{
				// Marking
				currentMap.BMsetIDsMap(currentX, currentY, Constants.BM_NO_AGENT_TRACE);
				currentMap.BMsetNextCell(currentX, currentY, agentID, new Coordinate2D(-1, -1));
				
				/** BMI OPTIMIZATION */
				if ((!oldMap.BMisBlocking(currentX, currentY, (int) this.getViewingRange())) &&
					(!currentMap.BMisBlocking(currentX, currentY, (int) this.getViewingRange())) &&
					//(oldMap.BMnoClosedCellsWithIDAround(currentX, currentY)) &&
					(currentMap.BMnoClosedCellsWithIDAround(currentX, currentY)) &&
					(oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE) &&
					(currentMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE) &&
					(currentMap.getPhysicalMapValue(currentX, currentY) != Constants.BMI_ATTRACTOR))
				{
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
					timeOfLastNonLoopCellClosing = localTime;
				}// end of !isBlocking		
				/** BMI OPTIMIZATION END */
				
				
				if (previousCell.getX() != -1)
				{
					currentX = previousCell.getX();
					currentY = previousCell.getY();
					stepMade = true;
					return stepMade;
				}
				else
				{
					if (Constants.DEBUG)
						System.out.println("Previous cell not found for cleaning.");
					agentState = Constants.BM_AGENT_LOOP_DETECTION;
					stepMade = false;
					return stepMade;
				}
			}
			else if (previousCell.getX() != -1)
			{
				/** BMI OPTIMIZATION */
				if ((!oldMap.BMisBlocking(currentX, currentY, (int) this.getViewingRange())) &&
					(!currentMap.BMisBlocking(currentX, currentY, (int) this.getViewingRange())) &&
					(oldMap.BMnoClosedCellsWithIDAround(currentX, currentY)) &&
					(oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE) &&
					(currentMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE))
				{
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
					timeOfLastNonLoopCellClosing = localTime;
				}// end of !isBlocking		
				/** END OF BMI OPTIMIZATION */
				
				if (Constants.DEBUG)
					System.out.println("This cell is clean from my ID, " +
						"going to the neighbouring one to clean it.");
				currentX = previousCell.getX();
				currentY = previousCell.getY();
				stepMade = true;
				return stepMade;
			}
			else
			{
				if (Constants.DEBUG)
					System.out.println("This cell is already clean from my ID.");
				agentState = Constants.BM_AGENT_LOOP_DETECTION;
				stepMade = false;
				return stepMade;
			}
		}
		
		/*******************\
		|***** STANDBY *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_STANDBY)
		{
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			if (Constants.DEBUG)
			{
				System.out.println("Agent " + agentID + " is in STANDBY");
				System.out.println("Local current CONTROL ID:" + oldMap.BMgetIDsMap(currentX, currentY));
				System.out.println("Local current STANDBY ID:" + oldMap.BMgetStandbyMap(currentX, currentY));
			}

			// If cell becomes visited
			if (oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_CLOSED)
			{
				if (Constants.DEBUG)
					System.out.println("Cell became visited. Switching to cleaning.");
				currentMap.BMsetStandbyMap(currentX, currentY, Constants.BM_NO_AGENT_TRACE);
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If replaced by another agent in STANDBY
			else if (oldMap.BMgetStandbyMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Agent with a higher priority took over the standby slot. Switching to cleaning.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If another agent TOOK CONTROL over
			else if (oldMap.BMgetIDsMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Agent with a higher priority took over the cell. Switching to cleaning.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If cell is cleaned
			else if (oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE)
			{
				if (Constants.DEBUG)
					System.out.println("Cell cleaned. Switching back to CONTROL.");
				currentMap.BMsetStandbyMap(currentX, currentY, Constants.BM_NO_AGENT_TRACE);
				agentState = Constants.BM_AGENT_LOOP_CONTROL;
				stepMade = false;
				return stepMade;
			}
			// otherwise remain standby
			else
			{
				// Remain standby
				if (Constants.DEBUG)
				{
					System.out.println("Remaining in standby");
					System.out.println("Local current CONTROL ID:" + oldMap.BMgetIDsMap(currentX, currentY));
					System.out.println("Local current STANDBY ID:" + oldMap.BMgetStandbyMap(currentX, currentY));
				}
				stepMade = false;
				return stepMade;
			}
		}
		
		/***************\
		|***** OFF *****|
		\***************/
		else if (agentState == Constants.BM_AGENT_OFF)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is OFF");
			stepMade = false;
			return stepMade;
		}
			
		 
		
		/* Say if a step has been made */
		return stepMade;
	}
	// End of "CALL" function
}
