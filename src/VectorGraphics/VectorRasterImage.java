package VectorGraphics;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

import net.sf.epsgraphics.EpsGraphics;

public class VectorRasterImage extends Image
{
	private BufferedImage bufferedImage;
	private EpsGraphics vectorImage;
	
	// Constructor function
	public VectorRasterImage()
	{}

	@Override
	public int getWidth(ImageObserver observer) {
		return 0;
	}

	@Override
	public int getHeight(ImageObserver observer) {
		return 0;
	}

	@Override
	public ImageProducer getSource() {
		return null;
	}

	@Override
	public Graphics getGraphics() {
		return null;
	}

	@Override
	public Object getProperty(String name, ImageObserver observer) {
		return null;
	}

	
	
}
