package tools;

public class AW2CellLink 
{
	private Coordinate2D source;
	private Coordinate2D destination;
	private int value;
	
	public AW2CellLink()
	{
		value = -1;
	}
	
	public AW2CellLink(Coordinate2D source_in, Coordinate2D dest_in)
	{
		source = source_in;
		destination = dest_in;
		value = 0;
	}
	
	public Coordinate2D getSource(){return source;}
	public Coordinate2D getDestination(){return destination;}
	public int getValue(){return value;}
	
	public void setSource(Coordinate2D c){ source = c; }
	public void setDestination(Coordinate2D c){ destination = c; }
	public void setValue(int v){value = v;}
	
}
