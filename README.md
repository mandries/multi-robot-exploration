### Code for the paper [Multi-robot taboo-list exploration of unknown structured environments](https://hal.inria.fr/hal-01196008) (IROS 2015)

Date: February-March 2015  
Code writting time: 2012 (Feb-July), 2015 (Feb-March)  
Article authors: **Mihai Andries, François Charpillet**  
Code author: **Mihai Andries**  
Contact: mihai@andries.eu  
Host lab: Inria Nancy

To launch the code: 
1. Download Eclipse IDE (for Java)
2. Import the folder with the code as a project into Eclipse (File -> Import; then General -> Existing projects into workspace)
3. Correct the link to the library used by the code:
    3.1 In the window "Package Explorer" (left of the screen, normally) find the imported project
    3.2 Click-right on the folder of the project, then select "Properties", then inside select "Java build path" and set the right path to the JAR epsgrqphics-1.2.jar (inside the lib folder).
4. Launch the code from src/tools/Viewer.java (by clicking on the Run button)

The type of launched algorithm is defined by the line:
```java
private static int ALGO = BMILRV; //BMILRV; //BMILRVSM; // FED; // FED; // FEG; // BMI; // BM; 
```