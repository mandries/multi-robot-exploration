package algorithms.BrickMortar_NewImplementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import algorithms.Agent;


import tools.Constants;
import tools.Coordinate2D;
import tools.Map2D;
import tools.Map2D.ViewingRangeMap;
import tools.Viewer;


/** 
 * Improvements over BM_Classic:
 * 		Loop closing till the second intersection, skipping the first one 
 * 		(every loop closing closes at least one cell)
 * 
 * 		Keep in memory the time of last nonLoopCellClosing,
 * 			so as to identify false loops.
 * 
 * 		Use LRTA instead of NodeCounting for the DispersionMap
 * 
 *  	Loop closing during loop cleaning
 *  
 *  	Next: add the ATTRACTOR CELL on a separate feature map
 * */
public class FrontierExploratorDistributed /* Brick&Mortar Improved, Long Vision*/
extends Agent
{
	// The position of the agent on the grid
	private int cellX;
	private int cellY;
	
	private int agentID;
	
	// The local clock used for time-stamps
	private int localTime = 0;
	
	// Total number of steps made by the agent
	private int totalSteps = 0;
	
	// A vector containing the route taken by the agent
	private Vector <Coordinate2D> lastTrip = new Vector <Coordinate2D>();
	
	// (A pointer to) the map on which the agent travels
	//	and which is shared with other agents
	// Maps for synchrony
	private Map2D map; // The map
	
	/* 
	 * Bresenham view (robot local view, not top-down view on the map)
	 * */
	int orientationAngle = 0; //int orientationAngle, 
	float visionFieldArc = 360; //int visionFieldArc,
	int bresenhamDegStep = 1; //int degStep, // angle degree step
	float bresenhamSamplingStep = (float) 0.5; //float step
	int BMI_VIEWING_RANGE = 10; // declared above
	
	public FrontierExploratorDistributed(Map2D inputMap, int x, int y, int agentID_in)
	{
		map = inputMap;
		cellX = x;
		cellY = y;
		agentID = agentID_in;
		lastTrip.add(new Coordinate2D(x, y));
		//lastTrip.add(new Coordinate2D(cellX, cellY));
		// Set the initial agent state to loop detection
		super.setViewingRange(BMI_VIEWING_RANGE);
	}
		
	
	@Override
	public Coordinate2D getGlobalCoordinates(){return new Coordinate2D(cellX, cellY);}
	
	public int getAgentSteps() {return totalSteps;}
	@Override
	public int getX(){return cellX;}
	@Override
	public int getY(){return cellY;}
	public Vector <Coordinate2D> getLastTrip(){return lastTrip;}
		
	public float getOrientationAngle() {return orientationAngle;} 
	public float getVisionFieldArc() {return visionFieldArc;} //int visionFieldArc,
	public int getBresenhamDegStep() {return bresenhamDegStep;} //int degStep, // angle degree step
	public float getBresenhamSamplingStep() {return bresenhamSamplingStep;} //float step
	public float getViewingRange() {return super.getViewingRange();}
	
	@Override
	public void step()
	{	
		// Call the algorithm while a step is not made
		localTime++;
		call();
		lastTrip.add(new Coordinate2D(getX(), getY()));
		totalSteps++;
	}
	
	/* Make a call to the BrickMortar algorithm*/
	public void call()	
	{
		// Get the map with the cells in the Bresenham viewing range (with cells of type ViewingRangeMap.NOT_VIEWABLE, BM_WALL, BM_EXPL, ...)
		ViewingRangeMap viewingRangeMapRelativeCoords = this.map.BMI_LongVision_Bresenham_getViewingRangeMap(
						getX(), getY(), (int) this.getViewingRange(), (int) this.getOrientationAngle(), 
						(int) this.getVisionFieldArc(), this.getBresenhamDegStep(), this.getBresenhamSamplingStep());
		
		int offsetX = getX() - viewingRangeMapRelativeCoords.getRobotLocation().getX();
		int offsetY = getY() - viewingRangeMapRelativeCoords.getRobotLocation().getY();
		//Coordinate2D currentRelativeLocation = viewingRangeMapRelativeCoords.getRobotLocation();//new Coordinate2D(cellX, cellY);
		
		// 1. Mark everything in the field of view as explored.
		Vector <Coordinate2D> cellsInVisionFieldRelativeCoords = this.map.BMgetReacheableCellsInMap(viewingRangeMapRelativeCoords);
		
		for (int cellID=0; cellID < cellsInVisionFieldRelativeCoords.size(); cellID+=1)
		{
			//System.out.println("Reacheable cells number: " + cellsInVisionFieldRelativeCoords.size());
			Coordinate2D cellRelativeCoord = cellsInVisionFieldRelativeCoords.get(cellID);
			// Avoid marking the place where you stand before marking surrounding places. This may block you.
			map.setPhysicalMapValue(
					cellRelativeCoord.getX() + offsetX, 
					cellRelativeCoord.getY() + offsetY,
					Constants.BM_EXPLORED);
		}
		
		// 2. Navigate to the closest unexplored region, that was not assigned to another robot.
		Coordinate2D closestUnexploredCell = this.getClosestUnexploredCell();
		//System.out.print("Closest unexplored cell is: ");
		//closestUnexploredCell.print();
		
		// If exploration has finished, go to RDV point
		if (closestUnexploredCell.getX() == -1)
		{
			//System.out.println("Goind to RDV point.");
			// Go to RDV point.
			Coordinate2D rdvPoint = new Coordinate2D(Viewer.rdvX, Viewer.rdvY);
			int[][] gradientMap = this.getGradientFromLocationToRobot(rdvPoint);
			this.makeOneStepTowardsTarget(gradientMap);			
		}
		else
		{
			int assignedFrontierID = Viewer.FEDgetFrontierAssignments().get(this.agentID); //this.assignFrontierToAgent();
			if (assignedFrontierID != Constants.FED_FRONTIER_NOT_SET)
			{
				//System.out.println("Agent " + agentID + " selected frontier " + assignedFrontierID);
				
				// Get the closest cell from the frontier assigned to the agent
				Vector<Vector<Coordinate2D>> openFrontiers = Viewer.FEDgetOpenFrontiers(); // TODO optimize here, so as not to recalculate the frontiers every time
				//System.out.println("There are : " + openFrontiers.size() + " open frontiers.");
				Coordinate2D closestCellFromAssignedFrontier = this.getClosestCellFromGivenFrontier(openFrontiers.get(assignedFrontierID));
				int[][] gradientMap = this.getGradientFromLocationToRobot(closestCellFromAssignedFrontier);
				this.makeOneStepTowardsTarget(gradientMap);
			}
		}
	}

	/** Returns the coordinate of the closest unexplored cell */
	public Coordinate2D getClosestUnexploredCell()
	{
		boolean exploredAllMap = false;
		boolean foundUnexploredCell = false;
		
		double[][] dataMap = map.getDataMap();
		int[][] gradientMap = new int[dataMap.length][dataMap[0].length];
		final int GRADIENT_NOT_SET = -1; 
		
		for (int x=0; x<gradientMap.length; x++)
		{
			for (int y=0; y<gradientMap[0].length; y++)
			{
				gradientMap[x][y] = GRADIENT_NOT_SET;
			}
		}
		
		Vector<Coordinate2D> cellsToAnalyze = new Vector<Coordinate2D>();
		cellsToAnalyze.add(new Coordinate2D(cellX, cellY));
		gradientMap[cellX][cellY] = 0;
		
		// As the list of cells is ordered in ascending order, 
		//	simply return the coords of th	e first unexplored cell you find. 
		while ((!exploredAllMap) && (!foundUnexploredCell))
		{
			// Analyze the first cell from the list
			Coordinate2D cellToAnalyze = cellsToAnalyze.get(0);
			cellsToAnalyze.remove(0);
			int localGradientValue = gradientMap[cellToAnalyze.getX()][cellToAnalyze.getY()];
			
			if (dataMap[cellToAnalyze.getX()][cellToAnalyze.getY()] == Constants.BM_UNEXPLORED)
			{
				foundUnexploredCell = true;
				return cellToAnalyze;
			}
			else
			{
				// Add neighbors and continue searching
				for (int	x=Math.max(0,  cellToAnalyze.getX()-1);
							x<=Math.min(cellToAnalyze.getX()+1, gradientMap.length-1);
							x+=1)
				{
					for (int 	y=Math.max(0,  cellToAnalyze.getY()-1);
								y<=Math.min(cellToAnalyze.getY()+1, gradientMap[0].length-1);
								y+=1)		
					{
						if	( 	(
									(x!=cellToAnalyze.getX()) || 
									(y!=cellToAnalyze.getY()) // not the same cell
								)
								&&
								(
									(x==cellToAnalyze.getX()) || 
									(y==cellToAnalyze.getY()) // 4 directions
								)
							)
						{
							if ((gradientMap[x][y] == GRADIENT_NOT_SET) && (dataMap[x][y] != Constants.BM_WALL))
							{
								// Set new gradient value for these cells
								gradientMap[x][y] = localGradientValue + 1;
								cellsToAnalyze.add(new Coordinate2D(x,y));
							}	
						}
					}
				}
			}

			// Stop if all the map was analyzed.
			if (cellsToAnalyze.size() == 0)
			{
				exploredAllMap = true;
			}
		}
		
		// If no cell was found, return (-1, -1)
		return new Coordinate2D(-1,-1);
	}
	
	/** Returns true if unexplored cells still exist in this map */
	public boolean unexploredCellsExist()
	{
		double[][] dataMap = map.getDataMap();
		for (int x=0; x<dataMap.length; x+=1)
		{
			for (int y=0; y<dataMap[0].length; y+=1)
			{
				if (dataMap[x][y] == Constants.BM_UNEXPLORED)
					return true;
			}
		}
		
		return false;
	}
	
	/** Constructs a gradient that leads the robot towards the target, 
	 * by following the cell with the smallest gradient value around him.*/
	public int[][] getGradientFromLocationToRobot(Coordinate2D targetLocation)
	{
		double[][] dataMap = map.getDataMap();
		int[][] gradientMap = new int[dataMap.length][dataMap[0].length];
		
		final int GRADIENT_NOT_SET = Integer.MAX_VALUE;
		
		for (int x=0; x<gradientMap.length; x++)
		{
			for (int y=0; y<gradientMap[0].length; y++)
			{
				gradientMap[x][y] = GRADIENT_NOT_SET;
			}
		}
		gradientMap[targetLocation.getX()][targetLocation.getY()] = 0;
		
		if (targetLocation.equals(new Coordinate2D(cellX, cellY)))
		{
			return gradientMap;
		}
		
		boolean allMapIsAnalyzed = false;
		boolean hasMetRobot = false;
		Vector<Coordinate2D> cellsToAnalyze = new Vector<Coordinate2D>();
		cellsToAnalyze.add(targetLocation);
		
		while ((!hasMetRobot) && (!allMapIsAnalyzed))
		{
			Coordinate2D cellToAnalyze = cellsToAnalyze.get(0);
			cellsToAnalyze.remove(0);
			
			int localGradientValue = gradientMap[cellToAnalyze.getX()][cellToAnalyze.getY()];
			
			// Add neighbors and continue searching
			for (int	x=Math.max(0,  cellToAnalyze.getX()-1);
						x<=Math.min(cellToAnalyze.getX()+1, gradientMap.length-1);
						x+=1)
			{
				for (int 	y=Math.max(0,  cellToAnalyze.getY()-1);
							y<=Math.min(cellToAnalyze.getY()+1, gradientMap[0].length-1);
							y+=1)		
				{
					if	( 	(
							(x!=cellToAnalyze.getX()) || 
							(y!=cellToAnalyze.getY()) // not the same cell
						)
						&&
						(
							(x==cellToAnalyze.getX()) || 
							(y==cellToAnalyze.getY()) // 4 directions
						)
					)
					{
						if ((gradientMap[x][y] == GRADIENT_NOT_SET) && (dataMap[x][y] != Constants.BM_WALL))
						{
							// Set new gradient value for these cells
							gradientMap[x][y] = localGradientValue + 1;
							cellsToAnalyze.add(new Coordinate2D(x,y));
						}
						
						if ((x==cellX) && (y==cellY))
						{
							//System.out.println(x+ ", " + y + " is the location of the robot. Returning the gradient map.");
							return gradientMap;
						}	
					}					
				}
			}
			
			// Stop when all the cells were analyzed.
			if (cellsToAnalyze.size() == 0)
				allMapIsAnalyzed = true;
		}
		
		return gradientMap;
	}
	
	/** Pushes the agent one step ahead towards its goal, using the given gradient map. */
	public void makeOneStepTowardsTarget(int[][] gradientMap)
	{
		int minimalValue = Integer.MAX_VALUE;
		Coordinate2D nextCell = new Coordinate2D(-1, -1);
		
		for (int	x=Math.max(0,  cellX-1);
					x<=Math.min(cellX+1, gradientMap.length-1);
					x+=1)
		{
			for (int y=Math.max(0,  cellY-1);
					 y<=Math.min(cellY+1, gradientMap[0].length-1);
					 y+=1)		
			{
				if	( 	(
						(x!=cellX) || 
						(y!=cellY) // not the same cell
					)
					&&
					(
						(x==cellX) || 
						(y==cellY) // 4 directions
					)
				)
				{
					if (gradientMap[x][y] < minimalValue)
					{
						minimalValue = gradientMap[x][y];
						nextCell = new Coordinate2D(x,y);
					}	
				}				
			}
		}
			
		// If found the next cell to go to, then step onto it.
		if (nextCell.getX() != -1)
		{
			cellX = nextCell.getX();
			cellY = nextCell.getY();
		}
	}
	
		
	/** Assigns a frontier to this agent.
	 * Returns the ID of the closest frontier.
	 * 
	 * For each agent (in the order of their IDs) it chooses the closes frontier.
	 * 
	 * Warning: stop using this. Use the group greedy instead.
	 *  */
	public int ___assignFrontierToAgent()
	{
		int assignedFrontierID=-1;

		// Get the existing frontier assignments 
		HashMap<Integer, Integer> frontierAssignments = Viewer.FEDgetFrontierAssignments();

		// Get the list of existing frontiers
		Vector<Vector<Coordinate2D>> frontiers = Viewer.FEDgetOpenFrontiers();
		
		// Store the popularity of frontiers in a hash map
		HashMap<Integer, Integer> frontiersPopularity = new HashMap<Integer, Integer>();
		
		// Initialize the popularity of all frontiers to 0.
		for (int frontierID=0; frontierID < frontiers.size(); frontierID++)
		{
			frontiersPopularity.put(frontierID, 0);
		}
		
		// Calculate the popularity of each frontier.
		for (Map.Entry<Integer, Integer> entry : frontierAssignments.entrySet()) 
		{
		    //Integer agentID = entry.getKey(); // agent ID
		    Integer frontierID = entry.getValue(); // frontier id
		    
		    // Increment the popularity of the given frontier
		    int selectedFrontierPopularity = frontiersPopularity.get(frontierID);
		    frontiersPopularity.put(frontierID, selectedFrontierPopularity+1);
		}
		
		// Calculate the frontier with minimal popularity
		int minimalPopularity = Integer.MAX_VALUE;
		int frontierIDWithMinimalPopularity = -1;
		for (Map.Entry<Integer, Integer> entry : frontiersPopularity.entrySet())
		{
			Integer frontierID = entry.getKey();
			Integer frontierPopularity = entry.getValue();
			
			if (frontierPopularity <= minimalPopularity)
			{
				minimalPopularity = frontierPopularity;
				frontierIDWithMinimalPopularity = frontierID;
			}
		}
		
		// TODO use here a group-greedy approach.
		// Assign the frontiers in augmenting cost order.
		
		// Create a list of frontiers with minimal popularity
		Vector<Integer> frontiersIDsWithMinimalPopularity = new Vector<Integer>();
		for (Map.Entry<Integer, Integer> entry : frontiersPopularity.entrySet())
		{
			Integer frontierID = entry.getKey();
			Integer frontierPopularity = entry.getValue();
			
			if (frontierPopularity == minimalPopularity)
			{
				frontiersIDsWithMinimalPopularity.add(frontierID);
			}
		}
		
		// Get the closest frontier among the least popular
		int closestDistance = Integer.MAX_VALUE;
		int[][] distanceMap = getDistanceMapFromAgent();
		for (int frontierID=0; frontierID < frontiersIDsWithMinimalPopularity.size(); frontierID++)
		{
			Coordinate2D closestCellFromAssignedFrontier = this.getClosestCellFromGivenFrontier(frontiers.get(frontierID));
			
			if (distanceMap[closestCellFromAssignedFrontier.getX()][closestCellFromAssignedFrontier.getY()] < closestDistance)
			{
				// Assign closest frontier to the agent, and not simply the least popular.
				// Most close to agent among least popular.
				closestDistance = distanceMap[closestCellFromAssignedFrontier.getX()][closestCellFromAssignedFrontier.getY()];
				assignedFrontierID = frontierID;
			}
		}
		
		
		// Assign the frontier to the agent, informing the others about our choice
		frontierAssignments.put(agentID, assignedFrontierID);
		// Increase the popularity of this frontier, because this agent has selected it --> no need, this is calculated
		
		return assignedFrontierID;
	}

	/** Generates a distance map, with the 0 distance being at the position of the agent. */
	public int[][] getDistanceMapFromAgent()
	{
		double[][] dataMap = map.getDataMap();
		int[][] distanceMap = new int[dataMap.length][dataMap[0].length];
		
		final int GRADIENT_NOT_SET = Integer.MAX_VALUE;
		
		for (int x=0; x<distanceMap.length; x++)
		{
			for (int y=0; y<distanceMap[0].length; y++)
			{
				distanceMap[x][y] = GRADIENT_NOT_SET;
			}
		}
		
		
		// Generate the distance map, starting from the robot location
		distanceMap[cellX][cellY] = 0;
		Vector<Coordinate2D> cellsToAnalyze = new Vector<Coordinate2D>();
		cellsToAnalyze.add(new Coordinate2D(cellX, cellY));
		
		boolean allMapIsAnalyzed = false;
		
		// Calculate the distance map
		while (!allMapIsAnalyzed)
		{
			Coordinate2D cellToAnalyze = cellsToAnalyze.get(0);
			cellsToAnalyze.remove(0);
			
			int localGradientValue = distanceMap[cellToAnalyze.getX()][cellToAnalyze.getY()];
			
			// Add neighbors and continue searching
			for (int	x=Math.max(0,  cellToAnalyze.getX()-1);
						x<=Math.min(cellToAnalyze.getX()+1, distanceMap.length-1);
						x+=1)
			{
				for (int 	y=Math.max(0,  cellToAnalyze.getY()-1);
							y<=Math.min(cellToAnalyze.getY()+1, distanceMap[0].length-1);
							y+=1)		
				{
					if	( 	(
							(x!=cellToAnalyze.getX()) || 
							(y!=cellToAnalyze.getY()) // not the same cell
						)
						&&
						(
							(x==cellToAnalyze.getX()) || 
							(y==cellToAnalyze.getY()) // 4 directions
						)
					)
					{
						if ((distanceMap[x][y] == GRADIENT_NOT_SET) && (dataMap[x][y] != Constants.BM_WALL))
						{
							// Set new gradient value for these cells
							distanceMap[x][y] = localGradientValue + 1;
							cellsToAnalyze.add(new Coordinate2D(x,y));
						}
					}					
				}
			}
			
			// Stop when all the cells were analyzed.
			if (cellsToAnalyze.size() == 0)
				allMapIsAnalyzed = true;
		}
		
		return distanceMap;
	}
	
	/** Returns the coordinate of the closest cell from the given frontier. */
	public Coordinate2D getClosestCellFromGivenFrontier(Vector<Coordinate2D> frontier)
	{
		Coordinate2D closestCell = new Coordinate2D(-1,-1);
	
		int minimalDistanceToRobot = Integer.MAX_VALUE;
		int[][] distanceMapFromAgent = this.getDistanceMapFromAgent();
		
		for (int frontierCellID = 0; frontierCellID < frontier.size(); frontierCellID += 1)
		{
			Coordinate2D frontierCell = frontier.get(frontierCellID);
			
			if (distanceMapFromAgent[frontierCell.getX()][frontierCell.getY()] < minimalDistanceToRobot)
			{
				minimalDistanceToRobot = distanceMapFromAgent[frontierCell.getX()][frontierCell.getY()];
				closestCell = frontierCell;
			}
		}
			
		return closestCell;
	}
	
	/** Returns true if agents is at the given spot */
	@Override
	public boolean isAt(int in_x, int in_y)
	{
		if ((cellX == in_x) && (cellY == in_y))
		{
			return true;
		}
		return false;
	}
}
