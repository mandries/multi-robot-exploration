package tools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;

public class Map2D 
{
	public static final double EMPTY_CELL = 0;
	public static final double WALL_CELL = 1;
	
	private static boolean DEBUG = false;//true;
	
	/** DUPLICATED BLOCK OF CONSTANTS (SEE BrickMortar CLASS) */
	public static final double BM_PATH_SEEN = -1; // Brick&Mortar path existence value
	public final static int BMI_BRESENHAM_NOT_VISIBLE =  -2;  // This constant is used for checking which cells are blocking for their surroundings.
	public final static int MARKED = -10;	
	/**********************************************************/
	
	/**************** Map exploration state *******************/
	public static final int MAP_NOT_YET_TOTALLY_EXPLORED = 0;
	public static final int MAP_EXPLORED = 1; // For LRTA, BM
	public static final int MAP_CLOSED = 2;	
	/**********************************************************/
	
	
	private double[][] map; // TERRAIN
	private double[][] dataMap; // LAST VISIT TIME for LRTA, NodeCounting, EVAW
	
	/*************  Brick&Mortar maps  ****************/
	/* This map can also be used as a data map
	 * for the Brick&Mortar algorithm */
	//private double[][] map; // TERRAIN (not used by BM) 
	//private double[][] dataMap; // The map that contains BM Marking
	private static int[][] BMIDsMap; // Map for BM to write agents' IDs
	private static int[][] BMstandbyMap; // map for BM to access standby agents
	private static int[][][] BMIndividualTraceMap; // Map for BMA3 to write agents' pass time through these nodes
	//private static int[][] BMfeatureMap; // to keep trace of loop entries (and possible of ONLY RETURN paths to the RDV)
	/************** Brick&Mortar Simplified map *******/
	// a map used by BM and BM_Simplified to implement SHARED/COMMON LRTA
	private static int[][] BMCommonDispersionMap;
	private static Coordinate2D[][][] BMnextCellsMap;
	/**************************************************/
	
	private int mapHeight;
	private int mapWidth;
	
	//private int cellHeight = Constants.CELL_HEIGHT;
	//private int cellWidth = Constants.CELL_WIDTH;
	
	public Map2D(){}
	
	// Create a new Map2D, using input height and width
	public Map2D(int width, int height)
	{
		dataMap = new double[width][height];
		map = new double[width][height];
		
		mapWidth = width;
		mapHeight = height;
		
		for (int i=0; i<width; i++)
		{
			for (int j=0; j<height; j++)
			{
				map[i][j] = EMPTY_CELL;
				dataMap[i][j] = Constants.BM_UNEXPLORED; // EMPTY_CELL
			}
		}
	}
	
	/** Make a copy of the map 
	 * (used for synchronized version of Brick&Mortar */
	public void BMcopyMap(Map2D copyIntoThisMap)
	{
		for (int x=0; x<mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				// 1. Physical map
				copyIntoThisMap.setPhysicalMapValue(x, y, dataMap[x][y]);
				copyIntoThisMap.BMsetIDsMap(x, y, BMIDsMap[x][y]);
				copyIntoThisMap.BMsetStandbyMap(x, y, BMstandbyMap[x][y]);
				copyIntoThisMap.BMsetCommonDispersionMap(x, y, BMCommonDispersionMap[x][y]);
				for (int k=0; k<Viewer.MAX_AGENTS; k++)
				{
					copyIntoThisMap.BMsetIndividualTraceMap(x, y, k, BMIndividualTraceMap[x][y][k]);
					copyIntoThisMap.BMsetNextCell(x, y, k, BMnextCellsMap[x][y][k]);
				}
			}
		}
	}
	
	public int getCellHeight(){return Constants.CELL_HEIGHT; }//cellHeight;}
	public int getCellWidth(){return Constants.CELL_WIDTH; }//cellWidth;}
	public int getMapHeight(){return mapHeight;}
	public int getMapWidth(){return mapWidth;}
	public void setCellHeight(int in){Constants.CELL_HEIGHT = in;}
	public void setCellWidth(int in){Constants.CELL_WIDTH = in;}
	public void setDebugMode(boolean in_mode){DEBUG = in_mode;}
	
	public void BMincVisitsMap(int x, int y)
	{
		BMCommonDispersionMap[x][y]++;
	}
	
	public int BMgetVisitsNumberMap(int x, int y)
	{
		return BMCommonDispersionMap[x][y]++;
	}
	
	public void BMsetCommonDispersionMap(int x, int y, int value)
	{
		BMCommonDispersionMap[x][y] = value;
	}
	public int BMgetCommonDispersionMap(int x, int y)
	{
		return BMCommonDispersionMap[x][y]; 
	}
	
	/** Statistics functions */
	public int getTotalBlankCells()
	{
		int blankCells=0;
		for (int x=0; x < mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				if (dataMap[x][y] != Constants.BM_WALL)
				//if (map[x][y] != WALL_CELL)
				{
					blankCells++;
				}
			}
		}
		return blankCells;
	}
	
	public int getTotalWallCells()
	{
		int wallCells=0;
		for (int x=0; x < mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				if (map[x][y] == WALL_CELL)
				{
					wallCells++;
				}
			}
		}
		return wallCells;		
	}
	
	
	/** Transform an input image to a map (for LRTA-like algos)*/
	public void LRTAImageToMap2D(String imagePath)
	{
		BufferedImage img;// = new BufferedImage(0, 0, 0, null);
		int imgHeight;
		int imgWidth;
		
		// Try to open image
		try
		{			
			img = ImageIO.read(new File(imagePath));
			imgHeight = img.getHeight();
			imgWidth = img.getWidth();
			map = new double[imgWidth][imgHeight];
			dataMap = new double[imgWidth][imgHeight];
		}
		catch (Exception e)
		{
			System.err.println("ERROR 001: FAILED TO OPEN Image file");
			e.printStackTrace();
			return;
		}		
		
		// Create the map using image colors
		for (int y=0; y<imgHeight; y++)
		{
			for (int x = 0; x<imgWidth; x++)
			{
				// Setting last visit values to 0
				dataMap[x][y] = 0;
				
				int pixelRGB = img.getRGB(x, y);
				//System.out.println("(x: " + x + "y: " + y + ") =" + pixelRGB);
						
				int alpha = (pixelRGB >>24 ) & 0xFF;
				int red = (pixelRGB >>16 ) & 0xFF;
				int green = (pixelRGB >> 8 ) & 0xFF;
				int blue = pixelRGB & 0xFF;
				
				//System.out.println("(x: " + x + "y: " + y + ") = " + red + ":" + green + ":" +blue);
				
				Color color = new Color(red, green, blue, alpha);
				if (color.equals(Color.black))
				{
					map[x][y] = WALL_CELL;
					//System.out.println("BLACK");
				}
				else if (color.equals(Color.white))
				{
					map[x][y] = EMPTY_CELL;
				}
				/** Construction d'un pixel : */
				//pixelRGB = (alpha<<24)+(red<<16)+(green<<8)+blue;
				/** Les composantes doivent obligatoirement ��tre comprises dans l'intervalle 0-255 */
			}
		}
		
		mapHeight = imgHeight;	
		mapWidth = imgWidth;
	}
	
	/** Transform an input image to a map (for Brick&Mortar)*/
	public void BMImageToMap2D(String imagePath)
	{
		BufferedImage img;// = new BufferedImage(0, 0, 0, null);
		int imgHeight;
		int imgWidth;
		
		// Try to open image
		try
		{			
			img = ImageIO.read(new File(imagePath));
			imgHeight = img.getHeight();
			imgWidth = img.getWidth();
			// 0. Physic map (unused by BM)
			//map = new double[imgWidth][imgHeight];
			
			// 1. Termination map (used for the Tabu list)
			dataMap = new double[imgWidth][imgHeight];
			
			// 2. Individual trace map
			BMIndividualTraceMap = new int[imgWidth][imgHeight][Viewer.MAX_AGENTS];
			
			// 3. Common dispersion map
			BMCommonDispersionMap = new int[imgWidth][imgHeight];
			
			// 4. Feature map (loop entries) 
			//BMfeatureMap = new int[imgWidth][imgHeight];
			
			// 5. IDs map
			BMIDsMap = new int [imgWidth][imgHeight];
			BMstandbyMap = new int [imgWidth][imgHeight];
			
			BMnextCellsMap = new Coordinate2D[imgWidth][imgHeight][Viewer.MAX_AGENTS];
			
			// Set map size
			mapHeight = imgHeight;	
			mapWidth = imgWidth;
		}
		catch (Exception e)
		{
			System.err.println("ERROR 001: FAILED TO OPEN Image file");
			e.printStackTrace();
			return;
		}		
		
		// Create the map using image colors
		for (int y=0; y<imgHeight; y++)
		{
			for (int x = 0; x<imgWidth; x++)
			{
				// 1. Termination map (used for the Tabu list)
				dataMap[x][y] = Constants.BM_UNEXPLORED;
				
				// 2. Individual trace map
				for (int k=0; k<Viewer.MAX_AGENTS; k++)
				{
					BMIndividualTraceMap[x][y][k] = -1;	
					BMnextCellsMap[x][y][k] = new Coordinate2D(-1, -1);
				}				
							
				// 3. Common dispersion map
				BMCommonDispersionMap[x][y] = 0; // ZERO visits initially
				
				// 4. Feature map (loop entries)
				//BMfeatureMap[x][y] = Viewer.BMfeatureNoFeature; // No features initially
				
				// 5. IDs map
				BMIDsMap[x][y] = Constants.BM_NO_AGENT_TRACE;
				
				// 6. Standby map
				BMstandbyMap[x][y] = Constants.BM_NO_AGENT_TRACE;
				
				int pixelRGB = img.getRGB(x, y);
				//System.out.println("(x: " + x + "y: " + y + ") =" + pixelRGB);
						
				int alpha = (pixelRGB >>24 ) & 0xFF;
				int red = (pixelRGB >>16 ) & 0xFF;
				int green = (pixelRGB >> 8 ) & 0xFF;
				int blue = pixelRGB & 0xFF;
				
				//System.out.println("(x: " + x + "y: " + y + ") = " + red + ":" + green + ":" +blue);
				
				Color color = new Color(red, green, blue, alpha);
				if (color.equals(Color.black))
				{
					//map[x][y] = WALL_CELL;
					dataMap[x][y] = Constants.BM_WALL;
					//System.out.println("BLACK");
				}
				else if (color.equals(Color.white))
				{
					//map[x][y] = EMPTY_CELL;
					dataMap[x][y] = Constants.BM_UNEXPLORED;
				}
				/** Construction d'un pixel : */
				//pixelRGB = (alpha<<24)+(red<<16)+(green<<8)+blue;
				/** Les composantes doivent obligatoirement etre comprises dans l'intervalle 0-255 */
			}
		}
	}
	
	/* Return the datamap (used only for debugging) */
	public double[][] getDataMap()
	{ 
		return dataMap;
	}
	
	/* Return the value of the cell */
	public double getPhysicalMapValue(int x, int y)
	{
		return dataMap[x][y];
	}
	
	/** Set the date of the last visit for a cell */
	public void setPhysicalMapValue(int x, int y, double value)
	{
		dataMap[x][y] = value;
	}
	
	
	/*****************************************************************************************\
	|*****************************************************************************************|
	|******************************  Brick&Mortar functions  *********************************|
	|*****************************************************************************************|
	\*****************************************************************************************/

	
	public void BMgenerateMap(int width_in, int height_in, int walls_in)
	{
		map = new double[width_in][height_in];
		dataMap = new double[width_in][height_in];
		
			// Create the map using image colors
			for (int y=0; y<height_in; y++)
			{
				for (int x = 0; x<width_in; x++)
				{
					// Setting last visit values to 0
					dataMap[x][y] = Constants.BM_UNEXPLORED;
				}
			}
			
			int wallsPlaced=0;
			while (wallsPlaced < walls_in)
			{
				int x = (int) (Viewer.randomGenerator.nextDouble()*100)%width_in;
				int y = (int) (Viewer.randomGenerator.nextDouble()*100)%height_in;
				
				if (dataMap[x][y] != Constants.BM_WALL)
				{
					dataMap[x][y] = Constants.BM_WALL;
					wallsPlaced++;
				}
			}
			
			mapHeight = height_in;	
			mapWidth = width_in;
	}
	
	/** Brick&Mortar functions
	 * Say if map explored
	 * */
	public int BMexplorationState()
	{
		int totalExplored = 0;
		//int totalVisited=0;
		
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				if (dataMap[x][y] == Constants.BM_UNEXPLORED)
				{
					if (DEBUG)
					{
						System.out.println("Map not yet explored !");
					}
					return MAP_NOT_YET_TOTALLY_EXPLORED;
				}
				else if (dataMap[x][y] == Constants.BM_EXPLORED)
				{
					totalExplored++;
				}
				//else if (dataMap[x][y] == BM_VISITED)
				//{
				//	totalVisited++;
				//}
			}
		}
				
		if (totalExplored == 0)
		{
			if (DEBUG)
			{
				System.out.println("Map visited !");
			}
			return MAP_CLOSED;
		}
		else
		{
			if (DEBUG)
			{
				System.out.println("Map explored !");
			}
			return MAP_EXPLORED;
		}
	}
	
	/** FrontierExploratorGreedy functions
	 * Say if map is explored 
	 * States: 	MAP_NOT_YET_TOTALLY_EXPLORED - not explored
	 * 			1 - explored, no RDV yet
	 * 			2 - explored, RDV OK
	 * */
	public int FEexplorationState()
	{
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				if (dataMap[x][y] == Constants.BM_UNEXPLORED)
				{
					if (DEBUG)
					{
						System.out.println("Map not yet explored !");
					}
					return MAP_NOT_YET_TOTALLY_EXPLORED;
				}				
			}
		}
				
		// If you got until here, then everything was explored.
		int agentsAtRDVPoint = Viewer.countAgentsAt(Viewer.rdvX, Viewer.rdvY);
		//System.out.println("Agents at RDV: " + agentsAtRDVPoint + " (out of " + Viewer.getAgents().size() + ")");
		// The map was explored, and all the agents are at the RDV point
		if (agentsAtRDVPoint == Viewer.getAgents().size())
		{
			return MAP_CLOSED;
		}
		// The map was explored, but the agents are still en route to the RDV point.
		else
		{
			return MAP_EXPLORED;
		}
	}
	
	/**
	 * FE calculate exploration percentage
	 * */
	public float FEgetExplorationPercentage()
	{
		float visitedCells = 0;
		//float exploredCells = 0;
		float totalCells = 0;
		
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				if ((dataMap[x][y] == Constants.BM_EXPLORED) || (dataMap[x][y] == Constants.BM_UNEXPLORED))
					totalCells += 1;
				
				if (dataMap[x][y] == Constants.BM_EXPLORED)
				{
					visitedCells += 1;
				}
			}
		}
				
		return visitedCells/totalCells;
	}
	
	/**
	 * BM calculate visit percentage done
	 * */
	public float BMgetVisitPercentage()
	{
		float visitedCells = 0;
		//float exploredCells = 0;
		float totalCells = 0;
		
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				totalCells += 1;
				if (dataMap[x][y] == Constants.BM_CLOSED)
				{
					visitedCells += 1;
				}
			}
		}
				
		return visitedCells/totalCells;
	}
	
	/** Brick&Mortar functions
	 * Get map image with a BM legend (Wall/Visited/Explored/Unvisited)
	 * */
	public BufferedImage BMgetMapImage()
	{
		BufferedImage result = 
				new BufferedImage(	Constants.CELL_WIDTH * mapWidth, 
									Constants.CELL_HEIGHT * mapHeight, 
									1 /*TYPE_INT_RGB*/);
		Graphics g = result.getGraphics();
		
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y = 0; y<mapHeight; y++)
			{
				// Set floor color
				if (dataMap[x][y] == Constants.BM_UNEXPLORED)
				{
					g.setColor(Color.white);
				}
				else if (dataMap[x][y] == Constants.BM_WALL)
				{
					g.setColor(Color.black);
				}
				else if (dataMap[x][y] == Constants.BM_EXPLORED)
				{
					g.setColor(Color.GREEN);
					
					if (BMCommonDispersionMap[x][y] == Integer.MAX_VALUE)
						g.setColor(Color.YELLOW);
					/*
					if (BMfeatureMap[x][y] == Viewer.BMfeatureLoopEntrance)
					{
						g.setColor(Color.YELLOW);
					}
					*/
				}
				else if (dataMap[x][y] == Constants.BM_CLOSED)
				{
					g.setColor(Color.GRAY);
					/*
					if (BMfeatureMap[x][y] == Viewer.BMfeatureLoopEntrance)
					{
						g.setColor(Color.ORANGE);
					}
					*/
				}
				else if (dataMap[x][y] == Constants.BMI_ATTRACTOR)
				{
					g.setColor(Color.yellow);
				}
				
				// Draw the floor tile
				g.fillRect
				(x*Constants.CELL_WIDTH, y*Constants.CELL_HEIGHT, Constants.CELL_WIDTH, Constants.CELL_HEIGHT);
				
				// Draw the separation lines
				g.setColor(Color.black);
				g.drawRect
				(x*Constants.CELL_WIDTH, y*Constants.CELL_HEIGHT, Constants.CELL_WIDTH, Constants.CELL_HEIGHT);
				
				// BMI
				// Draw the LRTA value on cells
				//g.setColor(Color.red);
				//g.drawString("" + BMCommonDispersionMap[x][y], x*cellWidth, (y+1)*cellHeight);
				//g.drawString("" + BMIndividualTraceMap[x][y][0], x*cellWidth, (y+1)*cellHeight);
			}
		}
		//g.setColor(Color.green);
		//g.drawRect(0, 0, Constants.CELL_WIDTH * mapWidth -1, Constants.CELL_HEIGHT * mapHeight -1);
		
		/*
		// Draw LRTA values on cells
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y = 0; y<mapHeight; y++)
			{
				g.setColor(Color.red);
				g.drawString("" + BMCommonDispersionMap[x][y], x*Constants.CELL_WIDTH, (y+1)*Constants.CELL_HEIGHT);
				//g.drawString("" + BMCommonDispersionMap[x][y], x*cellWidth + (cellWidth/8), (y+1)*cellHeight - cellHeight/4);
				//g.drawString("" + BMIndividualTraceMap[x][y][0], x*cellWidth + (cellWidth/8), (y+1)*cellHeight - cellHeight/4);
				//g.drawString("" + BMIndividualTraceMap[x][y], x*cellWidth + (cellWidth/8), (y+1)*cellHeight - cellHeight/4);
			}
		}
		*/
		
		return result;
	}
	
	
	/** Brick&Mortar functions
	 * Get map image with a BM legend (Wall/Visited/Explored/Unvisited)
	 * */
	public void BMgetMapImageOnGraphics(Graphics g)
	{
		/*
		BufferedImage result = 
				new BufferedImage(	Constants.CELL_WIDTH * mapWidth, 
									Constants.CELL_HEIGHT * mapHeight, 
									1); // TYPE_INT_RGB
		Graphics g = result.getGraphics();
		*/
		
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y = 0; y<mapHeight; y++)
			{
				// Set floor color
				if (dataMap[x][y] == Constants.BM_UNEXPLORED)
				{
					g.setColor(Color.white);
				}
				else if (dataMap[x][y] == Constants.BM_WALL)
				{
					g.setColor(Color.black);
				}
				else if (dataMap[x][y] == Constants.BM_EXPLORED)
				{
					Color color = new Color(0, 200, 0);
					//g.setColor(Color.GREEN);
					g.setColor(color);
					/*
					if (BMfeatureMap[x][y] == Viewer.BMfeatureLoopEntrance)
					{
						g.setColor(Color.YELLOW);
					}
					*/
				}
				else if (dataMap[x][y] == Constants.BM_CLOSED)
				{
					g.setColor(Color.GRAY);
					/*
					if (BMfeatureMap[x][y] == Viewer.BMfeatureLoopEntrance)
					{
						g.setColor(Color.ORANGE);
					}
					*/
				}
				else if (dataMap[x][y] == Constants.BMI_ATTRACTOR)
				{
					g.setColor(Color.yellow);
				}
				
				// Draw the floor tile
				g.fillRect
				(x*Constants.CELL_WIDTH, y*Constants.CELL_HEIGHT, Constants.CELL_WIDTH, Constants.CELL_HEIGHT);
				
				// Draw the separation lines
				/*
				g.setColor(Color.black);
				g.drawRect
				(x*Constants.CELL_WIDTH, y*Constants.CELL_HEIGHT, Constants.CELL_WIDTH, Constants.CELL_HEIGHT);
				*/
				
				// BMI
				// Draw the LRTA value on cells
				//g.setColor(Color.red);
				//g.drawString("" + BMCommonDispersionMap[x][y], x*cellWidth, (y+1)*cellHeight);
				//g.drawString("" + BMIndividualTraceMap[x][y][0], x*cellWidth, (y+1)*cellHeight);
			}
		}
		/*
		g.setColor(Color.green);
		g.drawRect(0, 0, Constants.CELL_WIDTH * mapWidth -1, Constants.CELL_HEIGHT * mapHeight -1);
		*/
		/*
		// Draw LRTA values on cells
		for (int x = 0; x<mapWidth; x++)
		{
			for (int y = 0; y<mapHeight; y++)
			{
				g.setColor(Color.red);
				g.drawString("" + BMCommonDispersionMap[x][y], x*Constants.CELL_WIDTH, (y+1)*Constants.CELL_HEIGHT);
				//g.drawString("" + BMCommonDispersionMap[x][y], x*cellWidth + (cellWidth/8), (y+1)*cellHeight - cellHeight/4);
				//g.drawString("" + BMIndividualTraceMap[x][y][0], x*cellWidth + (cellWidth/8), (y+1)*cellHeight - cellHeight/4);
				//g.drawString("" + BMIndividualTraceMap[x][y], x*cellWidth + (cellWidth/8), (y+1)*cellHeight - cellHeight/4);
			}
		}
		*/
	}
	
	/**
	 * Brick&Mortar functions 
	 * Set the value of a node on the IDsMap
	 * */
	public void BMsetIDsMap(int x, int y, int agentID)
	{
		BMIDsMap[x][y] = agentID;
	}
	
	/**
	 * Brick&Mortar functions 
	 * Get the value of a node on the IDsMap
	 * */
	public int BMgetIDsMap(int x, int y)
	{
		return BMIDsMap[x][y];
	}
	
	/**
	 * Brick&Mortar functions 
	 * Set the value of a node on the StandbyMap
	 * */
	public void BMsetStandbyMap(int x_in, int y_in, int agentID_in)
	{
		BMstandbyMap[x_in][y_in] = agentID_in;
	}
	
	/**
	 * Brick&Mortar functions 
	 * Get the value of a node on the IDsMap
	 * */
	public int BMgetStandbyMap(int x_in, int y_in)
	{
		return BMstandbyMap[x_in][y_in];
	}
	
	/**
	 * Brick&Mortar functions
	 * Set the value of the IndividualTraceMap for a cell
	 */
	public void BMsetIndividualTraceMap(int x, int y, int agentID, int agentTime)
	{
		BMIndividualTraceMap[x][y][agentID] = agentTime;
	}

	/**
	 * Brick&Mortar functions
	 * Get the value of the IndividualTraceMap for a given cell
	 */
	public int BMgetIndividualTraceMap(int x, int y, int agentID)
	{
		return BMIndividualTraceMap[x][y][agentID];
	}
	
	
	
	
	// 2015.01.26
	// A class that is used in the creation of viewing range maps
	// Allows to tell where the robot is located inside the map.
	// This is useful for BMI calculation of the cells that may be blocked
	//		if the current cell is closed.
	public class ViewingRangeMap
	{
		Coordinate2D robotLocation = new Coordinate2D();
		int[][] map;
		
		// Generate a map of the required size
		public ViewingRangeMap(int width_in, int height_in, Coordinate2D robotLocation_in, int[][] map_in)
		{
			map = new int[width_in][height_in]; // create a new map
			setMap(map_in); // copy the map
			setRobotLocation(robotLocation_in); // set the robot location
		}
		
		// Set the robot location
		private void setRobotLocation(Coordinate2D robotLocation_in)
		{
			robotLocation.setX(robotLocation_in.getX());
			robotLocation.setY(robotLocation_in.getY());
		} 
		
		public void setMapValue(int x, int y, int value){map[x][y] = value;}
		public int[][] getMap(){return map;}
		public void setMap(int[][] map_in)
		{
			for (int x=0; x<map_in.length; x++)
			{
				for (int y=0; y<map_in[0].length; y++)
				{
					map[x][y] = map_in[x][y];
				}
			}
		}
		
		public Coordinate2D getRobotLocation(){return robotLocation;}
		
		public void printMap()
		{
			System.out.println("Map: ");
			
			for (int y=0; y<map[0].length; y+=1)
			{
				for (int x=0; x<map.length; x+=1)
				{
					System.out.print(map[x][y] + " \t");
				}
				System.out.println();
			}
		}
	}
	
	
	/** Returns a map of cells that are in the viewing range 
	 * dataMap[][] is considered a globally acessible map.
	 * 
	 * viewingRange between [0, +inf]
	 * map is in INT format
	 * 
	 * Unit tested on 2015.02.05 (Mihai)
	 * */
	public ViewingRangeMap getCellsInSquareViewingRange(int x_in, int y_in, int viewingRange)
	{
		// TODO you can use here a vision field of a random shape.
		
		// Calculate the size of the viewed area of the map (top, bottom, left, right).
		int topSize, bottomSize, leftSize, rightSize;
		leftSize = (x_in < viewingRange ? x_in : viewingRange);
		rightSize = (mapWidth-1 - x_in < viewingRange ? mapWidth-1 - x_in : viewingRange);
		topSize = (y_in < viewingRange ? y_in : viewingRange);
		bottomSize = (mapHeight-1 - y_in < viewingRange ? mapHeight-1 - y_in : viewingRange);
		//int concernedMapWidth  = 1 + (2 * BM_VISION_RANGE); // this is the map maximum size
		//int concernedMapHeight = 1 + (2 * BM_VISION_RANGE);// this is the map maximum size
		int concernedMapWidth  = leftSize + 1 + rightSize; // this is the map maximum size
		int concernedMapHeight = topSize + 1 + bottomSize;// this is the map maximum size
		
		// Copy map to a customMap
		int[][] cellsInViewingRange = new int[concernedMapWidth][concernedMapHeight]; // will replace customMap
		//System.out.println("Map size: width=" + concernedMapWidth + "; height=" + concernedMapHeight);
		
		int startX = Math.max(0, x_in-viewingRange);
		int startY = Math.max(0, y_in-viewingRange);
		
		for (int x =  Math.max(0, x_in-viewingRange);
				 x <= Math.min(x_in+viewingRange, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-viewingRange);
					 y <= Math.min(y_in+viewingRange, mapHeight-1);
					 y++)
			{
				// The new algorithm that calculates the existence of a path
				cellsInViewingRange[x - startX][y-startY] = (int) dataMap[x][y];
			}
		}
		
		ViewingRangeMap viewingRangeMap = new ViewingRangeMap(
				concernedMapWidth, // map width
				concernedMapHeight, // map height
				new Coordinate2D(x_in-startX, y_in-startY), // robot location
				cellsInViewingRange
				);
	
		return viewingRangeMap;
	}
	
	/** Returns the list of reachable cells inside the viewing range
	 * Works by propagating a signal from the robot location.
	 * 
	 * WARNING: no copy is made of the viewing range map.
	 * 			the map is modified in this function.
	 * 
	 * Optimized, because it always leaves the open spaces at the farthest points from the agent.
	 * If the cells are closed in inverse order (far to close, and not in close-to-far order), the result should be better.
	 * */
	public Vector<Coordinate2D> BMgetReacheableCellsInMap(ViewingRangeMap viewingRangeMap)
	{
		int[][] originalMap = viewingRangeMap.getMap();
		int[][] reachableCellsMap = new int[originalMap.length][originalMap[0].length];
		
		// Make a copy of the map, on which to work on
		for (int x=0; x<originalMap.length; x++)
		{
			for (int y=0; y<originalMap[0].length; y++)
			{
				reachableCellsMap[x][y] = originalMap[x][y];
			}
		}
		
		// Make a structure that will contain the list of reachable cells
		Vector<Coordinate2D> reachableCells = new Vector<Coordinate2D>();
		
		// Declare where from will the signal start.
		Vector<Coordinate2D> cellsToAnalyze = new Vector<Coordinate2D>();
		cellsToAnalyze.add(viewingRangeMap.getRobotLocation());

		// 1. Mark the cell, remove cell from list
		// 2. Add all its reachable neighbors to the list
		// 3. Repeat for the first cell of the list, until list is empty
		while (!cellsToAnalyze.isEmpty())
		{
			// Get the next reachable cell 
			Coordinate2D reachableCell = cellsToAnalyze.get(0);
			
			// Consider it only if it is not marked, not a wall, not a closed cell.
			if (	(reachableCellsMap[reachableCell.getX()][reachableCell.getY()] != MARKED) &&
					(
						(reachableCellsMap[reachableCell.getX()][reachableCell.getY()] == Constants.BM_UNEXPLORED) ||
						(reachableCellsMap[reachableCell.getX()][reachableCell.getY()] == Constants.BM_EXPLORED) ||
						(reachableCellsMap[reachableCell.getX()][reachableCell.getY()] == Constants.BMI_ATTRACTOR)	
					)
				//	(reachableCellsMap[reachableCell.getX()][reachableCell.getY()] != Constants.BM_WALL) &&
				//	(reachableCellsMap[reachableCell.getX()][reachableCell.getY()] != Constants.BM_CLOSED) && 
				//	(reachableCellsMap[reachableCell.getX()][reachableCell.getY()] != BMI_BRESENHAM_NOT_VIEWABLE) // add only the cells in the field of view
					)
			{
				reachableCellsMap[reachableCell.getX()][reachableCell.getY()] = MARKED; // mark the cell as analyzed
				reachableCells.add(new Coordinate2D(reachableCell.getX(),reachableCell.getY())); // add it to the list of reachable cells
				
				// Add all its reachable neighbors to the list of cells to analyze
				Vector<Coordinate2D> neighborCells = getNeighbourCellsInMap4Link(reachableCell.getX(), reachableCell.getY(), viewingRangeMap.getMap());
				cellsToAnalyze.addAll(neighborCells);
			}
			
			// Remove the treated cell from the list of cells to analyze
			cellsToAnalyze.remove(0);
		}
		
		return reachableCells;
	}
	
	
	/** 2015.01.26 Returns a list of coordinates of cells, that are neighbors to the cell with the given input coordinates */
	public Vector<Coordinate2D> getNeighbourCellsInMap4Link(int x_in, int y_in, int[][] map_in)
	{
		Vector<Coordinate2D> result = new Vector<Coordinate2D>();
		int mapWidth = map_in.length;
		int mapHeight = map_in[0].length;
		
		for (int x =  Math.max(0, x_in-1); // neighbors in range of 1 cell
				 x <= Math.min(x_in+1, mapWidth-1); // neighbors in range of 1 cell 
				 x++)
		{
			for (int y = Math.max(0, y_in-1); // neighbors in range of 1 cell
					 y <= Math.min(y_in+1, mapHeight-1); // neighbors in range of 1 cell
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 directions
				{
					Coordinate2D newCell = new Coordinate2D(x,y);
					//newCell.setX(x);
					//newCell.setY(y);
					//newCell.setValue(dataMap[x][y]);
					result.add(newCell);
				}
			}
		}
		
		//System.err.println("Neighbors: " + result.size());
		return result;
	}
	
	/** 
	 * This function returns TRUE if cellA can be seen from cellB (if there is no obstacle between them).
	 * */
	/*
	public boolean noObstacleBetweenCells(Coordinate2D cellA, Coordinate2D cellB, float stepSize)
	{
		boolean noObstacle = true;
		
		// Line formula: ax + b
		float slope;
		float offset;
		
		// Calculate the slope of the line
		if (cellA.getY() - cellB.getY() == 0)
		{
			slope = 0;
			offset = cellA.getY();
		}
		else
		{
			// The slope should be calculated depending on the order of points
			
			// If A is to the left of B
			if (cellA.getX() < cellB.getX())
			{
				slope = ((float) (cellB.getY() - cellA.getY())) / ((float) cellB.getX() - cellA.getX());
				offset = (slope * cellB.getX()) - cellB.getY(); 
			}
			// If B is to the left of A
			else
			{
				slope = ((float) (cellA.getY() - cellB.getY())) / ((float) cellA.getX() - cellB.getX());
				offset = (slope * cellA.getX()) - cellA.getY();
			}
		}
		
		// Walk on the line between A and B, with the given step size
		//while ()
		{
			// TODO fill here the	 code
		}
		
		
		return noObstacle;
	}
	*/
	
	
	/** 
	 * This function returns a map of all the cells 
	 * 	visible by the agent inside his visionRange
	 * 	as explored cells. 
	 * 
	 * NOTE: Excludes the cells that are on the edge of the vision ray
	 * (because you can't judge if closing them is a blocking action or not).
	 * */
	public ViewingRangeMap BMI_LongVision_Bresenham_getViewingRangeMap(
			int x_in, 
			int y_in, 
			int viewingRange, 
			int orientationAngle, 
			float visionFieldArc,
			int degStep, // angle degree step
			float step
			)
	{
		// Generate a square map of the surroundings in the viewing range
		ViewingRangeMap viewingRangeMap = this.getCellsInSquareViewingRange(x_in, y_in, viewingRange);
		
		// Keep a copy of this map, onto which you will write down the visibility values.
		ViewingRangeMap visibilityBinaryMap = this.getCellsInSquareViewingRange(x_in, y_in, viewingRange);
		
		// Initialize the visibility binary map at "everything is not visible"
		for (int x=0; x<visibilityBinaryMap.getMap().length; x++)
		{
			for (int y=0; y<visibilityBinaryMap.getMap()[0].length; y++)
			{
				visibilityBinaryMap.setMapValue(x, y, BMI_BRESENHAM_NOT_VISIBLE);
			}
		}
		
		Coordinate2D robotLocation = viewingRangeMap.getRobotLocation();
		int[][] viewingMap = viewingRangeMap.getMap();
		//int mapOffsetX = x_in - viewingRangeMap.getRobotLocation().getX(); 
		//int mapOffsetY = y_in - viewingRangeMap.getRobotLocation().getY();
		
		// Bresenham
		// Calculate the angle from which sweeping the space will start
		int startDeg = (int) (orientationAngle - (visionFieldArc/2.0));
		int endDeg = (int) (orientationAngle + (visionFieldArc/2.0));
				
		// Identify what is visible in this map
		// For all the degrees
		for (	int deg = startDeg; 
				deg <= endDeg;
				deg += degStep)
		{
			boolean noObstacleOnRay = true;
			
			float range = 0; 
			
			while (	noObstacleOnRay
					&& 
					(range <= viewingRange))	
			{				
				int newX = (int) (robotLocation.getX() + range * Math.cos(Math.toRadians(deg)) +0.5); // 0.5 is because the robot is in the center of the cell.
				int newY = (int) (robotLocation.getY() + range * Math.sin(Math.toRadians(deg)) +0.5);
				
				// Calculation the location of the sampling spot
				Coordinate2D sampleCoordinate = new Coordinate2D(newX, newY);
				//sampleCoordinate.print();
						
				// If this falls inside map bounds, then continue.
				if ((sampleCoordinate.getX() >= 0) 	&&
					(sampleCoordinate.getX() < viewingMap.length) 	&&
					(sampleCoordinate.getY() >= 0) 	&&
					(sampleCoordinate.getY() < viewingMap[0].length) )
				{
					// Stop ray if this is an obstacle
					if (viewingMap[sampleCoordinate.getX()][sampleCoordinate.getY()] == Constants.BM_WALL)
					{
						// Declare this cell as viewable
						visibilityBinaryMap.setMapValue(
								sampleCoordinate.getX(), 
								sampleCoordinate.getY(),  
								viewingMap[sampleCoordinate.getX()][sampleCoordinate.getY()]);
						
						// Acknowledge that the ray has encountered an obstacle
						noObstacleOnRay = false;
					}
					else
					{
						// Set in a copy-map that this cell is visible
						visibilityBinaryMap.setMapValue(sampleCoordinate.getX(), sampleCoordinate.getY(), viewingMap[sampleCoordinate.getX()][sampleCoordinate.getY()]);
					
						// Increase the range
						range += step;
					}						
				}
				// Outside of map bounds
				else
				{
					noObstacleOnRay = false;
				}
				
			} // end of increasing range
		} // end of looping through degrees
		
		
		//return viewingRangeMap;
		return visibilityBinaryMap;
	}
	
	
	/** 
	 * This function returns a map of all the cells 
	 * 	visible by the agent inside his visionRange
	 * 	as explored cells. 
	 * 
	 * NOTE: Excludes the cells that are on the edge of the vision ray
	 * (because you can't judge if closing them is a blocking action or not).
	 * */
	public ViewingRangeMap BMILRVSM_getCommonExploredMap(
			int robotX,
			int robotY
			)
	{
		// Get the data map
		double[][] dataMap = this.getDataMap();
			
		// Generate a square map of the surroundings in the viewing range
		ViewingRangeMap visibilityBinaryMap =  this.getCellsInSquareViewingRange(robotX, robotY, 
				Math.max(this.mapWidth, this.mapHeight)); //viewingRange);
		
		/*
		// Initialize the visibility binary map at "everything is not visible"
		for (int x=0; x<visibilityBinaryMap.getMap().length; x++)
		{
			for (int y=0; y<visibilityBinaryMap.getMap()[0].length; y++)
			{
				visibilityBinaryMap.setMapValue(x, y, BMI_BRESENHAM_NOT_VISIBLE);
			}
		}
		*/
				
		for (int x=0; x<this.mapWidth; x+=1)
		{
			for(int y=0; y<this.mapHeight; y+=1)
			{
				if (
						/*
						(dataMap[x][y] == Constants.BM_EXPLORED) ||
						(dataMap[x][y] == Constants.BMI_ATTRACTOR) ||
						(dataMap[x][y] == Constants.BM_CLOSED) || 
						(dataMap[x][y] == Constants.BM_WALL) */
						dataMap[x][y] != Constants.BM_UNEXPLORED
					)
				{
					// Declare this cell as viewable by putting real data inside
					visibilityBinaryMap.setMapValue(x,y, (int) dataMap[x][y]);
				}
				else
					// Declare this cell as non visible
					visibilityBinaryMap.setMapValue(x, y, BMI_BRESENHAM_NOT_VISIBLE);
			}
		}
		
		return visibilityBinaryMap;
	}
	
	
	/** 
	 * This function marks all the cells visible by the agent inside his visionRange
	 * 	as explored cells. 
	 * 
	 * WARNING: this function shot not be used anymore. Marking should be done individually for each cell.
	 * If possible each cell should be closed, and not simply marked as explored. 
	 * */
	public void BMI_LongVision_BresenhamVisionColoring___(
			int x_in, 
			int y_in, 
			int viewingRange, 
			int orientationAngle, 
			float visionFieldArc,
			int degStep, // angle degree step
			float step
			)
	{
		System.out.println("Call!");
		// Calculate the angle from which sweeing the space will start
		int startDeg = (int) (orientationAngle - (visionFieldArc/2.0));
		int endDeg = (int) (orientationAngle + (visionFieldArc/2.0));
		//Coordinate2D robotLocation = new Coordinate2D(x_in, y_in);
		
		System.out.println("Start-end deg: " + startDeg + ", " + endDeg);
		System.out.println("Start-end radians: " + Math.toRadians(startDeg) + ", " + Math.toRadians(endDeg));
		
		//System.out.print("Robot location: ");
		//robotLocation.print();
		
		// For all the degrees
		// Identify what is visible in this map				
		for (	int deg = startDeg; 
				deg <= endDeg;
				deg += degStep)
		{
			boolean noObstacleOnRay = true;
			
			float range = 0; 
			
			while (	noObstacleOnRay
					&& 
					(range <= viewingRange))	
			{
				//System.out.println("Hop ! " + (Viewer.randomGenerator.nextDouble() * 1000)); // Simply check if it's working
				
				int newX = (int) (x_in + range * Math.cos(Math.toRadians(deg)) +0.5); // 0.5 is because the robot is in the center of the cell.
				int newY = (int) (y_in + range * Math.sin(Math.toRadians(deg)) +0.5);
				
				// Calculation the location of the sampling spot
				Coordinate2D sampleCoordinate = new Coordinate2D(newX, newY);
				//sampleCoordinate.print();
						
				// If this falls inside map bounds, then continue.
				if ((sampleCoordinate.getX() >= 0) 	&&
					(sampleCoordinate.getX() < dataMap.length) 	&&
					(sampleCoordinate.getY() >= 0) 	&&
					(sampleCoordinate.getY() < dataMap[0].length) )
				{
					// Stop ray if this is an obstacle
					if (dataMap[sampleCoordinate.getX()][sampleCoordinate.getY()] == Constants.BM_WALL)
					{
						noObstacleOnRay = false;
					}
					else
					{
						if (dataMap[sampleCoordinate.getX()][sampleCoordinate.getY()] == Constants.BM_UNEXPLORED)
						{
							// Mark the cell as explored
							this.setPhysicalMapValue(
								sampleCoordinate.getX(),
								sampleCoordinate.getY(),
								(int) Constants.BM_EXPLORED);
						}
							
						// Increase the range
						range += step;
					}						
				}
				// Outside of map bounds
				else
				{
					noObstacleOnRay = false;
				}
				
				
			} // end of increasing range
		} // end of looping through degrees

		// Vector<Coordinate2D> viewableCellsBresenham = getCellsInBresenhamViewingRange(...);
	}
	
	
	/**
	 * Brick&Mortar functions
	 * Says if the cell is not blocking the path
	 * between any two explored or unexplored cells
	 * inside the given map of cells.
	 * 
	 * Consider that bordering cells are always blockable. (these are the cells, for which you don't see all their 8 neighbors)
	 * WARNING : Accepts relative coordinates 
	 * */
	public boolean BMI_LongVision_isShadowOrEdge(int x_in_relativeCoords, int y_in_relativeCoords, ViewingRangeMap viewingRangeMap)
	{
		if (Constants.DEBUG)
			System.err.println("x_in: " + x_in_relativeCoords + "; y_in: " + y_in_relativeCoords + 
				"; map width: " + viewingRangeMap.getMap().length + 
				"; map height: " + viewingRangeMap.getMap()[0].length);
		
		// Check if the cell has any adjacent non_visible cells
		//If there is at least one, then it's blocking.
		for (int	x = Math.max(0, x_in_relativeCoords-1);
					x <= Math.min(x_in_relativeCoords + 1, viewingRangeMap.getMap().length -1);
					x += 1)
		{
			for (int	y =  Math.max(0, y_in_relativeCoords-1);
						y <= Math.min(y_in_relativeCoords+1, viewingRangeMap.getMap()[0].length - 1);
						y += 1)
			{
				if (
					( (x == x_in_relativeCoords) || (y == y_in_relativeCoords)) &&	// Check the 4 directions
					( (x != x_in_relativeCoords) || (y != y_in_relativeCoords) ) // exclude this cell
					)	
				{
					if (viewingRangeMap.getMap()[x][y] == BMI_BRESENHAM_NOT_VISIBLE)
					{
						if (Constants.DEBUG)
							System.out.println("[" + x_in_relativeCoords + ", " + y_in_relativeCoords + "] has un-seen neighbors and cannot be closed");
						return true; // Is blocking the route to something unknown
					}
				}
			}
		}
	
		
		// Check if this is not a visible cell on the border of the viewing range,
		//	in which case it may have non-visible open cells as neighbors.
		if (	(viewingRangeMap.getMap()[x_in_relativeCoords][y_in_relativeCoords] == Constants.BM_UNEXPLORED) ||
				(viewingRangeMap.getMap()[x_in_relativeCoords][y_in_relativeCoords] == Constants.BM_EXPLORED) ||
				(viewingRangeMap.getMap()[x_in_relativeCoords][y_in_relativeCoords] == Constants.BMI_ATTRACTOR)
			)
		{
			if ((x_in_relativeCoords == 0) || 
				(x_in_relativeCoords == viewingRangeMap.getMap().length-1) ||
				(y_in_relativeCoords == 0) || 
				(y_in_relativeCoords == viewingRangeMap.getMap()[0].length-1))
			{
				if (Constants.DEBUG)
					System.out.println("[" + x_in_relativeCoords + ", " + y_in_relativeCoords + "] is an open cell on the border of the vision field and cannot be closed");
				return true;
			}
		}	
		
		return false;
	}
	
	/**
	 * Brick&Mortar functions
	 * Says if the cell is not blocking the path
	 * between any two explored or unexplored cells
	 * inside the given map of cells.
	 * 
	 * Consider that bordering cells are always blockable. (these are the cells, for which you don't see all their 8 neighbors)
	 * WARNING : Accepts relative coordinates 
	 * */
	public boolean BMI_LongVision_isBlocking(int x_in_relativeCoords, int y_in_relativeCoords, ViewingRangeMap viewingRangeMap)
	{
		if (Constants.DEBUG)
			System.err.println("x_in: " + x_in_relativeCoords + "; y_in: " + y_in_relativeCoords + 
				"; map width: " + viewingRangeMap.getMap().length + 
				"; map height: " + viewingRangeMap.getMap()[0].length);
		
		// Check if the cell has any adjacent non_visible cells
		//If there is at least one, then it's blocking.
		for (int	x = Math.max(0, x_in_relativeCoords-1);
					x <= Math.min(x_in_relativeCoords + 1, viewingRangeMap.getMap().length -1);
					x += 1)
		{
			for (int	y =  Math.max(0, y_in_relativeCoords-1);
						y <= Math.min(y_in_relativeCoords+1, viewingRangeMap.getMap()[0].length - 1);
						y += 1)
			{
				if (
					( (x == x_in_relativeCoords) || (y == y_in_relativeCoords)) &&	// Check the 4 directions
					( (x != x_in_relativeCoords) || (y != y_in_relativeCoords) ) // exclude this cell
					)	
				{
					if (viewingRangeMap.getMap()[x][y] == BMI_BRESENHAM_NOT_VISIBLE)
					{
						if (Constants.DEBUG)
							System.out.println("[" + x_in_relativeCoords + ", " + y_in_relativeCoords + "] has un-seen neighbors and cannot be closed");
						return true; // Is blocking the route to something unknown
					}
				}
			}
		}
	
		
		// Check if this is not a visible cell on the border of the viewing range,
		//	in which case it may have non-visible open cells as neighbors.
		if (	(viewingRangeMap.getMap()[x_in_relativeCoords][y_in_relativeCoords] == Constants.BM_UNEXPLORED) ||
				(viewingRangeMap.getMap()[x_in_relativeCoords][y_in_relativeCoords] == Constants.BM_EXPLORED) ||
				(viewingRangeMap.getMap()[x_in_relativeCoords][y_in_relativeCoords] == Constants.BMI_ATTRACTOR)
			)
		{
			if ((x_in_relativeCoords == 0) || 
				(x_in_relativeCoords == viewingRangeMap.getMap().length-1) ||
				(y_in_relativeCoords == 0) || 
				(y_in_relativeCoords == viewingRangeMap.getMap()[0].length-1))
			{
				if (Constants.DEBUG)
					System.out.println("[" + x_in_relativeCoords + ", " + y_in_relativeCoords + "] is an open cell on the border of the vision field and cannot be closed");
				return true;
			}
		}			
			
		/*
		Instead of doing all these complex calculations, you can close one cell, and starting from another reachable cell count how many reachable cells are left.
		If the number is less than (original-1), then this cell is blocking
		WARNING: there non-visible cells may not be taken into account when doing this only on visible cells (but they have been taken into account in the previous two tests.)
		*/ 
		
		// 0. Calculate number of reachable cells from robot location, N1
		// 1. Get open location near the robot location
		// 2. Close the robot location
		// 3. Calculate number of reachable cells from open location, N2
		// 4. If (N1-1) > N2, then robot location was blocking. Else not blocking 
		// 5. Revert robot location to original value
		// 6. return the result from (4.)
		//Vector <Coordinate2D> cellsInVisionFieldRelativeCoords = this.oldMap.BMgetReacheableCellsInMap(viewingRangeMapRelativeCoords);
		
		Coordinate2D originalRobotLocation = new Coordinate2D(viewingRangeMap.getRobotLocation().getX(), viewingRangeMap.getRobotLocation().getY()); //viewingRangeMap.getRobotLocation();

		// 0. Calculate number of reachable cells from robot location, N1
		Coordinate2D originalCellToClose = new Coordinate2D(x_in_relativeCoords, y_in_relativeCoords); //viewingRangeMap.getRobotLocation();
		viewingRangeMap.setRobotLocation(originalCellToClose);
		Vector<Coordinate2D> explorableCellsAround_RobotLocation =  BMgetReacheableCellsInMap(viewingRangeMap); // explorable means: not wall, not closed.
		int reachableCellsN1 = explorableCellsAround_RobotLocation.size();
		
		// We do not block anything if there is only 1 way out
		if (reachableCellsN1 <= 1)
		{
			if (Constants.DEBUG)
				System.out.println("[" + x_in_relativeCoords + ", " + y_in_relativeCoords + "] is not blocking any path.");
			return false;
		}
			
		Coordinate2D openLocationNearCellToClose = new Coordinate2D(-1,-1);
		
		// 1. Get open location near the robot location
		for (int	x = Math.max(x_in_relativeCoords - 1, 0);
					x<= Math.min(x_in_relativeCoords + 1, viewingRangeMap.getMap().length-1);
					x+=1)
		{
			for (int	y = Math.max(y_in_relativeCoords - 1, 0);
						y<= Math.min(y_in_relativeCoords + 1, viewingRangeMap.getMap()[0].length-1);
						y+=1)
			{
				if (	((x == originalCellToClose.getX()) || (y == originalCellToClose.getY())) && // 4 directions
						((x != originalCellToClose.getX()) || (y != originalCellToClose.getY())) // not this ell
					)
				{
					if (	(viewingRangeMap.getMap()[x][y] == Constants.BM_UNEXPLORED) ||
							(viewingRangeMap.getMap()[x][y] == Constants.BM_EXPLORED) ||
							(viewingRangeMap.getMap()[x][y] == Constants.BMI_ATTRACTOR) )
					{
						openLocationNearCellToClose = new Coordinate2D(x,y); //originalRobotLocation; 
					}
				}
			}
		}
		
		//System.out.print("OpenLocation near robot: "); 
		//openLocationNearCellToClose.print();
		
		// 2. Close the robot location
			// Save the old value of the place where you are standing, to revert the map value after treatment.
			int currentCellOldValue = viewingRangeMap.getMap()[originalCellToClose.getX()][originalCellToClose.getY()];
			// Declare as a wall the cell that has to be identified as blocking or not 
			viewingRangeMap.setMapValue(
					originalCellToClose.getX(), 
					originalCellToClose.getY(), 
					(int) Constants.BM_WALL);

		// 3. Calculate number of reachable cells from open location, N2
			viewingRangeMap.setRobotLocation(openLocationNearCellToClose);
			Vector<Coordinate2D> explorableCellsAround_OpenLocation =  BMgetReacheableCellsInMap(viewingRangeMap); // explorable means: not wall, not closed.
			int reachableCellsN2 = explorableCellsAround_OpenLocation.size();
			
		// 5. Revert robot location to original value
			viewingRangeMap.setRobotLocation(originalRobotLocation);
			viewingRangeMap.setMapValue(
					originalCellToClose.getX(), 
					originalCellToClose.getY(), 
					currentCellOldValue);
			
		// 4. If (N1-1) > N2, then robot location was blocking. Else not blocking
		// 6. return the result from (4.)
			if (Constants.DEBUG)
				System.out.println("Reachable before: " + (reachableCellsN1) + "; now: " + reachableCellsN2);
			if (reachableCellsN1 - 1 > reachableCellsN2)
			{
				if (Constants.DEBUG)
					System.out.println("[" + x_in_relativeCoords + ", " + y_in_relativeCoords + "] is blocking a path.");
				return true; // is blocking
			}
			else
			{
				if (Constants.DEBUG)
					System.out.println("[" + x_in_relativeCoords + ", " + y_in_relativeCoords + "] is not blocking any path.");
				return false;
			}
	}
	
	
	
	/**
	 * Brick&Mortar functions
	 * Says if the cell is not blocking the path
	 * between any two explored or unexplored cells
	 * after having seen all the cells inside the given range around given position X,Y
	 * 
	 * Not TO DO: Check only 4 neighbors (to be tested)
	 * CHECKs the 8 neighbor cells (otherwise it wouldn't work)
	 * */
	public boolean BMisBlocking(int x_in, int y_in, int viewingRange)
	{
		// Get the square containing all the cells in the viewing range (considering a top-down view)
		ViewingRangeMap viewingRangeMap = getCellsInSquareViewingRange(x_in, y_in, viewingRange);
		
		// 2015.01.27
		// Consider a circular view, robot's perspective, with shadows.
		/*
		viewingRangeMap = getCellsInBresenhamViewingRange(
					viewingRangeMap, 
					BM_VISION_RANGE, 
					0, //int orientationAngle, 
					360, //int visionFieldArc,
					5, //int degStep, // angle degree step
					1, //int xStep, // x step for sweeping the surface
					1 //int yStep // y step for sweeping the surface
					);
		*/
		
		Vector<Coordinate2D> explorableCellsAround =  BMgetReacheableCellsInMap(viewingRangeMap); // explorable means: not wall, not closed.
					
		if (DEBUG)
		{
			System.out.println("There are " + explorableCellsAround.size() + 
				" explorable cells around cell [" + x_in + ", " + y_in + "]");
		}
		
		// We do not block anything if there is only 1 way out
		if (explorableCellsAround.size() == 1)
			return false;
		// else return true;
		else
		{
			boolean notBlocking = true;
			
			// Declare as a wall the cell that has to be identified as blocking or not 
			//customMap[x_in][y_in] = Constants.BM_WALL;
			//concernedMap[BM_VISION_RANGE][BM_VISION_RANGE] = Constants.BM_WALL;
			viewingRangeMap.setMapValue(
					viewingRangeMap.getRobotLocation().getX(), 
					viewingRangeMap.getRobotLocation().getY(), 
					(int) Constants.BM_WALL);
			
			int viewingRangeMapWidth = viewingRangeMap.getMap().length;
			int viewingRangeMapHeight = viewingRangeMap.getMap()[0].length;
					
			// For each pair of explorable cells around, check if there is a path between them 
			for (int i=0; i<explorableCellsAround.size()-1; i++)
			{
				for (int j=i+1; j<explorableCellsAround.size(); j++)			
				{
					//double[][] tmpMap = new double[concernedMapWidth][concernedMapHeight]; // will replace customMap
					double[][] tmpMap = new double[viewingRangeMapWidth][viewingRangeMapHeight]; // will replace customMap
					
					// Make a copy of the concerned map, on which to test the presence of a route between the pair of cells.
					for (int x=0; x<viewingRangeMapWidth; x++)
					{
						for (int y=0; y<viewingRangeMapHeight; y++)
						{
							tmpMap[x][y] = viewingRangeMap.getMap()[x][y];
						}
					}
					
					if  (!BMexistsPathDecentMethod(
							explorableCellsAround.elementAt(i),
							explorableCellsAround.elementAt(j),
							tmpMap))
					{
						//if (DEBUG)
						{
							System.out.println("The current cell is blocking a route between "
								+"[" + explorableCellsAround.get(i).getX() +
								  ", " + explorableCellsAround.get(i).getY() + "]"
								+"[" + explorableCellsAround.get(j).getX() +
								  ", " + explorableCellsAround.get(j).getY() + "]");
						}
						return true;
					}
				}
			}
			
			if(!notBlocking)
			{
				if (DEBUG)
				{
					System.out.println("The current cell is blocking a route !");
				}
			}
			else
			{
				if (DEBUG)
				{
					System.out.println("The current cell is not blocking anything");
				}
			}
					
			return (!notBlocking);
		}
	}
	
	/** Brick&Mortar functions
	 * Tells you if a path exists between two cells C1 and C2
	 * Used by "isBlocking" function
	 * */
	public boolean BMexistsPathStupidMethod(
			Coordinate2D C1, 
			Coordinate2D C2, 
			double[][] customMap)
	{
		// Check equality
		if (C1.equals(C2))
			return true;

		// Check if branch is blocked (search space ended)
		if (BMterminateOnMap(C1.getX(), C1.getY(), customMap))
			return false;
		
		// List of neighbors still to search for an existing path
		Vector <Coordinate2D> neighbors =
				new Vector <Coordinate2D>(); 
		boolean result = false;
				
		// If neighbors still exist
		// add them to a list
		for (int x =  Math.max(0, C1.getX()-1);
				 x <= Math.min(C1.getX()+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, C1.getY()-1);
					y <= Math.min(C1.getY()+1, mapHeight-1);
					y++)
			{
				if (((x != C1.getX()) || (y != C1.getY())) // not same cell 
						&& 
					((x == C1.getX()) || (y == C1.getY()))) // 4 direction
				{
					if ((customMap[x][y] == Constants.BM_EXPLORED) ||
						(customMap[x][y] == Constants.BM_UNEXPLORED))
					{
						//Coordinate2D c = new Coordinate2D(x,y);
						neighbors.add(new Coordinate2D(x,y));
					}		
				}
			}
		}		
		
		double[][] customMap1 = new double[mapWidth][mapHeight];		
		for (int x=0; x<mapWidth; x++)
		{
			for (int y=0; y<mapHeight; y++)
			{
				customMap1[x][y] = customMap[x][y];  
			}
		}
		// Check off current cell as already checked
		customMap1[C1.getX()][C1.getY()] = Constants.BM_WALL;
		
		//Recursion
		for (int i=0; i<neighbors.size(); i++)
		{
			result = result || BMexistsPathStupidMethod(
					neighbors.elementAt(i),
					C2,
					customMap1);
			
			// if existsPath then true
			if (result) return result;
		}
		
		return result;	
	}
	
	/** Brick&Mortar functions
	 * Tells you if a path exists between two cells C1 and C2
	 * Used by "isBlocking" function
	 * */
	public boolean BMexistsPathDecentMethod(
			Coordinate2D C1, 
			Coordinate2D C2, 
			double[][] customMap)
	{
		Vector <Coordinate2D> frontier =
				new Vector <Coordinate2D>();
		frontier.add(C1); // add the source to the frontier
		customMap[C1.getX()][C1.getY()] = BM_PATH_SEEN;
		
		// Mark all the cells surrounding the frontier 
		// and update the frontier
		while (frontier.size() != 0)
		{
			// Pick an element
			Coordinate2D cell = frontier.elementAt(0);
			
			for (int x =  Math.max(0, cell.getX()-1);
					 x <= Math.min(cell.getX()+1, customMap.length-1); // customMap.length = mapWidth 
					 x++)
			{
				for (int y = Math.max(0, cell.getY()-1);
						y <= Math.min(cell.getY()+1, customMap[0].length-1); // customMap[0].length = mapHeight
						y++)
				{
					if (((x != cell.getX()) || (y != cell.getY())) // not same cell 
							&& 
						((x == cell.getX()) || (y == cell.getY()))) // 4 direction
					{
							/*
							if ((customMap[x][y] != Constants.BM_WALL)
								&&
								(customMap[x][y] != Constants.BM_CLOSED)
								&&
								(customMap[x][y] != BM_PATH_SEEN ))
							*/
							if (
								((customMap[x][y] == Constants.BMI_ATTRACTOR)
								||
								(customMap[x][y] == Constants.BM_UNEXPLORED)
								||
								(customMap[x][y] == Constants.BM_EXPLORED))
								&&
								(customMap[x][y] != BM_PATH_SEEN ))
							{
								customMap[x][y] = BM_PATH_SEEN;
								frontier.add(new Coordinate2D(x,y));
							}		
							
							if (C2.equals(new Coordinate2D(x,y)))
							{
								return true;
							}
					}
				}
			}
			// Remove the picked element from the frontier			
			frontier.removeElementAt(0);
		}// end while
		return false;
	}
	
	/**
	 * Brick&Mortar functions
	 * Decide if agent should terminate its operation
	 * 	ON A GIVEN MAP !!!
	 * Returns true if an agent should terminate
	 * 		no	if an agent should not terminate
	 */
	public boolean BMterminateOnMap(int x_in, int y_in, double[][] customMap)
	{
		for (int 	x =  Math.max(0, x_in-1);
					x <= Math.min(x_in+1, mapWidth-1); 
					x++)
		{
			for (int y = Math.max(0, y_in-1);
					y <= Math.min(y_in+1, mapHeight-1);
					y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if ((customMap[x][y] == Constants.BM_EXPLORED) ||
						(customMap[x][y] == Constants.BM_UNEXPLORED))
					return false;
				}
			}
		}		
		return true;
	}
	
	
	/**
	 * Brick&Mortar functions
	 * Decide if agent should terminate its operation
	 * Returns true if an agent should terminate
	 * 		no	if an agent should not terminate
	 */
	public boolean BMterminate(int x_in, int y_in)
	{
		for (int 	x =  Math.max(0, x_in-1);
					x <= Math.min(x_in+1, mapWidth-1); 
					x++)
		{
			for (int y = Math.max(0, y_in-1);
					y <= Math.min(y_in+1, mapHeight-1);
					y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if ((dataMap[x][y] == Constants.BM_EXPLORED) ||
						(dataMap[x][y] == Constants.BM_UNEXPLORED))
					return false;
				}
			}
		}		
		return true;
	}
	
	/**
	 * Brick&Mortar functions
	 * Calculate the number of visited cells or walls
	 * around a given cell  
	 */	
	public int BMcountWallsVisitedAround(int x_in, int y_in)
	{
		int wallsVisitedAround = 0; // Max = 4
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 directions
				{
					if ((dataMap[x][y] == Constants.BM_WALL) ||
						(dataMap[x][y] == Constants.BM_CLOSED))
					wallsVisitedAround++;
				}
			}
		}		
		return wallsVisitedAround;
	}
	
	/**
	 * Brick&Mortar functions
	 * Tells if there are unexplored cells
	 * around a given cell  
	 */	
	public boolean BMexistUnexploredCellsAround(int x_in, int y_in)
	{
		int unexploredAround = 0; // Max = 4
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (dataMap[x][y] == Constants.BM_UNEXPLORED)
					unexploredAround++;	
				}
			}
		}	
		if (unexploredAround > 0)
			return true;
		else //if (unexploredAround == 0)
			return false;
	}
	

	/**
	 * Brick&Mortar functions
	 * Gets you (one of) the best unexplored neighbour cells
	 * returning the one with most walls/visited cells around
	 */
	public Coordinate2D BMgetBestUnexplored(int x_in, int y_in)
	{
		Coordinate2D result = new Coordinate2D(); 
		Vector <Coordinate2D> maxs = new Vector <Coordinate2D>(); // List of best unexplored cells
		int maxWallVisitedNeighbours=0; // maximum = 3
		
		// Check 4 directions
		/* Step1: Calculate the maximum number of walls/visited around a neighbour cell */
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (dataMap[x][y] == Constants.BM_UNEXPLORED)
					{
						int tmp = BMcountWallsVisitedAround(x,y);
						// New max found
						if (maxWallVisitedNeighbours < tmp)
						{
							maxWallVisitedNeighbours = tmp;
							maxs.clear();
							maxs.add(new Coordinate2D(x,y));
						}
						// Equal to max found
						else if (maxWallVisitedNeighbours == tmp)
						{
							maxs.add(new Coordinate2D(x,y));
						}
					}
				}
			}
		}	
		
		// Choose one max among several
		result = maxs.elementAt((int) (	
				(Viewer.randomGenerator.nextDouble()*100)%maxs.size()
										));
		result.setValue(dataMap[result.getX()][result.getY()]);
		return result;
	}
	
	/**
	 * Brick&Mortar functions
	 * Gets you (one of) the best explored neighbour cells
	 * returning the one with most walls/visited cells around
	 */
	public Coordinate2D BMgetBestExplored(int x_in, int y_in, int agentID, Coordinate2D previousCell)
	{
		//Coordinate2D previousCell = getPreviousCell(x_in, y_in, agentID);				
		Coordinate2D result = new Coordinate2D(); 
		Vector <Coordinate2D> unvisitedCells = new Vector <Coordinate2D>(); // List of best unexplored cells
		
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (	 (dataMap[x][y] == Constants.BM_EXPLORED) &&
							!(	(x == previousCell.getX()) && 
								(y == previousCell.getY())	 
						  	)) // Cell(t-1) != Cell(t+1)			
					{
						/*
						int tmp = countWallsVisitedAround(x,y);
						// New max found
						if (maxWallVisitedNeighbours < tmp)
						{
							maxWallVisitedNeighbours = tmp;
							maxs.clear();
							maxs.add(new Coordinate2D(x,y));
						}
						// Equal to max found
						else if (maxWallVisitedNeighbours == tmp)
						{
						*/
						unvisitedCells.add(new Coordinate2D(x,y));
						//}
					}
				}
			}
		}	
		
		// Choose one explored cell around 
		// among several explored cells, if any exist 
		// (exclude the Cell[t-1])
		if (unvisitedCells.size() > 0)
		{
			result = 
					unvisitedCells.elementAt(
							(int) ((Viewer.randomGenerator.nextDouble()*100)%unvisitedCells.size()));
			// TODO pick a direction per agentID
			return result;			
		}
		// If the only explored cell around is the
		// C[t-1] cell, then return it
		else // if (dataMap[previousCell.getX()][previousCell.getY()] == BM_EXPLORED)
		{
			if (DEBUG)
			{
				System.out.println("Didn't find anything better than " +
					"the previous cell: " +
					previousCell.getX() + ", " +
					previousCell.getY());
			}
			return previousCell;
		}
		// Otherwise terminate !
		// Termination should be checked before this function
		// so as to avoid returning a void cell coordinate		
	}	
	
	/**
	 * Brick&Mortar functions
	 * Gets you (one of) the best explored neighbour cells
	 * returning the one wich was visited THE LEAST NUMBER OF TIMES
	 * FUNCTION SPECIFIC FOR BMA4
	 */
	public Coordinate2D BMA4getBestExplored(int x_in, int y_in)
	{
		//Coordinate2D previousCell = getPreviousCell(x_in, y_in, agentID);				
		Coordinate2D result = new Coordinate2D(); 
		Vector <Coordinate2D> exploredCells = new Vector <Coordinate2D>(); // List of best unexplored cells
		
		int minX =-1;
		int minY =-1;
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (dataMap[x][y] == Constants.BM_EXPLORED)		
					{
						if (minX == -1) //value not set
						{
							minX = x;
							minY = y;
							exploredCells.add(new Coordinate2D(x,y));
						}
						else
						{
							if (BMCommonDispersionMap[x][y] < 
								BMCommonDispersionMap[minX][minY])
							{
								exploredCells.clear();
								exploredCells.add(new Coordinate2D(x,y));
							}
							else if (BMCommonDispersionMap[x][y] == 
									BMCommonDispersionMap[minX][minY])
							{							
								exploredCells.add(new Coordinate2D(x,y));
							}
						}
							
						/*
						int tmp = countWallsVisitedAround(x,y);
						// New max found
						if (maxWallVisitedNeighbours < tmp)
						{
							maxWallVisitedNeighbours = tmp;
							maxs.clear();
							maxs.add(new Coordinate2D(x,y));
						}
						// Equal to max found
						else if (maxWallVisitedNeighbours == tmp)
						{
						*/
						//exploredCells.add(new Coordinate2D(x,y));
						//}
					}
				}
			}
		}	
		
		// Choose one explored cell around 
		// among several explored cells, if any exist 
		// (exclude the Cell[t-1])
		if (exploredCells.size() > 0)
		{
			result = 
					exploredCells.elementAt(
							(int) ((Viewer.randomGenerator.nextDouble()*100)%exploredCells.size()));
			// TODO pick a direction per agentID
			return result;			
		}
		// If the only explored cell around is the
		// C[t-1] cell, then return it
		else // if (dataMap[previousCell.getX()][previousCell.getY()] == BM_EXPLORED)
		{
			if (DEBUG)
			{
				System.err.println("THIS SHOULD NOT HAPPEN (BMA4getBestExplored)");
				System.exit(0);
			}
			return (new Coordinate2D(-1,-1));
		}
		// Otherwise terminate !
		// Termination should be checked before this function
		// so as to avoid returning a void cell coordinate		
	}	
	
	/**
	 * Brick&Mortar functions
	 * Gets you (one of) the best explored neighbour cells
	 * returning the one with most walls/visited cells around
	 */
	public Coordinate2D BMgetBestExploredLeastInTraces(int x_in, int y_in)
	{
		//Coordinate2D previousCell = getPreviousCell(x_in, y_in, agentID);				
		Coordinate2D result = new Coordinate2D(); 
		Vector <Coordinate2D> exploredCells = new Vector <Coordinate2D>(); // List of best unexplored cells
		
		int minX = -1;
		int minY = -1;

		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (dataMap[x][y] == Constants.BM_EXPLORED)		
					{
						if (minX == -1) //value not set
						{
							minX = x;
							minY = y;
							exploredCells.add(new Coordinate2D(x,y));
						}
						else
						{
							// TODO modify here
							// TODO count the number of direction traces
							// that go into the cell, and not out of it
							// (which means all traces - traces comming to you)
							if (BMCommonDispersionMap[x][y] < 
								BMCommonDispersionMap[minX][minY])
							{
								exploredCells.clear();
								exploredCells.add(new Coordinate2D(x,y));
							}
							else if (BMCommonDispersionMap[x][y] == 
									BMCommonDispersionMap[minX][minY])
							{							
								exploredCells.add(new Coordinate2D(x,y));
							}
						}
					}
				}
			}
		}	
		
		// Choose one explored cell around 
		// among several explored cells, if any exist 
		// (exclude the Cell[t-1])
		if (exploredCells.size() > 0)
		{
			result = 
					exploredCells.elementAt(
							(int) ((Viewer.randomGenerator.nextDouble()*100)%exploredCells.size()));
			return result;			
		}
		// If the only explored cell around is the
		// C[t-1] cell, then return it
		else // if (dataMap[previousCell.getX()][previousCell.getY()] == BM_EXPLORED)
		{
			if (DEBUG)
			{
				System.err.println("THIS SHOULD NOT HAPPEN");
				System.exit(0);
			}
			return new Coordinate2D(-1,-1);
		}
		// Otherwise terminate !
		// Termination should be checked before this function
		// so as to avoid returning a void cell coordinate		
	}	
	
	/** Brick&Mortar functions
	 *  An intersection is
	 *  the cell with at least one explored
	 *  neighbor cell that does not belong in the loop (of an agent A)
	 * */
	public boolean BMisIntersection (int x_in, int y_in, int agentID_in)
	{
		/** BMI addition */
		if (dataMap[x_in][y_in] == Constants.BMI_ATTRACTOR)
		{
			return true;
		}
		/** BMI end */
		
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (dataMap[x][y] == Constants.BM_UNEXPLORED)
						return true;
					
					if ( (dataMap[x][y] == Constants.BM_EXPLORED) &&
						 (BMIDsMap[x][y] != agentID_in)	)	// not part of the loop
					{
						return true;
					}
				}
			}
		}
		if (Constants.DEBUG)
			System.out.println("[" + x_in + ", " + y_in + "] is not an intersection");
		return false;
	}
	
	/** Brick&Mortar functions
	 *  An intersection is
	 *  the cell with at least one explored
	 *  neighbor cell that does not belong in the loop (of an agent A)
	 * */
	public boolean BMisCrossIntersection (int x_in, int y_in, int agentID_in)
	{
		int totalNonLoopCells = 0;
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (dataMap[x][y] == Constants.BM_UNEXPLORED)
						totalNonLoopCells++;
					
					if (	 (dataMap[x][y] == Constants.BM_EXPLORED) 
							&&
							 (BMIDsMap[x][y] != agentID_in)
						) // Cell(t-1) != Cell(t+1)			
					{
						totalNonLoopCells++;
					}
				}
			}
		}
		if (totalNonLoopCells > 1) 
			return true;
		else
			return false;
	}
	
	public boolean BMisTIntersection (int x_in, int y_in, int agentID_in)
	{
		int totalNonLoopCells = 0;
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if (dataMap[x][y] == Constants.BM_UNEXPLORED)
						totalNonLoopCells++;
					
					if (	 (dataMap[x][y] == Constants.BM_EXPLORED) 
							&&
							 (BMIDsMap[x][y] != agentID_in)
						) // Cell(t-1) != Cell(t+1)			
					{
						totalNonLoopCells++;
					}
				}
			}
		}
		if (totalNonLoopCells == 1) 
			return true;
		else
			return false;
	}
	
	
	/** BM get previous cell */
	public Coordinate2D BMgetPreviousCell(int x_in, int y_in, int agentID_in)
	{
		Coordinate2D result = new Coordinate2D(-1, -1);
		int maxTimestamp = -1;
		
		//maxTimestamp = BMIndividualTraceMap[x][y][agentID_in];
		
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					//System.out.println("Cell [" + x+", " + y + "] has Timestamp "
							//+ BMIndividualTraceMap[x][y][agentID_in]);
					if ((BMnextCellsMap[x][y][agentID_in].equals(new Coordinate2D(x_in, y_in))) &&
						(BMIndividualTraceMap[x][y][agentID_in] > maxTimestamp) //&&
						//(BMIndividualTraceMap[x][y][agentID_in] < BMIndividualTraceMap[x_in][y_in][agentID_in]) // BMILV (2015.02.09)
						)
					{
						maxTimestamp = BMIndividualTraceMap[x][y][agentID_in];
						result = new Coordinate2D(x,y);
					}
				}
			}
		}
		
		return result;
	}
	
	/** BM get next cell */
	public Coordinate2D BMgetNextCell(int in_x, int in_y, int in_agentID)
	{
		return BMnextCellsMap[in_x][in_y][in_agentID];
	}
	
	public void BMsetNextCell(int in_x, int in_y, int in_agentID, Coordinate2D in_cell)
	{
		BMnextCellsMap[in_x][in_y][in_agentID] = in_cell;
	}
	
	
	/** Return the previous cell for loop cleaning */
	public Coordinate2D BMgetPreviousCellLoopCleaning(int x_in, int y_in, int agentID_in)
	{
		Coordinate2D result = new Coordinate2D(-1, -1);
		
		int maxTimestamp = -1;
		
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if ((BMIDsMap[x][y] == agentID_in) &&
						(BMnextCellsMap[x][y][agentID_in].equals(new Coordinate2D(x_in, y_in))) &&
						(BMIndividualTraceMap[x][y][agentID_in] > maxTimestamp) 
						)
					{
						maxTimestamp = BMIndividualTraceMap[x][y][agentID_in]; 
						result = new Coordinate2D(x,y);						
					}
				}
			}
		}
		
		return result;
	}
	
	/** Returns true if there are no closed cells with IDs around */
	public boolean BMnoClosedCellsWithIDAround(int x_in, int y_in)
	{
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				x <= Math.min(x_in+1, mapWidth-1); 
				x++)
		{
			for (int y = Math.max(0, y_in-1);
					y <= Math.min(y_in+1, mapHeight-1);
					y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 direction
				{
					if ((dataMap[x][y] == Constants.BM_CLOSED) &&
						(BMIDsMap[x][y] != Constants.BM_NO_AGENT_TRACE))
					return false;
				}
			}
		}
		
		return true;
	}
	
	/** Brick&Mortar functions
	 * Returns the previous cell for a given agent 
	 * Uses the individual trace map for identifying the previous cell
	 * 
	 * Allows to know which cell to avoid when making a step
	 * */
	public Coordinate2D BMgetPreviousCellWithTimestamps(int x_in, int y_in, int agentID_in)
	{
		int currentCellTimestamp = BMIndividualTraceMap[x_in][y_in][agentID_in]; 
		Coordinate2D result = new Coordinate2D(-1, -1);
		
		int maxPrevious = -1;
		
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in)) && // 4 direction
					(BMIndividualTraceMap[x][y][agentID_in] < 
						currentCellTimestamp )	 && 
					(BMIndividualTraceMap[x][y][agentID_in] > maxPrevious)) // previous timestamp
				{
					maxPrevious = BMIndividualTraceMap[x][y][agentID_in]; 
					result = new Coordinate2D(x,y);
				}
			}
		}
		
		if (result.getX() == -1)
		{
			if (DEBUG)
			{
				System.out.println("Attention: getPreviousCell has not found a previous cell !");
			}
		}
		return result;
	}
	
	/** Brick&Mortar functions
	 * Returns the previous cell for a given agent
	 * during his BACKWARD movement in the 
	 * Loop cleaning phase
	 */
	public Coordinate2D BMgetNextCellLoopClosingWithTimestamps(int x_in, int y_in, int agentID_in)
	{
		int currentCellTimestamp = BMIndividualTraceMap[x_in][y_in][agentID_in]; 
		Coordinate2D result = new Coordinate2D(-1, -1);
		
		int minNext = Integer.MAX_VALUE;
		
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in)) && // 4 direction
					(dataMap[x][y] != Constants.BM_WALL) &&
					(dataMap[x][y] != Constants.BM_CLOSED) &&
					(BMIndividualTraceMap[x][y][agentID_in] > 
						currentCellTimestamp )	 && 
					(BMIndividualTraceMap[x][y][agentID_in] < minNext)) // previous timestamp
				{
					minNext = BMIndividualTraceMap[x][y][agentID_in]; 
					result = new Coordinate2D(x,y);		
				}
			}
		}
	
		//System.out.println("NextCell for [" + x_in + ", " + y_in + "] is" +
				//"[" + result.getX() + ", " + result.getY() + "]");
		
		if (result.getX() == -1)
		{
			if (DEBUG)
			{
				System.out.println("Attention: getPreviousCellLoopCleaning has not found a previous cell !");
			}
		}
		return result;
	}
	
	
	
	
	/** Set a cell value on the feature map */
	/*
	public void BMsetFeatureMap(int in_x, int in_y, int in_feature)
	{
		BMfeatureMap[in_x][in_y] = in_feature;
	}
	*/
	
	/** Get a cell value on the feature map */
	/*
	public int BMgetFeatureMap(int in_x, int in_y)
	{
		return BMfeatureMap[in_x][in_y];
	}
	*/
	
	/** Returns true if an agent has already visited a given cell
	 * (checks if the IndividualTraceMap of agentID contains a value different of 0)
	 * */
	/*
	public boolean BMalreadyBeenHere(int in_x, int in_y, int in_agentID)
	{
		if (BMIndividualTraceMap[in_x][in_y][in_agentID] != 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	*/
	
	/** Set the value of a cell for the LRTA map used by BMI 
	 * It is used to break ties among equal explored/unexplored cells
	 * */
	public void BMupdateLRTAMap(int x_in, int y_in)
	{
		/* 1. Find the explored/unexplored cell with the minimum value V
		 * 	among the 4 surrounding cells
		 * 2. Set V+1 for the current cell.
		 */
		int minLRTAValue = Integer.MAX_VALUE;
		//int minX;// =-1;
		//int minY;// =-1;
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 directions
				{
					if ((BMCommonDispersionMap[x][y] < minLRTAValue) &&
						(dataMap[x][y] != Constants.BM_WALL) &&
						(dataMap[x][y] != Constants.BM_CLOSED))
					{
						minLRTAValue = BMCommonDispersionMap[x][y];
						//minX = x;
						//minY = y;
					}
				}
			}
		}
		
		// Set the current cell to Max(itself, 1+minNeighbour)
		BMCommonDispersionMap[x_in][y_in] = 1 + minLRTAValue;
	}
	
	/** Set the value of a cell for the LRTA map used by BMI 
	 * It is used to break ties among equal explored/unexplored cells
	 * */
	public void BMILVupdateLRTAMap(int x_in, int y_in)
	{
		/* 1. Find the explored/unexplored cell with the minimum value V
		 * 	among the 4 surrounding cells
		 * 2. Set V+1 for the current cell.
		 */
		int minLRTAValue = Integer.MAX_VALUE;
		int totalOpenCellsAround=0;
		
		if (dataMap[x_in][y_in] == Constants.BMI_ATTRACTOR)
		{
			// Set the current cell to Max infinity
			BMCommonDispersionMap[x_in][y_in] = Integer.MAX_VALUE;
			return;
		}
		
		boolean oneSideIsRoadToRDV = false;
		
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 directions
				{
					if ((dataMap[x][y] != Constants.BM_WALL) &&
						(dataMap[x][y] != Constants.BM_CLOSED))
					{
						totalOpenCellsAround +=1;
						
						if (BMCommonDispersionMap[x][y] == Integer.MAX_VALUE)
						{
							oneSideIsRoadToRDV = true;
						}
						
						if (BMCommonDispersionMap[x][y] < minLRTAValue)
						{
							minLRTAValue = BMCommonDispersionMap[x][y];
						}
					}
				}
			}
		}
		
		if  (
				(
					(totalOpenCellsAround == 2) && (oneSideIsRoadToRDV)
					&&
					(this.BMgetIDsMap(x_in, y_in) == Constants.BM_NO_AGENT_TRACE) && // no agent trace on this cell
					(this.BMnoClosedCellsWithIDAround(x_in, y_in)) && // no agent traces around
					(Viewer.BMILV_noAgentOnClosedCellAround(x_in, y_in)) // no agents on closed cells around this cell
					// && (Viewer.countAgentsAt(x_in, y_in) == 1)
				) 
				||
				(
					(totalOpenCellsAround == 1) &&
					(oneSideIsRoadToRDV)
				)
			)
		{
			// Say that this path leads to the RDV point. 
			// No point in taking it before ending the exploration.
			BMCommonDispersionMap[x_in][y_in] = Integer.MAX_VALUE;
			return;	
		}
		
		// Set the current cell to Max(itself, 1+minNeighbour)
		BMCommonDispersionMap[x_in][y_in] = 1 + minLRTAValue;
	}
	
	/** Set the value of a cell for the LRTA map used by BMI 
	 * It is used to break ties among equal explored/unexplored cells
	 * */
	public void BMupdateThrunMap(int x_in, int y_in)
	{
		/* 1. Find the explored/unexplored cell with the minimum value V
		 * 	among the 4 surrounding cells
		 * 2. Set V+1 for the current cell.
		 */
		int minLRTAValue = Integer.MAX_VALUE;
		int minX =-1;
		int minY =-1;
		// Check 4 directions
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 directions
				{
					if ((BMCommonDispersionMap[x][y] < minLRTAValue) &&
						(dataMap[x][y] != Constants.BM_WALL) &&
						(dataMap[x][y] != Constants.BM_CLOSED))
					{
						minLRTAValue = BMCommonDispersionMap[x][y];
						minX = x;
						minY = y;
					}
				}
			}
		}
		
		// Set the current cell to Max(itself, 1+minNeighbour)
		BMCommonDispersionMap[x_in][y_in] = 
			Math.max(1+BMCommonDispersionMap[x_in][y_in], 
					 1 + minLRTAValue);
	}
	
	/** Set the value of a cell for the LRTA map used by BMI 
	 * It is used to break ties among equal explored/unexplored cells
	 * */
	public void BMupdateTimestampMap(int x_in, int y_in, int timestamp_in)
	{
		BMCommonDispersionMap[x_in][y_in] = timestamp_in;
	}
	
	/** Set the value of a cell for the NodeCounting map used by BMI 
	 * It is used to break ties among equal explored/unexplored cells
	 * */
	public void BMupdateNodeCountingMap(int x_in, int y_in)
	{
		BMCommonDispersionMap[x_in][y_in]++;
	}
	
	/** Get the value of a cell for the LRTA map used by BMI 
	 * It is used to break ties among equal explored/unexplored cells
	 * */
	public int BMgetLRTAMap(int in_x, int in_y)
	{
		return BMCommonDispersionMap[in_x][in_y];	
	}
	
	/**
	 * Takes a list of cells as input
	 * and returns a list of cells having most walls or closed cells around them
	 */
	public Vector<Coordinate2D> BMreturnCellsWithMostClosedWallsAround(Vector<Coordinate2D> cells_in)
	{
		Vector<Coordinate2D> result = new Vector<Coordinate2D>();
		
		int maxWalls = -1;
		for (int cell=0; cell<cells_in.size(); cell++)
		{
			int cellWalls = BMcountWallsVisitedAround(
					cells_in.get(cell).getX(), 
					cells_in.get(cell).getY());
			if (cellWalls > maxWalls)
			{
				maxWalls = cellWalls;
				result.clear(); // remove all cells with an inferior number of walls around
				result.add(cells_in.elementAt(cell));
			}
			else if (cellWalls == maxWalls)
			{
				result.add(cells_in.elementAt(cell));
			}
			// else do nothing with a worse cell
		}
		
		return result;
	}
	
	/**
	 * Takes a list of cells as input
	 * and returns the ones that have a minimal LRTA value
	 */
	public Vector<Coordinate2D> BMreturnCellsWithMinLRTA(Vector<Coordinate2D> cells_in)
	{
		Vector<Coordinate2D> result = new Vector<Coordinate2D>(); 
		
		int minLRTA = Integer.MAX_VALUE;
		
		for (int cell=0; cell<cells_in.size(); cell++)
		{
			int cellLRTA = BMCommonDispersionMap[cells_in.get(cell).getX()][cells_in.get(cell).getY()];
			if (cellLRTA < minLRTA)
			{
				minLRTA = cellLRTA;
				result.clear();
				result.add(cells_in.elementAt(cell));
			}
			else if (cellLRTA == minLRTA)
			{
				result.add(cells_in.elementAt(cell));
			}
			// else do nothing with a worse cell
		}
		
		return result;
	}
	
	/**
	 * Takes a list of cells as input
	 * Selects only the ones having a higher priority
	 * (Selects all the unexplored among the explored ones,
	 * 	or selects all the explored among explored if no unexplored present) 
	 * */
	public Vector<Coordinate2D> BMreturnPriorityCells(Vector<Coordinate2D> cells_in)
	{
		Vector<Coordinate2D> result = new Vector<Coordinate2D>(); 
		
		for (int cell=0; cell<cells_in.size(); cell++)
		{
			if (dataMap[cells_in.get(cell).getX()][cells_in.get(cell).getY()] ==
				Constants.BM_UNEXPLORED)
			{
				// Add unexplored if none in results
				if (result.size() == 0)
				{
					result.add(cells_in.get(cell));
				}
				// If cells already added,
				// 	see if they are explored or not.
				// If explored -> clear the result set
				// 	and insert the current cell
				else if (result.size() > 0)
				{
					if ((result.elementAt(0).getValue() == Constants.BM_EXPLORED) ||
						(result.elementAt(0).getValue() == Constants.BMI_ATTRACTOR))
					{
						result.clear();
						result.add(cells_in.get(cell));
					}
					else if (result.elementAt(0).getValue() == Constants.BM_UNEXPLORED)
					{
						result.add(cells_in.get(cell));
					}
				}
			}
			else if ((dataMap[cells_in.get(cell).getX()][cells_in.get(cell).getY()] == Constants.BM_EXPLORED) ||
					 (dataMap[cells_in.get(cell).getX()][cells_in.get(cell).getY()] == Constants.BMI_ATTRACTOR))
			{
				// Add unexplored if none in results
				if (result.size() == 0)
				{
					result.add(cells_in.get(cell));
				}
				else if (result.size() > 0)
				{
					if ((result.elementAt(0).getValue() == Constants.BM_EXPLORED) ||
						(result.elementAt(0).getValue() == Constants.BMI_ATTRACTOR))
					{
						result.add(cells_in.get(cell));
					}
				}
			}
			
		}
		
		return result;
	}
	
	/** 
	 * Returns the next cell for BM_Simplified
	 * Takes into account 
	 * 1. The cell priority (Unexplored > Explored > Closed)
	 * 2. The number of walls around
	 * 3. The LRTA value on these cells to break ties
	 * 4. If ties still exist, choose at random.
	 */
	public Coordinate2D BMSimplifiedGetNextCell(int x_in, int y_in)
	{
		Vector <Coordinate2D> result = new Vector<Coordinate2D>();
		
		//System.out.println("Neighbours of x=" + x_in + ", y=" + y_in);
		for (int x =  Math.max(0, x_in-1);
			 x <= Math.min(x_in+1, mapWidth-1); 
			 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 directions
				{
					//System.out.println("Checking x=" + x + ", y=" + y);
					if ((dataMap[x][y] != Constants.BM_WALL) &&
						(dataMap[x][y] != Constants.BM_CLOSED))
					{
						Coordinate2D newCell = new Coordinate2D();
						newCell.setX(x);
						newCell.setY(y);
						newCell.setValue(dataMap[x][y]);
						result.add(newCell);
					}
				}
			}
		}
		
		result = BMreturnPriorityCells(result);
		result = BMreturnCellsWithMinLRTA(result);
		result = BMreturnCellsWithMostClosedWallsAround(result);
		
		if (result.size() == 1)
		{
			return result.elementAt(0);
		}
		else if (result.size() > 1)// Choose random
		{
			return result.elementAt((int) (Viewer.randomGenerator.nextDouble()*result.size()));
		}
		else // if (result.size() == 0)
		{
			Coordinate2D nextCell = new Coordinate2D();
			nextCell.setX(x_in);
			nextCell.setY(y_in);
			return (nextCell); 
		}
	}
	
	/** Returns a list of cells, that are neighbours to the cell 
	 * with the input coordinates */
	public Vector<Coordinate2D> BMgetNeighbourCells(int x_in, int y_in)
	{
		Vector<Coordinate2D> result = new Vector<Coordinate2D>();
		
		for (int x =  Math.max(0, x_in-1);
				 x <= Math.min(x_in+1, mapWidth-1); 
				 x++)
		{
			for (int y = Math.max(0, y_in-1);
					 y <= Math.min(y_in+1, mapHeight-1);
					 y++)
			{
				if (((x != x_in) || (y != y_in)) && // not same cell
					((x == x_in) || (y == y_in))) // 4 directions
				{
					Coordinate2D newCell = new Coordinate2D(x,y);
					//newCell.setX(x);
					//newCell.setY(y);
					newCell.setValue(dataMap[x][y]);
					result.add(newCell);
				}
			}
		}
		
		return result;
	}
	
	
	/** Excludes all instances of a cell having the given coordinates
	 * from a given list of cells, and returns the resulting list */
	public Vector<Coordinate2D> BMexcludeCellFromList(int in_x, int in_y, Vector<Coordinate2D> in_cells)
	{
		Vector<Coordinate2D> result = new Vector<Coordinate2D>();
		
		for (int cell=0; cell < in_cells.size(); cell++)
		{
			if ((in_cells.elementAt(cell).getX() == in_x) &&
				(in_cells.elementAt(cell).getY() == in_y))
			{
				// Do nothing
			}
			else
			{
				result.add(in_cells.elementAt(cell));
			}
		}
		
		return result;
	}
	
	
	

	/** Returns true if this is a frontier cell. 
	 *  Returns false otherwise. FED = Frontier Explorator Distributed */
	public boolean FEDisFrontierCell(Coordinate2D cell_in)
	{
		// The cell should be unexplored.
		if (this.dataMap[cell_in.getX()][cell_in.getY()] != Constants.BM_UNEXPLORED)
			return false;
		
		// All the frontier cells are detected by this loop
		for (int 	x=Math.max(0, cell_in.getX() - 1);
					x<=Math.min(cell_in.getX()+1, dataMap.length-1);
					x+=1)
		{
			for (int 	y=Math.max(0, cell_in.getY() - 1);
						y<=Math.min(cell_in.getY()+1, dataMap[0].length-1);
						y+=1)
			{
				if (	((x!=cell_in.getX()) || (y!=cell_in.getY())) // not same cell
						//&&
						//((x==cell_in.getX()) || (y==cell_in.getY())) // 4 directions
					)
				{
					if (dataMap[x][y] == Constants.BM_EXPLORED)
						return true;
				}
			}
		}
		
		// If no frontier cell was detected untill now, then this is not a frontier cell.
		return false;
	}
}
