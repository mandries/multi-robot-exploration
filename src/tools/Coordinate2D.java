package tools;

//import java.util.Vector;

public class Coordinate2D 
{
	private int x;
	private int y;
	private double timeValue;

	/*************  AntWalk2 vars  ****************/
//	private int AW2firstInTime = 0;
//	private int AW2firstOutTime = 0;
//	private Vector<AW2CellLink> links;
	/**********************************************/
	
	public Coordinate2D(){}
	
	public Coordinate2D(int xin, int yin)
	{
		x = xin;
		y = yin;
		
//		AW2firstInTime = 0;
//		AW2firstOutTime = 0;
		
//		links = new Vector<AW2CellLink>();
	}
	
	public void setX(int in){x = in;}
	public void setY(int in){y = in;}
	public void setValue(double in){timeValue = in;}
//	public void AW2setFirstInTime(int in){AW2firstInTime = in;}
//	public void AW2setFirstOutTime(int in){AW2firstOutTime = in;}
	
	public int getX(){return x;}
	public int getY(){return y;}
	//public int AW2getFirstInTime(){return AW2firstInTime;}
	//public int AW2getFirstOutTime(){return AW2firstOutTime;}
	public double getValue(){return timeValue;}
	//public int AW2getInTime(){return inTime;}
	//public int AW2getOutTime(){return outTime;}
	
	// Return only the outgoing links !
	// CAUTION: Each cell keeps trace only of its outgoing links
//	public Vector<AW2CellLink> AW2getLinks(){return links;}
	
	public boolean equals(Coordinate2D in)
	{
		//System.out.println("Comparing [" + x + ", " + y + "] to " +
		//				"[" + in.getX() + ", " + in.getY() +"]");
		if (	(x == in.getX()) &&
				(y == in.getY()))
		{
			return true;
		}
		return false;
	}

	/** Returns the euclidean distance between the two points inside a space containing no obstacles. */
	public double getEuclideanDistanceTo(Coordinate2D targetCoordinate)
	{
		return Math.sqrt(
					Math.pow(this.x - targetCoordinate.x, 2) + 
					Math.pow(this.y - targetCoordinate.y, 2) 
					);
	}
	
	public void print()
	{
		System.out.println("Coord: [" + x + ", " + y + "]");
	}
}
