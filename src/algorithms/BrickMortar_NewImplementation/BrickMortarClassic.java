package algorithms.BrickMortar_NewImplementation;

import java.util.Vector;

import algorithms.Agent;

import tools.Constants;
import tools.Coordinate2D;
import tools.Map2D;

public class BrickMortarClassic
extends Agent
{
	// The position of the agent on the grid
	private int currentX;
	private int currentY;
	
	private int agentID;
	
	// The local clock used for time-stamps
	private int localTime = 0;
	
	// Total number of steps made by the agent
	private int totalSteps = 0;
	
	// A vector containing the route taken by the agent
	private Vector <Coordinate2D> lastTrip = new Vector <Coordinate2D>();
	
	// (A pointer to) the map on which the agent travels
	//	and which is shared with other agents
	// Maps for synchrony
	private Map2D oldMap; // The map from last step (orientation only for the current step)
	private Map2D currentMap; // The map that is modified at the current step
	
	private int agentState;
	
	public BrickMortarClassic(Map2D inputOldMap, Map2D inputNewMap, int x, int y, int agentID_in)
	{
		oldMap = inputOldMap;
		currentMap = inputNewMap;
		currentX = x;
		currentY = y;
		agentID = agentID_in;
		lastTrip.add(new Coordinate2D(currentX, currentY));
		// Set the initial agent state to loop detection
		agentState = Constants.BM_AGENT_LOOP_DETECTION;
	}
	
	/** Synchronization function (used to update the old map) */
	public void BMsetOldMap(Map2D inputMap)
	{
		oldMap = inputMap;
	}
	/** Synchronization function (used to update the current map) */
	public void BMsetCurrentMap(Map2D inputMap)
	{
		currentMap = inputMap;
	}
	
	
	public int getAgentSteps() {return totalSteps;}
	public int getX(){return currentX;}
	public int getY(){return currentY;}
	public int AW2getSearchLevel(){return -1;}
	public boolean AW2getAllVisited(){return false;}
	public Vector <Coordinate2D> getLastTrip(){return lastTrip;}
	
	/*
	 * TODO problems to solve: 
	 * 		agent A blocks agent B
	 * 		looping forms when force them to make a step
	 * 		(cleaning -> control -> detection)
	 */
	
	
	/* Forces 1..N calls to the algorithm to make a single step */
	// FIXED a problem may arise here when exploration ends
	//	OR when agent in standby
	//	OR when agent is OFF
	// the "step" function will loop forever
	public void step()
	{	
		// Call the algorithm while a step is not made
		localTime++;
		boolean stepMade; // = false;
		do 
		{
			stepMade=call();
			lastTrip.add(new Coordinate2D(currentX, currentY));
		}while ((!stepMade) && 
				(agentState != Constants.BM_AGENT_STANDBY) &&
				(agentState != Constants.BM_AGENT_OFF));
		totalSteps++;
	}
	
	/* Make a call to the BrickMortar algorithm*/
	public boolean call()	
	{
		boolean stepMade = false;
		if (Constants.DEBUG)
			System.out.println("Agent" + agentID + " at [" + currentX + ", " + currentY + "], " +
					"which is " + oldMap.getPhysicalMapValue(currentX, currentY));
		/* The call first checks the state of the agent
		 * 	and then performs an action
		 * */
		
		/*********************\
		|***** DETECTION *****|
		\*********************/
		if (agentState == Constants.BM_AGENT_LOOP_DETECTION)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP DETECTION");
		
			Coordinate2D tmpCell = oldMap.BMgetPreviousCell(currentX, currentY, agentID);
			if (Constants.DEBUG)
				System.out.println("Previous cell is: [" + tmpCell.getX() + ", " + tmpCell.getY() + "]");
			
			/*************/
			/** Marking **/
			/*************/

			/* 1. Already been in this cell */
			if ((oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_EXPLORED) &&
				// Not from opposite direction
				(!oldMap.BMgetNextCell(currentX, currentY, agentID).equals
				 (oldMap.BMgetPreviousCell(currentX, currentY, agentID)))	&&
				// THIS Agent has once passed through this cell
				 (!oldMap.BMgetNextCell(currentX, currentY, agentID).equals(new Coordinate2D(-1, -1))) &&
				 (oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE))
			{
				agentState = Constants.BM_AGENT_LOOP_CONTROL;
				stepMade = false;
				return stepMade;
			}			
			/* 2. Agent hasn't yet visited this cell 
			 * and cell is not blocking anything */
			// and cell not controlled by someone else
			else if ((!oldMap.BMisBlocking(currentX, currentY, 1 /* vision range*/)) &&
					(!currentMap.BMisBlocking(currentX, currentY, 1 /* vision range*/)) &&
					(currentMap.BMnoClosedCellsWithIDAround(currentX, currentY)))
			{
				// TODO if all agents start controlling loops, then no one can close
				// better to say if all cells are filled with IDs
				// then there's a deadlock
				 if (oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE)
				 {
					// 1. BM map 
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
					currentMap.BMsetNextCell(currentX, currentY, agentID, new Coordinate2D(-1, -1));					 
				 }
				 else // if (map.BMgetIDsMap(currentX, currentY) != Constants.BM_NO_AGENT_TRACE)
				 {
					 // Check if no cells with this ID are around
				 }
			}// end of !isBlocking			
			else if (currentMap.BMisBlocking(currentX, currentY, 1 /* vision range*/))
			{
				// 1. BM map 
				if (currentMap.getPhysicalMapValue(currentX, currentY) != Constants.BM_CLOSED)
				{
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_EXPLORED);
				}
			}// end of isBlocking
			// 2. Common dispersion map
			//map.BMupdateTimestampMap(currentX, currentY, localTime);
			currentMap.BMupdateNodeCountingMap(currentX, currentY);
			//map.BMupdateLRTAMap(currentX, currentY);
			//map.BMupdateThrunMap(currentX, currentY);
						
			// 3. Individual trace map
			currentMap.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);	
			
			
			/****************/
			/** Navigation **/
			/****************/
			Vector<Coordinate2D> nextCells = new Vector<Coordinate2D>();
			nextCells = oldMap.BMgetNeighbourCells(currentX, currentY);
			nextCells = oldMap.BMreturnPriorityCells(nextCells);

			// If it is the only attainable cell
			if (nextCells.size() == 1)
			{
				Coordinate2D nextCell =	nextCells.elementAt(0);

				// 2. NextCellMap Leave direction trace
				currentMap.BMsetNextCell(currentX, currentY, agentID, nextCell);
				
				currentX = nextCell.getX();
				currentY = nextCell.getY();				
				stepMade = true; // Report that 1 step has been made
				return stepMade;
			}
			// Else break ties among equal cells
			else if (nextCells.size() != 1)
			{
				// Remove the previous cell from the next possible cells
				Coordinate2D previousCell = 
						oldMap.BMgetPreviousCell(currentX, currentY, agentID);
				nextCells = oldMap.BMexcludeCellFromList(
						previousCell.getX(), previousCell.getY(), nextCells);
				
				if (nextCells.size() > 0)
				{
					if (nextCells.elementAt(0).getValue() == Constants.BM_EXPLORED)
					{
						nextCells = oldMap.BMreturnCellsWithMinLRTA(nextCells);										
					}
					else if (nextCells.elementAt(0).getValue() == Constants.BM_UNEXPLORED)
					{
						nextCells = oldMap.BMreturnCellsWithMostClosedWallsAround(nextCells);
					}						
				}

				// Nowhere left to go (surrounded by closed cells and walls)
				if (nextCells.size() == 0)
				{
					currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
					currentMap.BMsetNextCell(currentX, currentY, agentID, new Coordinate2D(-1, -1));
					agentState = Constants.BM_AGENT_OFF;
					stepMade = false; // Report that no step has been made
				}
				else if (nextCells.size() == 1)
				{
					// Move to the next cell
					Coordinate2D nextCell = nextCells.elementAt(0);

					// 2. NextCellMap Leave direction trace
					currentMap.BMsetNextCell(currentX, currentY, agentID, nextCell);
					
					currentX = nextCell.getX();
					currentY = nextCell.getY();
					stepMade = true; // Report that 1 step has been made
				}
				else if (nextCells.size() > 1)
				{
					// Choose a random cell
					Coordinate2D nextCell = new Coordinate2D();
					nextCell = nextCells.elementAt((int) (Math.random() * nextCells.size()));
					
					// 2. NextCellMap Leave direction trace
					currentMap.BMsetNextCell(currentX, currentY, agentID, nextCell);
					
					currentX = nextCell.getX();
					currentY = nextCell.getY();
					stepMade = true; // Report that 1 step has been made
				}
			}
			
		} // end of LOOP_DETECTION
		
		/*******************\
		|***** CONTROL *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_LOOP_CONTROL)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CONTROL");

			// 3. Individual trace map
			currentMap.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			// If cell already closed by someone else, start loop cleaning
			if (oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_CLOSED)
			{
				if (Constants.DEBUG)
					System.out.println("This cell is closed.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// Agent has found its own ID trace
			else if (oldMap.BMgetIDsMap(currentX, currentY) == agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Found my own ID trace.");
				agentState = Constants.BM_AGENT_LOOP_CLOSING;
				stepMade = false;
				return stepMade;
			}
			// Agent did not find any trace (cell explored without control)
			else if ((oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE) &&
				(oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_EXPLORED))
			{
				// Marking
				currentMap.BMsetIDsMap(currentX, currentY, agentID); // Control cell

				// Navigation
				Coordinate2D nextCell = 
						oldMap.BMgetNextCell(currentX, currentY, agentID);
				
				// FIX BM
				if (nextCell.getX() == -1)
				{
					if (Constants.DEBUG)
						System.out.println("No next cell found.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				else if (oldMap.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) == Constants.BM_CLOSED)
				{
					if (Constants.DEBUG)
						System.out.println("The next cell is closed.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				// ELSE IF CELL NOT CLOSED (THEREFORE EXPLORED)
				else if (oldMap.BMgetIDsMap(nextCell.getX(), nextCell.getY()) > agentID) //&&
					//(map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) != Constants.BM_NO_AGENT_TRACE))
				{
					if (Constants.DEBUG)
						System.out.println("Agent with a higher priority controls next cell.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}				
				// Check for agents in STANDBY
				else if(oldMap.BMgetStandbyMap(nextCell.getX(), nextCell.getY()) > agentID)
				{
					if (Constants.DEBUG)
						System.out.println("Another agent with a higher priority " +
								"is on standby on the next cell.");
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
				else //if (map.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == Constants.BM_NO_AGENT_TRACE) 
					//(map.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
				{
					if (Constants.DEBUG)
						System.out.println("The next cell is open and free to control.");
					currentX = nextCell.getX();
					currentY = nextCell.getY();
					stepMade = true;
					return stepMade;	
				}
			}
			
			// If B controls and B > A
			else if (oldMap.BMgetIDsMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Another agent with a higher priority is controling this cell.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If C standby and C > A
			else if (oldMap.BMgetStandbyMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Another agent with a higher priority is on standby here.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// Otherwise become standby
			else
			{
				// Wait for your cell to be liberated
				if (Constants.DEBUG)
				{
					System.out.println("Waiting for the cell to be liberated");
					System.out.println("Local current CONTROL ID:" + oldMap.BMgetIDsMap(currentX, currentY));
					System.out.println("Local current STANDBY ID:" + oldMap.BMgetStandbyMap(currentX, currentY));
				}
				
				currentMap.BMsetStandbyMap(currentX, currentY, agentID);
				agentState = Constants.BM_AGENT_STANDBY;
				stepMade = false;
				return stepMade;				
			}			
		}
		
		/*******************\
		|***** CLOSING *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_LOOP_CLOSING )
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CLOSING");
			
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			// Marking
			//Switch to loop cleaning if intersection reached
			if (oldMap.BMisIntersection(currentX, currentY, agentID))
			{
				if (Constants.DEBUG)
					System.out.println("Intersection detected !");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			else //if(!map.BMisIntersection(currentX, currentY, agentID))
			{
				// Marking
				currentMap.setPhysicalMapValue(currentX, currentY, Constants.BM_CLOSED);
				
				// Navigation
				Coordinate2D nextCell = oldMap.BMgetNextCell(currentX, currentY, agentID);

				if (oldMap.BMgetIDsMap(nextCell.getX(), nextCell.getY()) == agentID)
				{
					if (oldMap.getPhysicalMapValue(nextCell.getX(), nextCell.getY()) != Constants.BM_CLOSED)
					{
						currentX = nextCell.getX();
						currentY = nextCell.getY();
						stepMade = true;
						return stepMade;
					}
					else // cell is closed
					{
						agentState = Constants.BM_AGENT_LOOP_CLEANING;
						stepMade = false;
						return stepMade;
					}
				}
				else
				{
					agentState = Constants.BM_AGENT_LOOP_CLEANING;
					stepMade = false;
					return stepMade;
				}
			}
		}
		
		/********************\
		|***** CLEANING *****|
		\********************/
		else if (agentState == Constants.BM_AGENT_LOOP_CLEANING)
		{
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is in LOOP CLEANING");
			Coordinate2D previousCell =
					oldMap.BMgetPreviousCellLoopCleaning(currentX, currentY, agentID);
			
			if (oldMap.BMgetIDsMap(currentX, currentY) == agentID)
			{
				// Marking
				currentMap.BMsetIDsMap(currentX, currentY, Constants.BM_NO_AGENT_TRACE);
				currentMap.BMsetNextCell(currentX, currentY, agentID, new Coordinate2D(-1, -1));
				
				if (previousCell.getX() != -1)
				{
					currentX = previousCell.getX();
					currentY = previousCell.getY();
					stepMade = true;
					return stepMade;
				}
				else
				{
					if (Constants.DEBUG)
						System.out.println("Previous cell not found for cleaning.");
					agentState = Constants.BM_AGENT_LOOP_DETECTION;
					stepMade = false;
					return stepMade;
				}
			}
			else if (previousCell.getX() != -1)
			{
				if (Constants.DEBUG)
					System.out.println("This cell is clean from my ID, " +
						"going to the neighbouring one to clean it.");
				currentX = previousCell.getX();
				currentY = previousCell.getY();
				stepMade = true;
				return stepMade;
			}
			else
			{
				if (Constants.DEBUG)
					System.out.println("This cell is already clean from my ID.");
				agentState = Constants.BM_AGENT_LOOP_DETECTION;
				stepMade = false;
				return stepMade;
			}
		}
		
		/*******************\
		|***** STANDBY *****|
		\*******************/
		else if (agentState == Constants.BM_AGENT_STANDBY)
		{
			// 3. Individual trace map
			//map.BMsetIndividualTraceMap(currentX, currentY, agentID, localTime);
			
			if (Constants.DEBUG)
			{
				System.out.println("Agent " + agentID + " is in STANDBY");
				System.out.println("Local current CONTROL ID:" + oldMap.BMgetIDsMap(currentX, currentY));
				System.out.println("Local current STANDBY ID:" + oldMap.BMgetStandbyMap(currentX, currentY));
			}

			// If cell becomes visited
			if (oldMap.getPhysicalMapValue(currentX, currentY) == Constants.BM_CLOSED)
			{
				if (Constants.DEBUG)
					System.out.println("Cell became visited. Switching to cleaning.");
				currentMap.BMsetStandbyMap(currentX, currentY, Constants.BM_NO_AGENT_TRACE);
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If replaced by another agent in STANDBY
			else if (oldMap.BMgetStandbyMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Agent with a higher priority took over the standby slot. Switching to cleaning.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If another agent TOOK CONTROL over
			else if (oldMap.BMgetIDsMap(currentX, currentY) > agentID)
			{
				if (Constants.DEBUG)
					System.out.println("Agent with a higher priority took over the cell. Switching to cleaning.");
				agentState = Constants.BM_AGENT_LOOP_CLEANING;
				stepMade = false;
				return stepMade;
			}
			// If cell is cleaned
			else if (oldMap.BMgetIDsMap(currentX, currentY) == Constants.BM_NO_AGENT_TRACE)
			{
				if (Constants.DEBUG)
					System.out.println("Cell cleaned. Switching back to CONTROL.");
				currentMap.BMsetStandbyMap(currentX, currentY, Constants.BM_NO_AGENT_TRACE);
				agentState = Constants.BM_AGENT_LOOP_CONTROL;
				stepMade = false;
				return stepMade;
			}
			// otherwise remain standby
			else
			{
				// Remain standby
				if (Constants.DEBUG)
				{
					System.out.println("Remaining in standby");
					System.out.println("Local current CONTROL ID:" + oldMap.BMgetIDsMap(currentX, currentY));
					System.out.println("Local current STANDBY ID:" + oldMap.BMgetStandbyMap(currentX, currentY));
				}
				stepMade = false;
				return stepMade;
			}
		}
		
		/***************\
		|***** OFF *****|
		\***************/
		else if (agentState == Constants.BM_AGENT_OFF)
		{
			if (Constants.DEBUG)
				System.out.println("Agent " + agentID + " is OFF");
			stepMade = false;
			return stepMade;
		}
			
		 
		
		/* Say if a step has been made */
		return stepMade;
	}
	// End of "CALL" function
}
